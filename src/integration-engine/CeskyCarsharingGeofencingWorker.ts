import { BaseWorker, DataSource, log } from "@golemio/core/dist/integration-engine";
import { CeskyCarsharingGeofencingDataSourceFactory } from "#ie/dataSources/CeskyCarsharingGeofencingDataSourceFactory";
import { CeskyCarsharingStaticDataService } from "#ie/CeskyCarsharingStaticDataService";

import { CeskyCarsharingGeofencingTransformation } from "#ie/transformations";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { ICeskyCarsharingGeofencingJson, ICeskyCarsharingPolygon } from "#sch/datasources/CeskyCarsharingGeofencingZone";
import { GeofencingZonesModel } from "./models/GeofencingZonesModel";
import { ModuleContainerToken, SharedBikesContainer } from "#ie/ioc";

export class CeskyCarsharingGeofencingWorker extends BaseWorker {
    protected readonly name = "CeskyCarsharingGeofencingWorker";

    private dataSource: DataSource;
    private ceskyCarsharingStaticDataService: CeskyCarsharingStaticDataService;
    private geofencingZonesModel: GeofencingZonesModel;
    private geofencingTransformation: CeskyCarsharingGeofencingTransformation;

    constructor() {
        super();
        this.dataSource = CeskyCarsharingGeofencingDataSourceFactory.getDataSource();
        this.ceskyCarsharingStaticDataService = SharedBikesContainer.resolve<CeskyCarsharingStaticDataService>(
            ModuleContainerToken.CeskyCarsharingStaticDataService
        );
        this.geofencingZonesModel = SharedBikesContainer.resolve<GeofencingZonesModel>(ModuleContainerToken.GeofencingZonesModel);
        this.geofencingTransformation = SharedBikesContainer.resolve<CeskyCarsharingGeofencingTransformation>(
            ModuleContainerToken.CeskyCarsharingGeofencingTransformation
        );
    }

    public refreshCeskyCarsharingGeofencingData = async (): Promise<void> => {
        try {
            await this.ceskyCarsharingStaticDataService.saveSystemsStaticData();
            const zones = await this.getGeofencingZonesFromDataSource();
            const transformedData = await this.geofencingTransformation.transform(zones);
            await this.geofencingZonesModel.save(transformedData);
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError("Error refreshing CeskyCarsharing geofencing data", this.constructor.name, err);
            }
        }
    };

    private async getGeofencingZonesFromDataSource(): Promise<ICeskyCarsharingPolygon[]> {
        const allData: ICeskyCarsharingGeofencingJson = await this.dataSource.getAll();
        return allData.polygons;
    }
}
