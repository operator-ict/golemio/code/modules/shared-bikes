import { MobilityOperatorDataSourceFactory } from "#ie/dataSources/MobilityOperatorDataSourceFactory";
import { StaticDataProvider } from "#ie/dataSources/StaticDataProvider";
import { GeoFilter } from "#ie/helpers/GeoFilter";
import { SystemInformationModel } from "#ie/models/SystemInformationModel";
import { MobilityOperatorRentalAppProvider } from "#ie/transformations/MobilityOperator/MobilityOperatorRentalAppProvider";
import { MobilityOperatorTransformation } from "#ie/transformations/MobilityOperator/MobilityOperatorTransformation";
import { RekolaStaticDataService } from "#ie/transformations/Rekola/RekolaStaticDataService";
import { DataSource } from "@golemio/core/dist/integration-engine";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { DependencyContainer, instanceCachingFactory } from "@golemio/core/dist/shared/tsyringe";
import { ModuleContainerToken } from "./ModuleContainerToken";
import { GeofencingZonesModel } from "#ie/models/GeofencingZonesModel";
import { CeskyCarsharingStaticDataService } from "#ie/CeskyCarsharingStaticDataService";
import { CeskyCarsharingGeofencingTransformation } from "#ie/transformations";
import { CeskyCarsharingBikeStatusTransformation } from "#ie/transformations/CeskyCarsharing/CeskyCarsharingBikeStatusTransformation";
import { BikeStatusModel } from "#ie/models/BikeStatusModel";
import { StationInformationHistoryModel } from "#ie/models/StationInformationHistoryModel";
import { OldDataCleanerHelper } from "#ie/helpers/OldDataCleanerHelper";

//#region Initialization
const SharedBikesContainer: DependencyContainer = IntegrationEngineContainer.createChildContainer();
//#endregion

//#region Datasources
SharedBikesContainer.registerSingleton<StaticDataProvider>(ModuleContainerToken.StaticDataProvider, StaticDataProvider);
SharedBikesContainer.register<DataSource>(ModuleContainerToken.MobilityOperatorDataSource, {
    useFactory: instanceCachingFactory<DataSource>(() => MobilityOperatorDataSourceFactory.getDataSource()),
});
//#endregion

//#region Transformations
SharedBikesContainer.registerSingleton<MobilityOperatorTransformation>(
    ModuleContainerToken.MobilityOperatorTransformation,
    MobilityOperatorTransformation
);
SharedBikesContainer.registerSingleton<MobilityOperatorRentalAppProvider>(
    ModuleContainerToken.MobilityOperatorRentalAppProvider,
    MobilityOperatorRentalAppProvider
);
SharedBikesContainer.registerSingleton<CeskyCarsharingGeofencingTransformation>(
    ModuleContainerToken.CeskyCarsharingGeofencingTransformation,
    CeskyCarsharingGeofencingTransformation
);
SharedBikesContainer.registerSingleton<CeskyCarsharingBikeStatusTransformation>(
    ModuleContainerToken.CeskyCarsharingBikeStatusTransformation,
    CeskyCarsharingBikeStatusTransformation
);
//#endregion

//#region Helpers
SharedBikesContainer.registerSingleton<GeoFilter>(ModuleContainerToken.GeoFilter, GeoFilter);
SharedBikesContainer.registerSingleton<OldDataCleanerHelper>(ModuleContainerToken.OldDataCleanerHelper, OldDataCleanerHelper);
//#endregion

//#region Repositories
SharedBikesContainer.registerSingleton<SystemInformationModel>(
    ModuleContainerToken.SystemInformationModel,
    SystemInformationModel
);
SharedBikesContainer.registerSingleton<BikeStatusModel>(ModuleContainerToken.BikeStatusModel, BikeStatusModel);
SharedBikesContainer.registerSingleton<StationInformationHistoryModel>(
    ModuleContainerToken.StationInformationHistoryModel,
    StationInformationHistoryModel
);
SharedBikesContainer.registerSingleton<GeofencingZonesModel>(ModuleContainerToken.GeofencingZonesModel, GeofencingZonesModel);
//#endregion

//#region Services
SharedBikesContainer.registerSingleton<CeskyCarsharingStaticDataService>(
    ModuleContainerToken.CeskyCarsharingStaticDataService,
    CeskyCarsharingStaticDataService
);
SharedBikesContainer.registerSingleton<RekolaStaticDataService>(
    ModuleContainerToken.RekolaStaticDataService,
    RekolaStaticDataService
);
//#endregion

export { SharedBikesContainer };
