const ModuleContainerToken = {
    StaticDataProvider: Symbol(),
    BikeStatusModel: Symbol(),
    SystemInformationModel: Symbol(),
    StationInformationHistoryModel: Symbol(),
    MobilityOperatorTransformation: Symbol(),
    MobilityOperatorDataSource: Symbol(),
    MobilityOperatorRentalAppProvider: Symbol(),

    CeskyCarsharingGeofencingTransformation: Symbol(),
    CeskyCarsharingBikeStatusTransformation: Symbol(),

    CeskyCarsharingStaticDataService: Symbol(),
    RekolaStaticDataService: Symbol(),

    GeoFilter: Symbol(),
    GeofencingZonesModel: Symbol(),

    OldDataCleanerHelper: Symbol(),
};

export { ModuleContainerToken };
