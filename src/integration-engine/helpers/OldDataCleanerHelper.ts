import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { ModuleContainerToken } from "#ie/ioc";
import { StationInformationHistoryModel } from "#ie/models/StationInformationHistoryModel";
import { BikeStatusModel } from "#ie/models/BikeStatusModel";

@injectable()
export class OldDataCleanerHelper {
    constructor(
        @inject(ModuleContainerToken.StationInformationHistoryModel)
        private stationInformationHistoryModel: StationInformationHistoryModel,
        @inject(ModuleContainerToken.BikeStatusModel) private bikeStatusModel: BikeStatusModel
    ) {}

    public async deleteOldTrackableData(deleteBefore: number, systemId: string): Promise<void> {
        await this.stationInformationHistoryModel.deleteDataBefore(deleteBefore, systemId);
        await this.bikeStatusModel.deleteDataBefore(deleteBefore, systemId);
    }
}
