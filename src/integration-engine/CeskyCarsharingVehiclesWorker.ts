import { CeskyCarsharingStaticDataService } from "#ie/CeskyCarsharingStaticDataService";
import { CeskyCarsharingVehiclesDataSourceFactory } from "#ie/dataSources/CeskyCarsharingVehiclesDataSourceFactory";
import { CeskyCarsharingBikeStatusTransformation } from "#ie/transformations/CeskyCarsharing/CeskyCarsharingBikeStatusTransformation";
import { ICeskyCarsharingCar, ICeskyCarsharingVehicles } from "#sch/datasources";
import { BaseWorker, DataSource } from "@golemio/core/dist/integration-engine";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { BikeStatusModel } from "./models/BikeStatusModel";
import { ModuleContainerToken, SharedBikesContainer } from "#ie/ioc";
import {
    AJO_SYSTEM_ID,
    ANYTIME_SYSTEM_ID,
    AUTONAPUL_SYSTEM_ID,
    CAR4WAY_SYSTEM_ID,
} from "#ie/transformations/CeskyCarsharing/transformConstants";
import { OldDataCleanerHelper } from "#ie/helpers/OldDataCleanerHelper";

export class CeskyCarsharingVehiclesWorker extends BaseWorker {
    protected readonly name: string = "CeskyCarsharingVehiclesWorker";

    private dataSource: DataSource;
    private staticDataService: CeskyCarsharingStaticDataService;
    private bikeStatusTransformation: CeskyCarsharingBikeStatusTransformation;
    private bikeStatusModel: BikeStatusModel;
    private oldDataCleanerHelper: OldDataCleanerHelper;

    constructor() {
        super();
        this.dataSource = CeskyCarsharingVehiclesDataSourceFactory.getDataSource();
        this.staticDataService = SharedBikesContainer.resolve<CeskyCarsharingStaticDataService>(
            ModuleContainerToken.CeskyCarsharingStaticDataService
        );
        this.bikeStatusModel = SharedBikesContainer.resolve<BikeStatusModel>(ModuleContainerToken.BikeStatusModel);
        this.bikeStatusTransformation = SharedBikesContainer.resolve<CeskyCarsharingBikeStatusTransformation>(
            ModuleContainerToken.CeskyCarsharingBikeStatusTransformation
        );
        this.oldDataCleanerHelper = SharedBikesContainer.resolve<OldDataCleanerHelper>(ModuleContainerToken.OldDataCleanerHelper);
    }

    public refreshCeskyCarsharingVehiclesData = async (): Promise<void> => {
        const transformationDate = new Date();

        try {
            const data: ICeskyCarsharingVehicles = await this.dataSource.getAll();
            const cars: ICeskyCarsharingCar[] = data.cars;

            await this.staticDataService.saveSystemsStaticData();

            const vehicleStatus = await this.bikeStatusTransformation.transform({
                cars,
                transformationDate,
            });
            await this.bikeStatusModel.saveIfInCzechia(vehicleStatus);

            const ceskyCarsharingSystemIds = [AJO_SYSTEM_ID, ANYTIME_SYSTEM_ID, AUTONAPUL_SYSTEM_ID, CAR4WAY_SYSTEM_ID];

            for (let systemId of ceskyCarsharingSystemIds) {
                await this.oldDataCleanerHelper.deleteOldTrackableData(transformationDate.getTime(), systemId);
            }
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError("Error while refreshing Cesky Carsharing vehicle data", this.constructor.name, err);
            }
        }
    };
}
