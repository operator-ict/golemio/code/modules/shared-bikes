import { CeskyCarsharingGeofencingWorker } from "#ie/CeskyCarsharingGeofencingWorker";
import { CeskyCarsharingVehiclesWorker } from "#ie/CeskyCarsharingVehiclesWorker";
import { HoppyGoSharedCarsWorker } from "#ie/HoppyGoSharedCarsWorker";
import { NextbikeSharedBikesWorker } from "#ie/NextbikeSharedBikesWorker";
import { RekolaSharedBikesWorker } from "#ie/RekolaSharedBikesWorker";
import { SharedBikes } from "#sch";
import { config } from "@golemio/core/dist/integration-engine/config";
import { IQueueDefinition } from "@golemio/core/dist/integration-engine/queueprocessors";

const queueDefinitions: IQueueDefinition[] = [
    {
        name: "Rekola" + SharedBikes.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + ".rekola" + SharedBikes.name.toLowerCase(),
        queues: [
            {
                name: "refreshRekolaGeofencingData",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 10 * 60 * 1000, // 10 minutes
                },
                worker: RekolaSharedBikesWorker,
                workerMethod: "refreshRekolaGeofencingData",
            },
            {
                name: "refreshRekolaTrackableData",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 1 * 60 * 1000, // 1 minute
                },
                worker: RekolaSharedBikesWorker,
                workerMethod: "refreshRekolaTrackableData",
            },
        ],
    },
    {
        name: "Nextbike" + SharedBikes.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + ".nextbike" + SharedBikes.name.toLowerCase(),
        queues: [
            {
                name: "refreshNextbikeData",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 1 * 60 * 1000, // 1 minute
                },
                worker: NextbikeSharedBikesWorker,
                workerMethod: "refreshNextbikeData",
            },
        ],
    },
    {
        name: "HoppyGo" + SharedBikes.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + ".hoppygo" + SharedBikes.name.toLowerCase(),
        queues: [
            {
                name: "refreshHoppyGoSharedCarsData",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 2 * 60 * 1000, // 2 minutes
                },
                worker: HoppyGoSharedCarsWorker,
                workerMethod: "refreshHoppyGoSharedCarsData",
            },
        ],
    },
    {
        name: "CeskyCarsharing" + SharedBikes.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + ".ceskycarsharing" + SharedBikes.name.toLowerCase(),
        queues: [
            {
                name: "refreshCeskyCarsharingVehiclesData",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 2 * 60 * 1000, // 2 minutes
                },
                worker: CeskyCarsharingVehiclesWorker,
                workerMethod: "refreshCeskyCarsharingVehiclesData",
            },
        ],
    },
    {
        name: "CeskyCarsharing" + SharedBikes.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + ".ceskycarsharing" + SharedBikes.name.toLowerCase(),
        queues: [
            {
                name: "refreshCeskyCarsharingGeofencingData",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 2 * 60 * 1000, // 2 minutes
                },
                worker: CeskyCarsharingGeofencingWorker,
                workerMethod: "refreshCeskyCarsharingGeofencingData",
            },
        ],
    },
];

export { queueDefinitions };
