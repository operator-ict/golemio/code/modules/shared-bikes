import { getRentalApps } from "#ie/transformations/CeskyCarsharing/transformRentalApps";
import { MobilityOperatorRentalAppProvider } from "#ie/transformations/MobilityOperator/MobilityOperatorRentalAppProvider";
import {
    ISharedBikesPricingOutput,
    ISharedBikesPricingPlanOutput,
    ISharedBikesRentalAppOutput,
    ISharedBikesSystemInformationOutput,
    ISharedBikesVehicleTypeOutput,
} from "#sch";
import { PricingModel } from "./models/PricingModel";
import { PricingPlansModel } from "./models/PricingPlansModel";
import { RentalAppsModel } from "./models/RentalAppsModel";
import { SystemInformationModel } from "./models/SystemInformationModel";
import { VehicleTypesModel } from "./models/VehicleTypesModel";
import { StaticDataProvider } from "#ie/dataSources/StaticDataProvider";
import { ModuleContainerToken } from "#ie/ioc";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class CeskyCarsharingStaticDataService {
    private PROVIDER_NAME = "CeskyCarsharing";

    constructor(
        @inject(ModuleContainerToken.MobilityOperatorRentalAppProvider)
        private rentalAppProvider: MobilityOperatorRentalAppProvider,
        @inject(ModuleContainerToken.StaticDataProvider) private staticDataProvider: StaticDataProvider
    ) {}

    public async saveSystemsStaticData(): Promise<void> {
        const staticApps = getRentalApps();
        const mobilityOperatorApps = await this.rentalAppProvider.getMobilityOperatorRentalApps();
        const systems = this.staticDataProvider.getSystemInformation(this.PROVIDER_NAME);

        let appsToSave = [];
        let systemsToSave = [];

        for (let system of systems) {
            if (mobilityOperatorApps[system.system_id] !== undefined) {
                system.rental_app_id = mobilityOperatorApps[system.system_id].id;
                appsToSave.push(mobilityOperatorApps[system.system_id]);
            } else {
                appsToSave.push(staticApps[system.system_id]);
            }
            systemsToSave.push(system);
        }

        await this.saveRentalApps(appsToSave);
        await this.saveSystemInformation(systemsToSave);
        await this.saveVehicleTypes(this.staticDataProvider.getVehicleTypes(this.PROVIDER_NAME));
        await this.savePricingPlans(this.staticDataProvider.getPricingPlans(this.PROVIDER_NAME));
        await this.savePricings(this.staticDataProvider.getPricings(this.PROVIDER_NAME));
    }

    private async saveVehicleTypes(vehicleTypes: ISharedBikesVehicleTypeOutput[]): Promise<void> {
        const vehicleTypesModel = new VehicleTypesModel();
        await vehicleTypesModel.save(vehicleTypes);
    }

    private async saveRentalApps(rentalApps: ISharedBikesRentalAppOutput[]): Promise<void> {
        const rentalAppsModel = new RentalAppsModel();
        await rentalAppsModel.save(rentalApps);
    }

    private async saveSystemInformation(systemInformation: ISharedBikesSystemInformationOutput[]): Promise<void> {
        const systemInformationModel = new SystemInformationModel();
        await systemInformationModel.save(systemInformation);
    }

    private async savePricingPlans(pricingPlans: ISharedBikesPricingPlanOutput[]): Promise<void> {
        const pricingPlansModel = new PricingPlansModel();
        await pricingPlansModel.save(pricingPlans);
    }

    private async savePricings(pricings: ISharedBikesPricingOutput[]): Promise<void> {
        const pricingModel = new PricingModel();
        await pricingModel.save(pricings);
    }
}
