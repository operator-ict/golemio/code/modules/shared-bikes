import { getRentalApps } from "#ie/transformations/HoppyGo/getRentalApps";
import { getSystemInformation } from "#ie/transformations/HoppyGo/getSystemInformation";
import { getVehicleTypes } from "#ie/transformations/HoppyGo/getVehicleTypes";
import { HOPPYGO_RENTAL_APP_ID, HOPPYGO_SYSTEM_ID } from "#ie/transformations/HoppyGo/transformConstants";
import { MobilityOperatorRentalAppProvider } from "#ie/transformations/MobilityOperator/MobilityOperatorRentalAppProvider";
import { ISharedBikesRentalAppOutput, ISharedBikesSystemInformationOutput, ISharedBikesVehicleTypeOutput } from "#sch";
import { RentalAppsModel } from "./models/RentalAppsModel";
import { SystemInformationModel } from "./models/SystemInformationModel";
import { VehicleTypesModel } from "./models/VehicleTypesModel";

export class HoppyGoStaticDataService {
    private systemInformationModel: SystemInformationModel;
    private rentalAppProvider: MobilityOperatorRentalAppProvider;

    constructor(rentalAppProvider: MobilityOperatorRentalAppProvider) {
        this.systemInformationModel = new SystemInformationModel();
        this.rentalAppProvider = rentalAppProvider;
    }

    public async saveSystemBasics(): Promise<void> {
        const rentalApps = await this.rentalAppProvider.getMobilityOperatorRentalApps();
        if (rentalApps[HOPPYGO_SYSTEM_ID] !== undefined) {
            const rentalApp = rentalApps[HOPPYGO_SYSTEM_ID];
            const currentSystemInformation = getSystemInformation(rentalApp.id);
            await this.saveSystemInformation(currentSystemInformation);
            await this.saveRentalApps([rentalApp]);
        } else {
            const currentSystemInformation = getSystemInformation(HOPPYGO_RENTAL_APP_ID);
            await this.saveSystemInformation(currentSystemInformation);
            await this.saveRentalApps([getRentalApps()]);
        }
        await this.saveVehicleTypes(getVehicleTypes());
    }

    private async saveVehicleTypes(vehicleTypes: ISharedBikesVehicleTypeOutput[]): Promise<void> {
        const vehicleTypesModel = new VehicleTypesModel();
        await vehicleTypesModel.save(vehicleTypes);
    }

    private async saveRentalApps(rentalApps: ISharedBikesRentalAppOutput[]): Promise<void> {
        const rentalAppsModel = new RentalAppsModel();
        await rentalAppsModel.save(rentalApps);
    }

    private async saveSystemInformation(systemInformation: ISharedBikesSystemInformationOutput): Promise<void> {
        await this.systemInformationModel.save([systemInformation]);
    }
}
