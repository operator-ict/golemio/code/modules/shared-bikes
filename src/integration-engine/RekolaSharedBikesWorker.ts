import { RekolaGeofencingZonesDataSourceFactory } from "#ie/dataSources/RekolaGeofencingZonesDataSourceFactory";
import { RekolaTrackablesDataSourceFactory } from "#ie/dataSources/RekolaTrackablesDataSourceFactory";
import { ModuleContainerToken, SharedBikesContainer } from "#ie/ioc";
import { REKOLA_SYSTEM_ID } from "#ie/transformations/Rekola";
import { RekolaBikeStatusTransformation } from "#ie/transformations/Rekola/RekolaBikeStatusTransformation";
import { RekolaStaticDataService } from "#ie/transformations/Rekola/RekolaStaticDataService";
import { RekolaStationInformationTransformation } from "#ie/transformations/Rekola/RekolaStationInformationTransformation";
import { RekolaStationStatusTransformation } from "#ie/transformations/Rekola/RekolaStationStatusTransformation";
import { RekolaStationStatusVehicleTypeTransformation } from "#ie/transformations/Rekola/RekolaStationStatusVehicleTypeTransformation";
import { RekolaVehicleTypeTransformation } from "#ie/transformations/Rekola/RekolaVehicleTypeTransformation";
import { IRekolaTrackablesDatasource } from "#sch";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources";
import { BaseWorker } from "@golemio/core/dist/integration-engine/workers";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { BikeStatusModel } from "./models/BikeStatusModel";
import { GeofencingZonesModel } from "./models/GeofencingZonesModel";
import { StationInformationModel } from "./models/StationInformationModel";
import { StationStatusModel } from "./models/StationStatusModel";
import { StationStatusVehicleTypeModel } from "./models/StationStatusVehicleTypeModel";
import { VehicleTypesModel } from "./models/VehicleTypesModel";
import { RekolaGeofencingTransformation } from "./transformations";
import { OldDataCleanerHelper } from "#ie/helpers/OldDataCleanerHelper";

export class RekolaSharedBikesWorker extends BaseWorker {
    private zonesDatasource: DataSource;
    private trackablesDatasource: DataSource;
    private vehicleTypesModel: VehicleTypesModel;
    private stationInformationModel: StationInformationModel;
    private stationStatusModel: StationStatusModel;
    private bikeStatusModel: BikeStatusModel;
    private geofencingZonesModel: GeofencingZonesModel;
    private stationStatusVehicleTypeModel: StationStatusVehicleTypeModel;
    private geofencingTransformation: RekolaGeofencingTransformation;
    private bikeStatusTransformation: RekolaBikeStatusTransformation;
    private stationInformationTransformation: RekolaStationInformationTransformation;
    private stationStatusTransformation: RekolaStationStatusTransformation;
    private vehicleTypeTransformation: RekolaVehicleTypeTransformation;
    private stationStatusVehicleTypeTransformation: RekolaStationStatusVehicleTypeTransformation;
    private rekolaStaticDataService: RekolaStaticDataService;
    private oldDataCleanerHelper: OldDataCleanerHelper;

    constructor() {
        super();
        this.zonesDatasource = RekolaGeofencingZonesDataSourceFactory.getDataSource();
        this.trackablesDatasource = RekolaTrackablesDataSourceFactory.getDataSource();
        this.vehicleTypesModel = new VehicleTypesModel();
        this.stationInformationModel = new StationInformationModel();
        this.stationStatusModel = new StationStatusModel();
        this.bikeStatusModel = new BikeStatusModel();
        this.geofencingZonesModel = new GeofencingZonesModel();
        this.stationStatusVehicleTypeModel = new StationStatusVehicleTypeModel();
        this.geofencingTransformation = new RekolaGeofencingTransformation();
        this.stationInformationTransformation = new RekolaStationInformationTransformation();
        this.stationStatusTransformation = new RekolaStationStatusTransformation();
        this.bikeStatusTransformation = new RekolaBikeStatusTransformation();
        this.vehicleTypeTransformation = new RekolaVehicleTypeTransformation();
        this.stationStatusVehicleTypeTransformation = new RekolaStationStatusVehicleTypeTransformation();
        this.rekolaStaticDataService = SharedBikesContainer.resolve<RekolaStaticDataService>(
            ModuleContainerToken.RekolaStaticDataService
        );
        this.oldDataCleanerHelper = SharedBikesContainer.resolve<OldDataCleanerHelper>(ModuleContainerToken.OldDataCleanerHelper);
    }

    /**
     * Worker method - fetch new Rekola geofencing data
     */
    public refreshRekolaGeofencingData = async (): Promise<void> => {
        try {
            const data = await this.zonesDatasource.getAll();
            const zones = await this.geofencingTransformation.transform(data);
            await this.geofencingZonesModel.replace(zones, REKOLA_SYSTEM_ID);
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError("Error while refreshing geofencing data", this.constructor.name, err);
            }
        }
    };

    /**
     * Worker method - fetch new Rekola trackable data
     */
    public refreshRekolaTrackableData = async (): Promise<void> => {
        const transformationDate = new Date();

        try {
            await this.rekolaStaticDataService.saveRekolaStaticData();

            const data: IRekolaTrackablesDatasource = await this.trackablesDatasource.getAll();

            const stationInformation = await this.stationInformationTransformation.transform(data.racks);
            const stationStatus = await this.stationStatusTransformation.transform({
                racks: data.racks,
                transformationDate,
            });
            const bikeStatus = await this.bikeStatusTransformation.transform({
                trackables: data,
                transformationDate,
            });
            const vehicleTypes = await this.vehicleTypeTransformation.transform(data);
            const stationStatusVehicleTypes = await this.stationStatusVehicleTypeTransformation.transform(data.racks);

            await this.vehicleTypesModel.save(vehicleTypes);
            await this.stationInformationModel.saveIfInCzechia(stationInformation);
            await this.stationStatusModel.save(stationStatus);
            await this.stationStatusVehicleTypeModel.save(stationStatusVehicleTypes);
            await this.bikeStatusModel.saveIfInCzechia(bikeStatus);

            await this.oldDataCleanerHelper.deleteOldTrackableData(transformationDate.getTime(), REKOLA_SYSTEM_ID);
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError("Error while refreshing trackable data: " + err.message, this.constructor.name, err);
            }
        }
    };
}
