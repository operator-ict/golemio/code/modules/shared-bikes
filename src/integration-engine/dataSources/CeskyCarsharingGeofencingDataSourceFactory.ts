import { config, DataSource, HTTPProtocolStrategy, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { SharedBikes } from "#sch";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class CeskyCarsharingGeofencingDataSourceFactory {
    public static getDataSource(): DataSource {
        return new DataSource(
            SharedBikes.datasources.CeskyCarsharingGeofencingJsonSchema.name,
            new HTTPProtocolStrategy({
                headers: config.datasources.CeskyCarsharingRequestHeaders,
                method: "POST",
                body: config.datasources.CeskyCarsharingRequestBody,
                url: config.datasources.CeskyCarsharingGeofencingApiUrl,
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                SharedBikes.datasources.CeskyCarsharingGeofencingJsonSchema.name,
                SharedBikes.datasources.CeskyCarsharingGeofencingJsonSchema.jsonSchema
            )
        );
    }
}
