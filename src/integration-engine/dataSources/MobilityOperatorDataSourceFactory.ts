import { config, DataSource, HTTPProtocolStrategy, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { SharedBikes } from "#sch";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class MobilityOperatorDataSourceFactory {
    public static getDataSource(): DataSource {
        return new DataSource(
            SharedBikes.datasources.MobilityOperatorJsonSchema.name,
            new HTTPProtocolStrategy({
                headers: {},
                method: "GET",
                url: config.datasources.MobilityOperatorUrl,
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                SharedBikes.datasources.MobilityOperatorJsonSchema.name,
                SharedBikes.datasources.MobilityOperatorJsonSchema.jsonSchema
            )
        );
    }
}
