import { config, DataSource, HTTPProtocolStrategy, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { SharedBikes } from "#sch";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class CeskyCarsharingVehiclesDataSourceFactory {
    public static getDataSource(): DataSource {
        return new DataSource(
            SharedBikes.datasources.CeskyCarsharingVehiclesJsonSchema.name,
            new HTTPProtocolStrategy({
                headers: config.datasources.CeskyCarsharingRequestHeaders,
                method: "POST",
                body: config.datasources.CeskyCarsharingRequestBody,
                url: config.datasources.CeskyCarsharingVehiclesApiUrl,
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                SharedBikes.datasources.CeskyCarsharingVehiclesJsonSchema.name,
                SharedBikes.datasources.CeskyCarsharingVehiclesJsonSchema.jsonSchema
            )
        );
    }
}
