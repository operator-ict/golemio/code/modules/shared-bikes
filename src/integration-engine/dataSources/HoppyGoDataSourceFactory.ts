import { config, DataSource, HTTPProtocolStrategy, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { SharedBikes } from "#sch";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class HoppyGoDataSourceFactory {
    public static getDataSource(): DataSource {
        return new DataSource(
            SharedBikes.datasources.HoppyGoVehiclesJsonSchema.name,
            new HTTPProtocolStrategy({
                headers: {},
                method: "GET",
                url: config.datasources.HoppyGoSharedCars,
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                SharedBikes.datasources.HoppyGoVehiclesJsonSchema.name,
                SharedBikes.datasources.HoppyGoVehiclesJsonSchema.jsonSchema
            )
        );
    }
}
