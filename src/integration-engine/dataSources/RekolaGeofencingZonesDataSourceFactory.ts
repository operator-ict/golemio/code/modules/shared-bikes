import { config, DataSource, HTTPProtocolStrategy, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { SharedBikes } from "#sch";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class RekolaGeofencingZonesDataSourceFactory {
    public static getDataSource(): DataSource {
        return new DataSource(
            SharedBikes.datasources.rekolaGeofencingZones.name,
            new HTTPProtocolStrategy({
                headers: config.datasources.RekolaSharedBikesHeaders,
                method: "GET",
                url: config.datasources.RekolaSharedBikesBaseUrl + "/zones",
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                SharedBikes.datasources.rekolaGeofencingZones.name,
                SharedBikes.datasources.rekolaGeofencingZones.jsonSchema
            )
        );
    }
}
