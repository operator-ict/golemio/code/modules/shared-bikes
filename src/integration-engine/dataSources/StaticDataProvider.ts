import {
    ISharedBikesPricingOutput,
    ISharedBikesPricingPlanOutput,
    ISharedBikesRentalAppOutput,
    ISharedBikesSystemInformationOutput,
    ISharedBikesVehicleTypeOutput,
} from "#sch";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import fs from "fs";
import path from "path";

interface IDataMap {
    [provider: string]: {
        [fileName: string]: any;
    };
}

@injectable()
export class StaticDataProvider {
    private dataMap: IDataMap = {};

    public getPricingPlans(provider: string): ISharedBikesPricingPlanOutput[] {
        return this.getData(provider, "pricingPlans");
    }

    public getPricings(provider: string): ISharedBikesPricingOutput[] {
        return this.getData(provider, "pricings");
    }

    public getRentalApps(provider: string): ISharedBikesRentalAppOutput {
        return this.getData(provider, "rentalApps");
    }

    public getSystemInformation(provider: string): ISharedBikesSystemInformationOutput[] {
        let data = this.getData(provider, "systemInformation");
        if (Array.isArray(data)) {
            return data.map((system) => {
                system.start_date = new Date(system.start_date);
                return system;
            });
        }
        data.start_date = new Date(data.start_date);
        return [data];
    }

    public getVehicleTypes(provider: string): ISharedBikesVehicleTypeOutput[] {
        return this.getData(provider, "vehicleTypes");
    }

    private getData(provider: string, fileName: string): any {
        if (this.dataMap[provider] !== undefined && this.dataMap[provider][fileName] !== undefined) {
            return this.dataMap[provider][fileName];
        }
        const filePath = path.resolve(__dirname, `../../../staticData/${provider}/${fileName}.json`);
        const data = JSON.parse(fs.readFileSync(filePath).toString("utf8"));
        this.dataMap[provider] = {
            ...this.dataMap[provider],
            [fileName]: data,
        };
        return data;
    }
}
