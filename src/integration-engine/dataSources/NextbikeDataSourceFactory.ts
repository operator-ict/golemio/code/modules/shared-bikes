import { INextbikeDefinitionTransformation } from "#ie/transformations/Nextbike";
import { SharedBikes, TNextbikeDataSources } from "#sch";
import { NEXT_BIKE_SOURCES } from "#sch/datasources";
import { config, DataSource, HTTPProtocolStrategy, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class NextbikeDataSourceFactory {
    private config = config.datasources.NextbikeSharedBikes;

    public getDataSourceDefinition(sourceId: string): DataSource {
        return new DataSource(
            SharedBikes.datasources.nextbikeDefinitionJsonSchema.name + "DataSource",
            new HTTPProtocolStrategy({
                headers: this.config.headers,
                method: "GET",
                url: `${this.config.baseUrl}${sourceId}/gbfs.json`,
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                SharedBikes.datasources.nextbikeDefinitionJsonSchema.name,
                SharedBikes.datasources.nextbikeDefinitionJsonSchema.jsonSchema
            )
        );
    }

    public getDataSourceData({ name, url }: INextbikeDefinitionTransformation): DataSource {
        switch (name) {
            case NEXT_BIKE_SOURCES.free_bike_status:
                return this.getDataSource(url, "nextbikeFreeBikeStatusJsonSchema");
            case NEXT_BIKE_SOURCES.station_information:
                return this.getDataSource(url, "nextbikeStationInfoJsonSchema");
            case NEXT_BIKE_SOURCES.station_status:
                return this.getDataSource(url, "nextbikeStationStatusJsonSchema");
            case NEXT_BIKE_SOURCES.system_information:
                return this.getDataSource(url, "nextbikeSystemInfoJsonSchema");
            case NEXT_BIKE_SOURCES.system_pricing_plans:
                return this.getDataSource(url, "nextbikeSystemPricingPlansJsonSchema");
            default:
                throw new Error(`Invalid data source type (${name})`);
        }
    }

    private getDataSource(url: string, schemaName: TNextbikeDataSources): DataSource {
        return new DataSource(
            SharedBikes.datasources[schemaName].name,
            new HTTPProtocolStrategy({
                headers: this.config.headers,
                method: "GET",
                url,
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(SharedBikes.datasources[schemaName].name, SharedBikes.datasources[schemaName].jsonSchema)
        );
    }
}
