import { SharedBikes } from "#sch";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import Sequelize, { Model } from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { BikeStatusModel } from "./BikeStatusModel";
import { StationStatusModel } from "./StationStatusModel";

@injectable()
export class StationInformationHistoryModel extends PostgresModel implements IModel {
    private stationStatusModel: StationStatusModel;
    private bikeStatusModel: BikeStatusModel;

    constructor() {
        super(
            SharedBikes.definitions.stationInformation.name + "Model",
            {
                outputSequelizeAttributes: SharedBikes.definitions.stationInformation.outputSequelizeAttributes,
                pgTableName: SharedBikes.definitions.stationInformation.pgTableName,
                pgSchema: SharedBikes.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                SharedBikes.definitions.stationInformation.name + "ModelValidator",
                SharedBikes.definitions.stationInformation.outputJsonSchema
            )
        );

        this.stationStatusModel = new StationStatusModel();
        this.bikeStatusModel = new BikeStatusModel();

        // Create associations
        this.stationStatusModel["sequelizeModel"].belongsTo(this.sequelizeModel, {
            foreignKey: "station_id",
        });
        this.sequelizeModel.hasOne(this.stationStatusModel["sequelizeModel"], {
            sourceKey: "id",
            foreignKey: "station_id",
        });

        this.bikeStatusModel["sequelizeModel"].belongsTo(this.sequelizeModel, {
            foreignKey: "station_id",
        });
        this.sequelizeModel.hasMany(this.bikeStatusModel["sequelizeModel"], {
            sourceKey: "id",
            foreignKey: "station_id",
            as: "bike_status",
        });
    }

    public deleteDataBefore = async (timestamp: number, systemId: string): Promise<number> => {
        const connection = PostgresConnector.getConnection();
        const t = await connection.transaction();

        try {
            const models = await this.sequelizeModel.findAll<Model>({
                attributes: {
                    include: ["id"],
                },
                include: [
                    {
                        as: "station_status",
                        model: this.stationStatusModel["sequelizeModel"],
                        where: {
                            last_reported: {
                                [Sequelize.Op.lt]: timestamp,
                            },
                        },
                        attributes: [],
                    },
                ],
                where: {
                    system_id: {
                        [Sequelize.Op.eq]: systemId,
                    },
                },
                transaction: t,
            });

            for (const model of models) {
                await model.destroy({ transaction: t });
            }

            await t.commit();
            return models.length;
        } catch (err) {
            await t.rollback();
            throw new GeneralError("Error while deleting old data", this.constructor.name, err);
        }
    };
}
