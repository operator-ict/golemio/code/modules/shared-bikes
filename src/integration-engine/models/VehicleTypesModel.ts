import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { SharedBikes } from "#sch";

export class VehicleTypesModel extends PostgresModel implements IModel {
    constructor() {
        super(
            SharedBikes.definitions.vehicleTypes.name + "Model",
            {
                outputSequelizeAttributes: SharedBikes.definitions.vehicleTypes.outputSequelizeAttributes,
                pgTableName: SharedBikes.definitions.vehicleTypes.pgTableName,
                pgSchema: SharedBikes.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                SharedBikes.definitions.vehicleTypes.name + "ModelValidator",
                SharedBikes.definitions.vehicleTypes.outputJsonSchema
            )
        );
    }
}
