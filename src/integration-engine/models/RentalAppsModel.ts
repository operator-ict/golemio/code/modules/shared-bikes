import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { SharedBikes } from "#sch";

export class RentalAppsModel extends PostgresModel implements IModel {
    constructor() {
        super(
            SharedBikes.definitions.rentalApps.name + "Model",
            {
                outputSequelizeAttributes: SharedBikes.definitions.rentalApps.outputSequelizeAttributes,
                pgTableName: SharedBikes.definitions.rentalApps.pgTableName,
                pgSchema: SharedBikes.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                SharedBikes.definitions.rentalApps.name + "ModelValidator",
                SharedBikes.definitions.rentalApps.outputJsonSchema
            )
        );
    }
}
