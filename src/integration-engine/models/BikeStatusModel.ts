import { GeoFilter } from "#ie/helpers/GeoFilter";
import { SharedBikesContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ISharedBikesBikeStatusOutput, SharedBikes } from "#sch";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class BikeStatusModel extends PostgresModel implements IModel {
    private readonly geoFilter: GeoFilter;

    constructor() {
        super(
            SharedBikes.definitions.bikeStatus.name + "Model",
            {
                outputSequelizeAttributes: SharedBikes.definitions.bikeStatus.outputSequelizeAttributes,
                pgTableName: SharedBikes.definitions.bikeStatus.pgTableName,
                pgSchema: SharedBikes.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                SharedBikes.definitions.bikeStatus.name + "ModelValidator",
                SharedBikes.definitions.bikeStatus.outputJsonSchema
            )
        );

        this.geoFilter = SharedBikesContainer.resolve<GeoFilter>(ModuleContainerToken.GeoFilter);
    }

    public deleteDataBefore = async (timestamp: number, systemId: string): Promise<number> => {
        try {
            return await this.sequelizeModel.destroy({
                where: {
                    last_reported: {
                        [Sequelize.Op.lt]: timestamp,
                    },
                    system_id: {
                        [Sequelize.Op.eq]: systemId,
                    },
                },
            });
        } catch (err) {
            throw new GeneralError("Error while purging old data", this.constructor.name, err);
        }
    };

    public saveIfInCzechia = async (vehicles: ISharedBikesBikeStatusOutput[]): Promise<void> => {
        await this.save(
            vehicles.filter((vehicle) => this.geoFilter.isInCzechia(vehicle.point.coordinates[1], vehicle.point.coordinates[0]))
        );
    };
}
