import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { config } from "@golemio/core/dist/integration-engine/config";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { ISharedBikesGeofencingZoneOutput, SharedBikes } from "#sch";
import { Model } from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class GeofencingZonesModel extends PostgresModel implements IModel {
    constructor() {
        super(
            SharedBikes.definitions.geofencingZones.name + "Model",
            {
                outputSequelizeAttributes: SharedBikes.definitions.geofencingZones.outputSequelizeAttributes,
                pgTableName: SharedBikes.definitions.geofencingZones.pgTableName,
                pgSchema: SharedBikes.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                SharedBikes.definitions.geofencingZones.name + "ModelValidator",
                SharedBikes.definitions.geofencingZones.outputJsonSchema
            )
        );
    }

    public replace = async (data: ISharedBikesGeofencingZoneOutput[], systemId: string): Promise<Model[]> => {
        const connection = PostgresConnector.getConnection();
        const t = await connection.transaction();

        try {
            await this.sequelizeModel.destroy({
                transaction: t,
                cascade: true,
                where: {
                    system_id: systemId,
                },
            });
            const rows = await this.sequelizeModel.bulkCreate(data as any, {
                transaction: t,
                returning: config.NODE_ENV === "test",
            });

            await t.commit();
            return rows;
        } catch (err) {
            await t.rollback();
            throw new GeneralError("Error while saving data", this.constructor.name, err);
        }
    };
}
