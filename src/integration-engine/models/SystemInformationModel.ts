import { ISharedBikesSystemInformationOutput, SharedBikes } from "#sch";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Model } from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class SystemInformationModel extends PostgresModel implements IModel {
    constructor() {
        super(
            SharedBikes.definitions.systemInformation.name + "Model",
            {
                outputSequelizeAttributes: SharedBikes.definitions.systemInformation.outputSequelizeAttributes,
                pgTableName: SharedBikes.definitions.systemInformation.pgTableName,
                pgSchema: SharedBikes.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                SharedBikes.definitions.systemInformation.name + "ModelValidator",
                SharedBikes.definitions.systemInformation.outputJsonSchema
            )
        );
    }

    public deleteBySystemId = async (systemId: string): Promise<void> => {
        const connection = PostgresConnector.getConnection();
        const t = await connection.transaction();

        try {
            const models = await this.sequelizeModel.findAll<Model>({
                where: {
                    system_id: systemId,
                },
                transaction: t,
            });

            for (const model of models) {
                await model.destroy({ transaction: t });
            }

            await t.commit();
        } catch (err) {
            await t.rollback();
            throw new GeneralError("Error while deleting system_information", this.constructor.name, err);
        }
    };

    public GetOne(id: string): Promise<ISharedBikesSystemInformationOutput> {
        return this.sequelizeModel.findOne({
            where: {
                system_id: id,
            },
            raw: true,
        });
    }
}
