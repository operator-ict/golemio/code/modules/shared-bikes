import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { SharedBikes } from "#sch";

export class StationStatusModel extends PostgresModel implements IModel {
    constructor() {
        super(
            SharedBikes.definitions.stationStatus.name + "Model",
            {
                outputSequelizeAttributes: SharedBikes.definitions.stationStatus.outputSequelizeAttributes,
                pgTableName: SharedBikes.definitions.stationStatus.pgTableName,
                pgSchema: SharedBikes.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                SharedBikes.definitions.stationStatus.name + "ModelValidator",
                SharedBikes.definitions.stationStatus.outputJsonSchema
            )
        );
    }
}
