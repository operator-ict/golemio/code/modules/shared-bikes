import { GeoFilter } from "#ie/helpers/GeoFilter";
import { SharedBikesContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ISharedBikesStationInformationOutput, SharedBikes } from "#sch";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class StationInformationModel extends PostgresModel implements IModel {
    private readonly geoFilter: GeoFilter;

    constructor() {
        super(
            SharedBikes.definitions.stationInformation.name + "Model",
            {
                outputSequelizeAttributes: SharedBikes.definitions.stationInformation.outputSequelizeAttributes,
                pgTableName: SharedBikes.definitions.stationInformation.pgTableName,
                pgSchema: SharedBikes.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                SharedBikes.definitions.stationInformation.name + "ModelValidator",
                SharedBikes.definitions.stationInformation.outputJsonSchema
            )
        );

        this.geoFilter = SharedBikesContainer.resolve<GeoFilter>(ModuleContainerToken.GeoFilter);
    }

    public saveIfInCzechia = async (station: ISharedBikesStationInformationOutput[]): Promise<void> => {
        await this.save(
            station.filter((station) => this.geoFilter.isInCzechia(station.point.coordinates[1], station.point.coordinates[0]))
        );
    };
}
