import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { SharedBikes } from "#sch";

export class StationStatusVehicleTypeModel extends PostgresModel implements IModel {
    constructor() {
        super(
            SharedBikes.definitions.stationsStatusVehicleType.name + "Model",
            {
                outputSequelizeAttributes: SharedBikes.definitions.stationsStatusVehicleType.outputSequelizeAttributes,
                pgTableName: SharedBikes.definitions.stationsStatusVehicleType.pgTableName,
                pgSchema: SharedBikes.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                SharedBikes.definitions.stationsStatusVehicleType.name + "ModelValidator",
                SharedBikes.definitions.stationsStatusVehicleType.outputJsonSchema
            )
        );
    }
}
