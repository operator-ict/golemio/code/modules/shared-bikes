import getUuidByString from "uuid-by-string";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import { ISharedBikesBikeStatusOutput, ISharedBikesRentalAppOutput, SharedBikes } from "#sch";
import { INextbikeFreeBikeStatus, INextbikeFreeBikeStatusInput } from "#sch/datasources";
import { getWhereGeometry } from "#ie/transformations/helpers/getWhereGeometry";
import { NEXTBIKE_VEHICLE_TYPE_ID, NEXTBIKE_ID_PREFIX } from "#ie/transformations/Nextbike";
import { getRentalAppsFromUris } from "#ie/transformations/Nextbike/getRentalAppsFromUris";

export interface INextBikeFreeBikeStatusOutput {
    bikeStatus: ISharedBikesBikeStatusOutput[];
    rentalApps: ISharedBikesRentalAppOutput[];
}

export class NextbikeFreeBikeStatusTransformation extends BaseTransformation implements ITransformation {
    public name: string;
    private readonly systemId: string;

    constructor(private sourceId: string) {
        super();
        this.name = SharedBikes.datasources.nextbikeFreeBikeStatusJsonSchema.name + sourceId + "Transformation";
        this.systemId = getUuidByString(sourceId);
    }

    public transform = async (datasourceData: INextbikeFreeBikeStatusInput): Promise<INextBikeFreeBikeStatusOutput> => {
        const bikeStatus: ISharedBikesBikeStatusOutput[] = [];
        const rentalApps: ISharedBikesRentalAppOutput[] = [];

        for (const item of datasourceData.data.bikes) {
            const transformedData = this.transformElement({ ...item, last_updated: datasourceData.last_updated });
            bikeStatus.push(transformedData.bikeStatus);
            rentalApps.push(transformedData.rentalApps);
        }

        return {
            bikeStatus,
            rentalApps,
        };
    };

    protected transformElement = (
        item: INextbikeFreeBikeStatus & { last_updated: number }
    ): {
        bikeStatus: ISharedBikesBikeStatusOutput;
        rentalApps: ISharedBikesRentalAppOutput;
    } => {
        const rentalAppId = getUuidByString(JSON.stringify(item.rental_uris));
        const pricingPlanId = `${NEXTBIKE_ID_PREFIX}${this.sourceId}-${item.pricing_plan_id}`;

        return {
            bikeStatus: {
                id: NEXTBIKE_ID_PREFIX + item.bike_id,
                system_id: this.systemId,
                point: getWhereGeometry(item.lat, item.lon),
                helmets: null,
                passengers: null,
                damage_description: null,
                description: null,
                vehicle_registration: null,
                is_reserved: item.is_reserved,
                is_disabled: item.is_disabled,
                vehicle_type_id: NEXTBIKE_VEHICLE_TYPE_ID,
                last_reported: new Date(item.last_updated * 1000).toISOString(),
                current_range_meters: null,
                charge_percent: null,
                rental_app_id: rentalAppId,
                station_id: item.station_id ? NEXTBIKE_ID_PREFIX + item.station_id : null,
                pricing_plan_id: pricingPlanId,
                make: null,
                model: null,
                color: null,
            },
            rentalApps: getRentalAppsFromUris(item.rental_uris, rentalAppId),
        };
    };
}
