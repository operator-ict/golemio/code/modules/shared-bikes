import { MobilityOperatorRentalAppProvider } from "#ie/transformations/MobilityOperator/MobilityOperatorRentalAppProvider";
import { NEXTBIKE_INTEGRATION_DATE } from "#ie/transformations/Nextbike/transformConstants";
import { ISharedBikesRentalAppOutput, ISharedBikesSystemInformationOutput, SharedBikes } from "#sch";
import { INextbikeRentalApps, INextbikeSystemInformation, INextbikeSystemInformationInput } from "#sch/datasources";
import { IMobilityOperatorRentalAppMap } from "#sch/datasources/MobilityOperator";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import getUuidByString from "uuid-by-string";

export interface INextBikeSystemInformationOutput {
    systemInfo: ISharedBikesSystemInformationOutput;
    rentalApps: ISharedBikesRentalAppOutput;
}

export class NextbikeSystemInformationTransformation extends BaseTransformation implements ITransformation {
    public name: string;
    private readonly systemId: string;
    private readonly transformationDate: Date;
    private rentalAppProvider: MobilityOperatorRentalAppProvider;

    constructor(sourceId: string, rentalAppProvider: MobilityOperatorRentalAppProvider) {
        super();
        this.name = SharedBikes.datasources.nextbikeSystemInfoJsonSchema.name + sourceId + "Transformation";
        this.systemId = getUuidByString(sourceId);
        this.transformationDate = new Date(NEXTBIKE_INTEGRATION_DATE);
        this.rentalAppProvider = rentalAppProvider;
    }

    public transform = async (datasourceData: INextbikeSystemInformationInput): Promise<INextBikeSystemInformationOutput> => {
        const rentalApps = await this.rentalAppProvider.getMobilityOperatorRentalApps();
        return this.transformElement({
            system: datasourceData.data,
            rentalApps,
        });
    };

    protected transformElement = (item: { system: INextbikeSystemInformation; rentalApps: IMobilityOperatorRentalAppMap }) => {
        let rentalAppId: string;
        if (item.rentalApps[this.systemId] !== undefined) {
            rentalAppId = item.rentalApps[this.systemId].id;
        } else {
            rentalAppId = getUuidByString(JSON.stringify(item.system.rental_apps));
        }

        return {
            systemInfo: {
                operator_id: item.system.system_id,
                system_id: this.systemId,
                language: item.system.language,
                logo: "https://upload.wikimedia.org/wikipedia/commons/f/f3/Nextbike_Logo.svg",
                name: this.getSystemName(item.system.name),
                short_name: null,
                operator: item.system.operator,
                url: item.system.url,
                purchase_url: null,
                start_date: this.transformationDate,
                phone_number: item.system.phone_number,
                email: item.system.email,
                feed_contact_email: null,
                timezone: item.system.timezone,
                license_id: item.system.license_id,
                license_url: null,
                attribution_organization_name: null,
                attribution_url: null,
                terms_of_use_url: "https://www.nextbikeczech.com/vseobecne-obchodni-podminky/",
                rental_app_id: rentalAppId,
            },
            rentalApps: item.rentalApps[this.systemId] ?? this.transformRentalApps(item.system.rental_apps, rentalAppId),
        };
    };

    private transformRentalApps = (data: INextbikeRentalApps, id: string): ISharedBikesRentalAppOutput => {
        return {
            id,
            android_store_url: data.android.store_uri,
            android_discovery_url: data.android.discovery_uri,
            ios_store_url: data.ios.store_uri,
            ios_discovery_url: data.ios.discovery_uri,
            web_url: null,
        };
    };

    private getSystemName = (name: string) => {
        return name.charAt(0).toUpperCase() + name.slice(1);
    };
}
