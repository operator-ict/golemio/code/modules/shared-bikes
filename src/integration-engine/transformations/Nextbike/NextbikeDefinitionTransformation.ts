import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { SharedBikes } from "#sch";
import { INextbikeDefinitionInput, INextbikeFeedDefinitionItem, NEXT_BIKE_SOURCES, TNextbikeDataTypes } from "#sch/datasources";

export interface INextbikeDefinitionTransformation extends INextbikeFeedDefinitionItem {
    name: TNextbikeDataTypes;
    url: string;
}

export class NextbikeDefinitionTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor(sourceId: string) {
        super();
        this.name = SharedBikes.datasources.nextbikeDefinitionJsonSchema.name + sourceId + "Data";
    }

    public transform = async (datasourceData: INextbikeDefinitionInput): Promise<INextbikeDefinitionTransformation[]> => {
        const result: INextbikeDefinitionTransformation[] = [];
        for (const feed of datasourceData.data.cs.feeds) {
            if (feed.name in NEXT_BIKE_SOURCES) {
                result.push({ name: feed.name as TNextbikeDataTypes, url: feed.url });
            }
        }

        return result;
    };

    protected transformElement = () => {
        throw new Error("Not yet implemented");
    };
}
