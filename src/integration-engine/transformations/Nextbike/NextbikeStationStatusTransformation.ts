import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import { ISharedBikesStationStatusOutput, ISharedBikesStationStatusVehicleTypeOutput, SharedBikes } from "#sch";
import { INextbikeStationStatus, INextbikeStationStatusInput } from "#sch/datasources";
import { NEXTBIKE_ID_PREFIX, NEXTBIKE_VEHICLE_TYPE_ID } from "#ie/transformations/Nextbike";

export interface INextBikeStationStatusOutput {
    stationStatus: ISharedBikesStationStatusOutput[];
    stationStatusVehicleType: ISharedBikesStationStatusVehicleTypeOutput[];
}

interface INextBikeStationStatusElementOutput {
    stationStatus: ISharedBikesStationStatusOutput;
    stationStatusVehicleType: ISharedBikesStationStatusVehicleTypeOutput;
}

export class NextbikeStationStatusTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor(sourceId: string) {
        super();
        this.name = SharedBikes.datasources.nextbikeStationStatusJsonSchema.name + sourceId + "Transformation";
    }

    public transform = async (datasourceData: INextbikeStationStatusInput): Promise<INextBikeStationStatusOutput> => {
        const stationStatus: ISharedBikesStationStatusOutput[] = [];
        const stationStatusVehicleType: ISharedBikesStationStatusVehicleTypeOutput[] = [];
        for (const item of datasourceData.data.stations) {
            const transformedData = this.transformElement(item);
            stationStatus.push(transformedData.stationStatus);
            stationStatusVehicleType.push(transformedData.stationStatusVehicleType);
        }

        return {
            stationStatus,
            stationStatusVehicleType,
        };
    };

    protected transformElement = (item: INextbikeStationStatus): INextBikeStationStatusElementOutput => ({
        stationStatus: {
            station_id: NEXTBIKE_ID_PREFIX + item.station_id,
            num_bikes_available: item.num_bikes_available,
            num_bikes_disabled: null,
            num_docks_available: item.num_docks_available,
            is_installed: item.is_installed,
            is_renting: item.is_renting,
            is_returning: item.is_returning,
            last_reported: new Date(item.last_reported * 1000).toISOString(),
        },
        stationStatusVehicleType: {
            station_id: NEXTBIKE_ID_PREFIX + item.station_id,
            vehicle_type_id: NEXTBIKE_VEHICLE_TYPE_ID,
            count: item.num_bikes_available,
            num_bikes_available: item.num_bikes_available,
            vehicle_docks_available: item.num_docks_available,
            num_bikes_disabled: null,
        },
    });
}
