import { ISharedBikesVehicleTypeOutput } from "#sch";
import { NEXTBIKE_VEHICLE_TYPE_ID } from "#ie/transformations/Nextbike";

export const getVehicleType = (): ISharedBikesVehicleTypeOutput => {
    return {
        id: NEXTBIKE_VEHICLE_TYPE_ID,
        form_factor: "bicycle",
        propulsion_type: "human",
        max_range_meters: null,
        name: "bicycle",
    };
};
