import getUuidByString from "uuid-by-string";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import { ISharedBikesPricingOutput, ISharedBikesPricingPlanOutput, SharedBikes } from "#sch";
import { NEXTBIKE_ID_PREFIX } from "#ie/transformations/Nextbike";
import { INextbikeSystemPricing, INextbikeSystemPricingPlans, INextbikeSystemPricingPlansInput } from "#sch/datasources";
import { Md5 } from "ts-md5";

export interface INextBikePricingPlansOutput {
    pricingPlan: ISharedBikesPricingPlanOutput[];
    pricing: ISharedBikesPricingOutput[];
}

export interface INextBikePricingPlansElementOutput {
    pricingPlan: ISharedBikesPricingPlanOutput;
    pricing: ISharedBikesPricingOutput[];
}

export class NextbikeSystemPricingPlansTransformation extends BaseTransformation implements ITransformation {
    public name: string;
    private readonly systemId: string;

    constructor(private sourceId: string) {
        super();
        this.name = SharedBikes.datasources.nextbikeSystemPricingPlansJsonSchema.name + sourceId + "Transformation";
        this.systemId = getUuidByString(sourceId);
    }

    public transform = async (datasourceData: INextbikeSystemPricingPlansInput): Promise<INextBikePricingPlansOutput> => {
        const pricingPlan: ISharedBikesPricingPlanOutput[] = [];
        const pricing: ISharedBikesPricingOutput[] = [];

        for (const item of datasourceData.data.plans) {
            const transformedData = this.transformElement({ ...item, last_updated: datasourceData.last_updated });
            pricingPlan.push(transformedData.pricingPlan);
            pricing.push(...transformedData.pricing);
        }

        return {
            pricingPlan,
            pricing,
        };
    };

    protected transformElement = (
        item: INextbikeSystemPricingPlans & { last_updated: number }
    ): INextBikePricingPlansElementOutput => {
        const pricingPlanId = `${NEXTBIKE_ID_PREFIX}${this.sourceId}-${item.plan_id}`;
        const description =
            this.systemId == "dfe4fb90-4db2-5058-869b-9638fab96642" || this.systemId == "a85546f5-4aeb-5123-a3ed-449b6dd884e9"
                ? "Nextbike Vám nabízí 15 minut zdarma na každou výpůjčku, " +
                  "více na: https://www.nextbikeczech.com/kompletni-cenik/ " +
                  item.description
                : item.description;

        const pricingPlan = {
            id: pricingPlanId,
            system_id: this.systemId,
            url: "https://www.nextbikeczech.com/kompletni-cenik/",
            last_updated: new Date(item.last_updated * 1000).toISOString(),
            name: item.name,
            currency: item.currency,
            price: item.price,
            is_taxable: item.is_taxable,
            description: description,
            surge_pricing: false,
        };

        const pricing = [];
        pricing.push(
            ...(item.per_min_pricing?.map((price: any) => this.transformPricing(price, pricingPlanId, "per_min_pricing")) || [])
        );
        pricing.push(
            ...(item.per_km_pricing?.map((price: any) => this.transformPricing(price, pricingPlanId, "per_km_pricing")) || [])
        );
        pricing.push(
            ...(item.per_min_reservation_pricing?.map((price: any) =>
                this.transformPricing(price, pricingPlanId, "per_min_reservation_pricing")
            ) || [])
        );

        return {
            pricingPlan,
            pricing,
        };
    };

    private transformPricing = (data: INextbikeSystemPricing, pricingPlanId: string, type: string): ISharedBikesPricingOutput => {
        return {
            id: Md5.hashStr(pricingPlanId + type + data.start + data.rate),
            pricing_plan_id: pricingPlanId,
            pricing_type: type,
            pricing_order: null,
            start: data.start,
            rate: data.rate,
            interval: data.interval,
            end: data.end ?? null,
            start_time_of_period: null,
            end_time_of_period: null,
        };
    };
}
