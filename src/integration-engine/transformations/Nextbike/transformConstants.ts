export const NEXTBIKE_VEHICLE_TYPE_ID = "nextbike-bike";
export const NEXTBIKE_ID_PREFIX = "nextbike-";
export const NEXTBIKE_INTEGRATION_DATE = "2022-06-20T00:00:00.000Z";
