import getUuidByString from "uuid-by-string";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import { ISharedBikesRentalAppOutput, ISharedBikesStationInformationOutput, SharedBikes } from "#sch";
import { INextbikeStationInformation, INextbikeStationInformationInput } from "#sch/datasources";
import { getWhereGeometry } from "#ie/transformations/helpers/getWhereGeometry";
import { NEXTBIKE_ID_PREFIX } from "#ie/transformations/Nextbike";
import { getRentalAppsFromUris } from "#ie/transformations/Nextbike/getRentalAppsFromUris";

export interface INextBikeStationInformationOutput {
    stationInfo: ISharedBikesStationInformationOutput[];
    rentalApps: ISharedBikesRentalAppOutput[];
}

export class NextbikeStationInformationTransformation extends BaseTransformation implements ITransformation {
    public name: string;
    private readonly systemId: string;

    constructor(sourceId: string) {
        super();
        this.name = SharedBikes.datasources.nextbikeStationInfoJsonSchema.name + sourceId + "Transformation";
        this.systemId = getUuidByString(sourceId);
    }

    public transform = async (datasourceData: INextbikeStationInformationInput): Promise<INextBikeStationInformationOutput> => {
        const stationInfo: ISharedBikesStationInformationOutput[] = [];
        const rentalApps: ISharedBikesRentalAppOutput[] = [];

        for (const item of datasourceData.data.stations) {
            const transformedData = this.transformElement(item);
            stationInfo.push(transformedData.stationInfo);
            rentalApps.push(transformedData.rentalApps);
        }

        return {
            stationInfo,
            rentalApps,
        };
    };

    protected transformElement = (
        item: INextbikeStationInformation
    ): {
        stationInfo: ISharedBikesStationInformationOutput;
        rentalApps: ISharedBikesRentalAppOutput;
    } => {
        const rentalAppId = getUuidByString(JSON.stringify(item.rental_uris));

        return {
            stationInfo: {
                id: NEXTBIKE_ID_PREFIX + item.station_id,
                system_id: this.systemId,
                name: item.name,
                short_name: item.short_name,
                point: getWhereGeometry(item.lat, item.lon),
                address: null,
                post_code: null,
                cross_street: null,
                region_id: item.region_id,
                rental_methods: null,
                is_virtual_station: item.is_virtual_station,
                station_area: null,
                capacity: item.capacity,
                vehicle_capacity: null,
                vehicle_type_capacity: null,
                is_valet_station: null,
                rental_app_id: rentalAppId,
            },
            rentalApps: getRentalAppsFromUris(item.rental_uris, rentalAppId),
        };
    };
}
