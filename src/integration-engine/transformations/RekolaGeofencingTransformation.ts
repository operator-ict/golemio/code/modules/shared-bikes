import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import {
    IRekolaGeofencingDatasourceItem,
    ISharedBikesGeofencingZoneOutput,
    IRekolaGeofencingDatasourceZone,
    SharedBikes,
} from "#sch";
import { REKOLA_PREFIX, REKOLA_SYSTEM_ID } from "#ie/transformations/Rekola";

export class RekolaGeofencingTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = SharedBikes.datasources.rekolaGeofencingZones.name;
    }

    public transform = (data: IRekolaGeofencingDatasourceItem[]): Promise<ISharedBikesGeofencingZoneOutput[]> => {
        const res: ISharedBikesGeofencingZoneOutput[] = [];

        for (const item of data) {
            for (const zone of item.zones) {
                res.push(this.transformElement(zone));
            }
        }

        return Promise.resolve(res);
    };

    /**
     * Transform zones
     */
    protected transformElement = (element: IRekolaGeofencingDatasourceZone): ISharedBikesGeofencingZoneOutput => {
        return {
            id: REKOLA_PREFIX + element.id,
            system_id: REKOLA_SYSTEM_ID,
            name: null,
            note: null,
            source: null,
            price: null,
            priority: 0,
            start: null,
            end: null,
            geom: {
                type: "MultiPolygon",
                coordinates: [[element.points.map(([lat, lng]) => [lng, lat])]],
            },
            ride_allowed: true,
            ride_through_allowed: true,
            maximum_speed_kph: null,
            parking_allowed: true,
        };
    };
}
