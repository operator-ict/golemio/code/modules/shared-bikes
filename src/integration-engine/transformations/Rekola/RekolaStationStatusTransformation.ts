import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { IRekolaTrackablesDatasourceRack, ISharedBikesStationStatusOutput, SharedBikes } from "#sch";
import { REKOLA_PREFIX } from "#ie/transformations/Rekola/transformConstants";

export class RekolaStationStatusTransformation extends BaseTransformation implements ITransformation {
    public name: string = SharedBikes.datasources.rekolaTrackables.name + "StationStatusTransformation";

    public transform = async (data: {
        racks: IRekolaTrackablesDatasourceRack[];
        transformationDate: Date;
    }): Promise<ISharedBikesStationStatusOutput[]> => {
        const stations: ISharedBikesStationStatusOutput[] = [];

        for (const rack of data.racks) {
            if (rack.isVisible) {
                stations.push(this.transformElement({ rack, transformationDate: data.transformationDate }));
            }
        }

        return stations;
    };

    protected transformElement = (element: {
        rack: IRekolaTrackablesDatasourceRack;
        transformationDate: Date;
    }): ISharedBikesStationStatusOutput => {
        return {
            station_id: REKOLA_PREFIX + element.rack.id,
            num_bikes_available: element.rack.vehicles.length,
            num_bikes_disabled: null,
            num_docks_available: null,
            is_installed: true,
            is_renting: element.rack.vehicles.length > 0,
            is_returning: true,
            last_reported: element.transformationDate.toISOString(),
        };
    };
}
