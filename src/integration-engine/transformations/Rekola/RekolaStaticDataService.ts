import { StaticDataProvider } from "#ie/dataSources/StaticDataProvider";
import { ModuleContainerToken } from "#ie/ioc";
import { PricingModel } from "#ie/models/PricingModel";
import { PricingPlansModel } from "#ie/models/PricingPlansModel";
import { RentalAppsModel } from "#ie/models/RentalAppsModel";
import { SystemInformationModel } from "#ie/models/SystemInformationModel";
import { MobilityOperatorRentalAppProvider } from "#ie/transformations/MobilityOperator/MobilityOperatorRentalAppProvider";
import { REKOLA_SYSTEM_ID } from "#ie/transformations/Rekola/transformConstants";
import {
    ISharedBikesPricingOutput,
    ISharedBikesPricingPlanOutput,
    ISharedBikesRentalAppOutput,
    ISharedBikesSystemInformationOutput,
} from "#sch";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class RekolaStaticDataService {
    constructor(
        @inject(ModuleContainerToken.SystemInformationModel) private systemInformationModel: SystemInformationModel,
        @inject(ModuleContainerToken.MobilityOperatorRentalAppProvider)
        private rentalAppProvider: MobilityOperatorRentalAppProvider,
        @inject(ModuleContainerToken.StaticDataProvider) private staticDataProvider: StaticDataProvider
    ) {}

    public async saveRekolaStaticData(): Promise<void> {
        const rentalApps = await this.rentalAppProvider.getMobilityOperatorRentalApps();
        let systemInformation = this.staticDataProvider.getSystemInformation("Rekola");

        if (rentalApps[REKOLA_SYSTEM_ID] !== undefined) {
            const rentalApp = rentalApps[REKOLA_SYSTEM_ID];
            await this.saveRentalApps([rentalApp]);
            systemInformation[0].rental_app_id = rentalApp.id;
        } else {
            await this.saveRentalApps([this.staticDataProvider.getRentalApps("Rekola")]);
        }
        await this.saveSystemInformation(systemInformation[0]);
        await this.savePricingPlans(this.staticDataProvider.getPricingPlans("Rekola"));
        await this.savePricings(this.staticDataProvider.getPricings("Rekola"));
    }

    private async savePricingPlans(pricingPlans: ISharedBikesPricingPlanOutput[]): Promise<void> {
        const pricingPlansModel = new PricingPlansModel();
        await pricingPlansModel.save(pricingPlans);
    }

    private async savePricings(pricings: ISharedBikesPricingOutput[]): Promise<void> {
        const pricingModel = new PricingModel();
        await pricingModel.save(pricings);
    }

    private async saveRentalApps(rentalApps: ISharedBikesRentalAppOutput[]): Promise<void> {
        const rentalAppsModel = new RentalAppsModel();
        await rentalAppsModel.save(rentalApps);
    }

    private async saveSystemInformation(systemInformation: ISharedBikesSystemInformationOutput): Promise<void> {
        await this.systemInformationModel.save([systemInformation]);
    }
}
