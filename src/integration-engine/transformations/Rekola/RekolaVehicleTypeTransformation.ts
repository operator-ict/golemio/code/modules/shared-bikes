import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import {
    IRekolaTrackablesDatasource,
    IRekolaTrackablesDatasourceVehicle,
    ISharedBikesVehicleTypeOutput,
    SharedBikes,
} from "#sch";
import { REKOLA_PREFIX } from "#ie/transformations/Rekola/transformConstants";

export class RekolaVehicleTypeTransformation extends BaseTransformation implements ITransformation {
    public name: string = SharedBikes.datasources.rekolaTrackables.name + "VehicleTypeTransformation";

    public transform = async (data: IRekolaTrackablesDatasource): Promise<ISharedBikesVehicleTypeOutput[]> => {
        let vehicleTypeOutputs: ISharedBikesVehicleTypeOutput[] = [];

        for (const rack of data.racks) {
            for (const bike of rack.vehicles) {
                vehicleTypeOutputs.push(this.transformElement(bike));
            }
        }

        for (const bike of data.vehicles) {
            vehicleTypeOutputs.push(this.transformElement(bike));
        }

        // Filter out redundant vehicle types
        vehicleTypeOutputs = [...new Map(vehicleTypeOutputs.map((type) => [type.id, type])).values()];

        return vehicleTypeOutputs;
    };

    protected transformElement = (element: IRekolaTrackablesDatasourceVehicle): ISharedBikesVehicleTypeOutput => {
        const vehicleType = element.type === "bike" ? "bicycle" : element.type;
        return {
            id: REKOLA_PREFIX + element.type,
            form_factor: vehicleType,
            propulsion_type: "human",
            max_range_meters: null,
            name: vehicleType,
        };
    };
}
