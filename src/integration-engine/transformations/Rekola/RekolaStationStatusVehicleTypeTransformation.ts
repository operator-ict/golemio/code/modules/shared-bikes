import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { IRekolaTrackablesDatasourceRack, ISharedBikesStationStatusVehicleTypeOutput, SharedBikes } from "#sch";
import { REKOLA_PREFIX } from "#ie/transformations/Rekola/transformConstants";

export class RekolaStationStatusVehicleTypeTransformation extends BaseTransformation implements ITransformation {
    public name: string = SharedBikes.datasources.rekolaTrackables.name + "StationStatusVehicleTypeTransformation";

    public transform = async (data: IRekolaTrackablesDatasourceRack[]): Promise<ISharedBikesStationStatusVehicleTypeOutput[]> => {
        const output: ISharedBikesStationStatusVehicleTypeOutput[] = [];

        for (const rack of data) {
            if (rack.isVisible) {
                output.push(...this.transformElement(rack));
            }
        }

        return output;
    };

    protected transformElement = (rack: IRekolaTrackablesDatasourceRack): ISharedBikesStationStatusVehicleTypeOutput[] => {
        const output: ISharedBikesStationStatusVehicleTypeOutput[] = [];
        for (const vehicleTypeId of this.getVehicleTypeIdsFromRack(rack)) {
            const stationStatusVehicleType: ISharedBikesStationStatusVehicleTypeOutput = {
                station_id: REKOLA_PREFIX + rack.id,
                vehicle_type_id: REKOLA_PREFIX + vehicleTypeId,
                count: rack.vehicles.length,
                num_bikes_available: rack.vehicles.filter((vehicle) => vehicle.type === vehicleTypeId).length,
                num_bikes_disabled: null,
                vehicle_docks_available: null,
            };
            output.push(stationStatusVehicleType);
        }
        return output;
    };

    private getVehicleTypeIdsFromRack = (rack: IRekolaTrackablesDatasourceRack): string[] => {
        const vehicleTypes: string[] = [];
        for (const vehicle of rack.vehicles) {
            if (!vehicleTypes.includes(vehicle.type)) {
                vehicleTypes.push(vehicle.type);
            }
        }
        return vehicleTypes;
    };
}
