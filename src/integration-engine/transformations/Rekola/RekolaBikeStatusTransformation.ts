import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { IRekolaTrackablesDatasource, IRekolaTrackablesDatasourceVehicle, ISharedBikesBikeStatusOutput, SharedBikes } from "#sch";
import { REKOLA_PREFIX, REKOLA_PRICING_PLAN_ID, REKOLA_SYSTEM_ID } from "#ie/transformations/Rekola/transformConstants";

export class RekolaBikeStatusTransformation extends BaseTransformation implements ITransformation {
    public name: string = SharedBikes.datasources.rekolaTrackables.name + "BikeStatusTransformation";

    public transform = async (data: {
        trackables: IRekolaTrackablesDatasource;
        transformationDate: Date;
    }): Promise<ISharedBikesBikeStatusOutput[]> => {
        const bikeStatus: ISharedBikesBikeStatusOutput[] = [];

        for (const rack of data.trackables.racks) {
            for (const bike of rack.vehicles) {
                const bikeInRack = this.transformElement({
                    bike,
                    stationId: REKOLA_PREFIX + rack.id,
                    transformationDate: data.transformationDate,
                });
                bikeStatus.push(bikeInRack);
            }
        }

        for (const bike of data.trackables.vehicles) {
            bikeStatus.push(this.transformElement({ bike, stationId: null, transformationDate: data.transformationDate }));
        }

        return bikeStatus;
    };

    protected transformElement = (element: {
        bike: IRekolaTrackablesDatasourceVehicle;
        stationId: string | null;
        transformationDate: Date;
    }): ISharedBikesBikeStatusOutput => {
        return {
            id: REKOLA_PREFIX + element.bike.id,
            system_id: REKOLA_SYSTEM_ID,
            point: {
                type: "Point",
                coordinates: [element.bike.position.lng, element.bike.position.lat],
            },
            helmets: null,
            passengers: null,
            damage_description: null,
            description: element.bike.label,
            vehicle_registration: null,
            is_reserved: element.bike.isBorrowed,
            is_disabled: false,
            vehicle_type_id: REKOLA_PREFIX + element.bike.type,
            last_reported: element.transformationDate.toISOString(),
            current_range_meters: null,
            charge_percent: null,
            rental_app_id: null,
            station_id: element.stationId,
            pricing_plan_id: REKOLA_PRICING_PLAN_ID,
            make: null,
            model: null,
            color: null,
        };
    };
}
