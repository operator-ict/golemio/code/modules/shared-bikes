import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { IRekolaTrackablesDatasourceRack, ISharedBikesStationInformationOutput, SharedBikes } from "#sch";
import { REKOLA_PREFIX, REKOLA_SYSTEM_ID } from "#ie/transformations/Rekola/transformConstants";

export class RekolaStationInformationTransformation extends BaseTransformation implements ITransformation {
    public name: string = SharedBikes.datasources.rekolaTrackables.name + "StationInformationTransformation";

    public transform = async (data: IRekolaTrackablesDatasourceRack[]): Promise<ISharedBikesStationInformationOutput[]> => {
        const stations: ISharedBikesStationInformationOutput[] = [];

        for (const rack of data) {
            if (rack.isVisible) {
                stations.push(this.transformElement(rack));
            }
        }

        return stations;
    };

    protected transformElement = (element: IRekolaTrackablesDatasourceRack): ISharedBikesStationInformationOutput => {
        return {
            id: REKOLA_PREFIX + element.id,
            system_id: REKOLA_SYSTEM_ID,
            name: element.name,
            point: {
                type: "Point",
                coordinates: [element.position.lng, element.position.lat],
            },
            address: null,
            post_code: null,
            cross_street: null,
            region_id: null,
            rental_methods: null,
            is_virtual_station: true,
            station_area: null,
            capacity: null,
            vehicle_capacity: null,
            vehicle_type_capacity: null,
            is_valet_station: null,
            rental_app_id: null,
        };
    };
}
