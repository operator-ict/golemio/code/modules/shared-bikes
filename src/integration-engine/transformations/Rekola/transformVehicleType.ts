import { IRekolaTrackablesDatasourceVehicle, ISharedBikesVehicleTypeOutput } from "#sch";
import { REKOLA_PREFIX } from "#ie/transformations/Rekola/transformConstants";

export function transformVehicleType(element: IRekolaTrackablesDatasourceVehicle): ISharedBikesVehicleTypeOutput {
    const vehicleType = element.type === "bike" ? "bicycle" : element.type;
    return {
        id: REKOLA_PREFIX + element.type,
        form_factor: vehicleType,
        propulsion_type: "human",
        max_range_meters: null,
        name: vehicleType,
    };
}
