import { getAjoPricingPlanId } from "#ie/transformations/CeskyCarsharing/AjoPricingHelper";
import { getAnytimePricingPlanId } from "#ie/transformations/CeskyCarsharing/AnytimePricingHelper";
import { getAutonapulPricingPlanId } from "#ie/transformations/CeskyCarsharing/AutonapulPricingHelper";
import { getCar4WayPricingPlanId } from "#ie/transformations/CeskyCarsharing/Car4WayPricingHelper";
import { getSystemIdByCeskyCarsharingCompanyId } from "#ie/transformations/CeskyCarsharing/CeskyCarsharingSystemIdentifier";
import {
    AJO_RENTAL_APP_ID,
    ANYTIME_RENTAL_APP_ID,
    AUTONAPUL_RENTAL_APP_ID,
    CAR4WAY_RENTAL_APP_ID,
    CESKY_CARSHARING_ID_PREFIX,
    CESKY_CARSHARING_VEHICLE_TYPE_DIESEL,
    CESKY_CARSHARING_VEHICLE_TYPE_ELECTRIC,
    CESKY_CARSHARING_VEHICLE_TYPE_HYBRID,
    CESKY_CARSHARING_VEHICLE_TYPE_LPG,
    CESKY_CARSHARING_VEHICLE_TYPE_PETROL,
} from "#ie/transformations/CeskyCarsharing/transformConstants";
import { getWhereGeometry } from "#ie/transformations/helpers/getWhereGeometry";
import { ISharedBikesBikeStatusOutput, SharedBikes } from "#sch";
import { ICeskyCarsharingCar } from "#sch/datasources";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { Point } from "geojson";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { log } from "@golemio/core/dist/integration-engine";

interface CarMakeModel {
    make: string;
    model: string;
}

@injectable()
export class CeskyCarsharingBikeStatusTransformation extends BaseTransformation implements ITransformation {
    public name: string = SharedBikes.datasources.HoppyGoVehiclesJsonSchema.name + "BikeStatusTransformation";

    public transform = async (data: {
        cars: ICeskyCarsharingCar[];
        transformationDate: Date;
    }): Promise<ISharedBikesBikeStatusOutput[]> => {
        const bikeStatus: ISharedBikesBikeStatusOutput[] = [];

        for (const car of data.cars) {
            try {
                const transformedCar = this.transformElement({
                    car,
                    transformationDate: data.transformationDate,
                });
                bikeStatus.push(transformedCar);
            } catch (err) {
                log.debug(err);
            }
        }

        return bikeStatus;
    };

    protected transformElement = (element: {
        car: ICeskyCarsharingCar;
        transformationDate: Date;
    }): ISharedBikesBikeStatusOutput => {
        const makeModel = this.getCarMakeModel(element.car);

        return {
            id: CESKY_CARSHARING_ID_PREFIX + element.car.rz,
            system_id: getSystemIdByCeskyCarsharingCompanyId(element.car.company_id),
            point: this.getCarLocation(element.car),
            helmets: null,
            passengers: null,
            damage_description: null,
            description: element.car.car_name,
            vehicle_registration: element.car.rz,
            is_reserved: false,
            is_disabled: false,
            vehicle_type_id: this.getVehicleTypeId(element.car),
            last_reported: element.transformationDate.toISOString(),
            current_range_meters: null,
            charge_percent: null,
            rental_app_id: this.getRentalAppIdByCompanyId(element.car.company_id),
            station_id: null,
            pricing_plan_id: this.getPricingPlanId(element.car),
            make: makeModel.make,
            model: makeModel.model,
            color: null,
        };
    };

    private getRentalAppIdByCompanyId(company_id: number): string {
        switch (company_id) {
            case 2:
                return AJO_RENTAL_APP_ID;
            case 3:
                return AUTONAPUL_RENTAL_APP_ID;
            case 5:
                return CAR4WAY_RENTAL_APP_ID;
            case 9:
                return ANYTIME_RENTAL_APP_ID;
            default:
                return "";
        }
    }

    private getCarLocation = (car: ICeskyCarsharingCar): Point => {
        return getWhereGeometry(Number(car.latitude), Number(car.longitude));
    };

    private getVehicleTypeId = (car: ICeskyCarsharingCar): string => {
        switch (car.fuel) {
            case 1:
                return CESKY_CARSHARING_VEHICLE_TYPE_PETROL;
            case 2:
                return CESKY_CARSHARING_VEHICLE_TYPE_DIESEL;
            case 3:
                return CESKY_CARSHARING_VEHICLE_TYPE_LPG;
            case 4:
                return CESKY_CARSHARING_VEHICLE_TYPE_ELECTRIC;
            case 5:
                return CESKY_CARSHARING_VEHICLE_TYPE_HYBRID;
            default:
                return CESKY_CARSHARING_VEHICLE_TYPE_PETROL;
        }
    };

    private getCarMakeModel = (car: ICeskyCarsharingCar): CarMakeModel => {
        switch (car.car_name) {
            case "Fabia III kombi  ":
                return {
                    make: "Škoda",
                    model: "Fabia III kombi",
                };
            case "Logan MCV kombi  ":
                return {
                    make: "Dacia",
                    model: "Logan MCV",
                };
            case "SUV Sportage  ":
                return {
                    make: "Kia",
                    model: "Sportage SUV",
                };
            case "jiné Sandero":
                return {
                    make: "Dacia",
                    model: "Sandero",
                };
            case "jiné Model":
                return {
                    make: "Tesla",
                    model: "Model 3",
                };
            case "jiné Corolla":
                return {
                    make: "Toyota",
                    model: "Corolla",
                };
            case "jiné i30":
                return {
                    make: "Hyundai",
                    model: "i30",
                };
            default:
                const make = car.car_name.split(" ")[0];
                return {
                    make,
                    model: car.car_name.slice(make.length).trim(),
                };
        }
    };

    private getPricingPlanId = (car: ICeskyCarsharingCar): string => {
        switch (car.company_id) {
            case 2:
                return getAjoPricingPlanId(car);
            case 3:
                return getAutonapulPricingPlanId(car);
            case 5:
                return getCar4WayPricingPlanId(car);
            case 9:
                return getAnytimePricingPlanId(car);
        }
        throw new GeneralError(`Could not find pricing plan id for ${car.company_name}`);
    };
}
