import { AJO_PRICING_PLAN_ID } from "#ie/transformations/CeskyCarsharing/transformConstants";
import { ICeskyCarsharingCar } from "#sch/datasources";

export function getAjoPricingPlanId(car: ICeskyCarsharingCar): string {
    if (car.car_name.includes("SUV")) {
        return AJO_PRICING_PLAN_ID.TARIF_TOP_SUV;
    }
    return AJO_PRICING_PLAN_ID.TARIF_TOP;
}
