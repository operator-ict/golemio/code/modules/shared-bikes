import { ICeskyCarsharingCar } from "#sch/datasources";
import { AUTONAPUL_PRICING_PLAN_ID } from "#ie/transformations/CeskyCarsharing/transformConstants";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";

export function getAutonapulPricingPlanId(car: ICeskyCarsharingCar): string {
    if (car.car_name.includes("Zoe") || car.car_name.includes("CitigoE") || car.car_name.includes("Spring")) {
        return AUTONAPUL_PRICING_PLAN_ID.ELECTRO;
    }

    if (car.car_name.includes("Citigo") || car.car_name.includes("Sandero") || car.car_name.includes("Fabia")) {
        return AUTONAPUL_PRICING_PLAN_ID.ECONOMY;
    }

    if (car.car_name.includes("Rapid") || car.car_name.includes("Megane") || car.car_name.includes("i30")) {
        return AUTONAPUL_PRICING_PLAN_ID.STANDARD;
    }

    if (car.car_name.includes("Transit") || car.car_name.includes("Trafic")) {
        return AUTONAPUL_PRICING_PLAN_ID.GRAND;
    }

    if (car.car_name.includes("Scala") || car.car_name.includes("Octavia") || car.car_name.includes("Corolla")) {
        return AUTONAPUL_PRICING_PLAN_ID.COMFORT;
    }

    if (car.car_name === "jiné Model" || car.car_name.includes("Model 3")) {
        return AUTONAPUL_PRICING_PLAN_ID.TESLA;
    }

    throw new GeneralError(`No pricing plan for ${car.car_name} by Autonapůl Carsharing.`);
}
