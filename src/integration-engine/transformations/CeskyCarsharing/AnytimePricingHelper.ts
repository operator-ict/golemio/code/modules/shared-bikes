import { ICeskyCarsharingCar } from "#sch/datasources";
import { ANYTIME_PRICING_PLAN_ID } from "#ie/transformations/CeskyCarsharing/transformConstants";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";

export function getAnytimePricingPlanId(car: ICeskyCarsharingCar): string {
    if (car.car_name.includes("Yaris")) {
        return ANYTIME_PRICING_PLAN_ID.YARIS;
    }
    if (car.car_name.includes("Corolla")) {
        return ANYTIME_PRICING_PLAN_ID.COROLLA;
    }
    if (car.car_name.includes("C-HR")) {
        return ANYTIME_PRICING_PLAN_ID.CHR;
    }
    if (car.car_name.includes("BMW")) {
        return ANYTIME_PRICING_PLAN_ID.BMW5;
    }
    if (car.car_name.includes("SQ8")) {
        return ANYTIME_PRICING_PLAN_ID.SQ8;
    }
    throw new GeneralError(`No pricing plan for ${car.car_name} by Anytime Carsharing.`);
}
