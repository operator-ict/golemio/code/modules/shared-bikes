export const ANYTIME_SYSTEM_ID = "anytime";
export const AJO_SYSTEM_ID = "ajo";
export const AUTONAPUL_SYSTEM_ID = "autonapul";
export const CAR4WAY_SYSTEM_ID = "car4way";
export const CESKY_CARSHARING_ID_PREFIX = "cesky-carsharing-";
export const ANYTIME_RENTAL_APP_ID = "anytime_rental_apps";
export const CAR4WAY_RENTAL_APP_ID = "car4way_rental_apps";
export const AJO_RENTAL_APP_ID = "ajo_rental_apps";
export const AUTONAPUL_RENTAL_APP_ID = "autonapul_rental_apps";
export const CESKY_CARSHARING_VEHICLE_TYPE_ELECTRIC = "carsharing_electric";
export const CESKY_CARSHARING_VEHICLE_TYPE_PETROL = "carsharing_benzin";
export const CESKY_CARSHARING_VEHICLE_TYPE_DIESEL = "carsharing_diesel";
export const CESKY_CARSHARING_VEHICLE_TYPE_HYBRID = "carsharing_hybrid";
export const CESKY_CARSHARING_VEHICLE_TYPE_LPG = "carsharing_benzin_LPG";
export const AJO_PRICING_PLAN_ID = Object.freeze({
    TARIF_TOP: "ajo_tarif_top",
    TARIF_TOP_SUV: "ajo_tarif_top_suv",
});

export const ANYTIME_PRICING_PLAN_ID = Object.freeze({
    YARIS: "anytime_yaris",
    COROLLA: "anytime_corolla",
    CHR: "anytime_chr",
    BMW5: "anytime_bmw5",
    SQ8: "anytime_sq8",
});

export const AUTONAPUL_PRICING_PLAN_ID = Object.freeze({
    ECONOMY: "autonapul_economy",
    STANDARD: "autonapul_standard",
    COMFORT: "autonapul_comfort",
    GRAND: "autonapul_grand",
    ELECTRO: "autonapul_electro",
    TESLA: "autonapul_tesla",
});

export const CAR4WAY_PRICING_PLAN_ID = Object.freeze({
    STANDARD: "car4way_standard",
    OPTIMUM: "car4way_optimum",
    SUV: "car4way_suv",
    BUSINESS: "car4way_business",
    MASTER: "car4way_master",
    TOP: "car4way_top",
});
