import { ICeskyCarsharingCar } from "#sch/datasources";
import { CAR4WAY_PRICING_PLAN_ID } from "#ie/transformations/CeskyCarsharing/transformConstants";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";

export function getCar4WayPricingPlanId(car: ICeskyCarsharingCar): string {
    const carName = car.car_name.toLowerCase();

    if (carName.includes("fabia")) {
        return CAR4WAY_PRICING_PLAN_ID.STANDARD;
    }

    if (carName.includes("scala")) {
        return CAR4WAY_PRICING_PLAN_ID.OPTIMUM;
    }

    if (carName.includes("karoq")) {
        return CAR4WAY_PRICING_PLAN_ID.SUV;
    }

    if (carName.includes("tucson")) {
        return CAR4WAY_PRICING_PLAN_ID.SUV;
    }

    if (carName.includes("octavia")) {
        return CAR4WAY_PRICING_PLAN_ID.BUSINESS;
    }

    if (carName.includes("caddy")) {
        return CAR4WAY_PRICING_PLAN_ID.MASTER;
    }

    if (carName.includes("i30")) {
        return CAR4WAY_PRICING_PLAN_ID.OPTIMUM;
    }

    if (carName.includes("superb")) {
        return CAR4WAY_PRICING_PLAN_ID.TOP;
    }

    if (carName.includes("a4")) {
        return CAR4WAY_PRICING_PLAN_ID.TOP;
    }

    if (carName.includes("q3")) {
        return CAR4WAY_PRICING_PLAN_ID.TOP;
    }

    if (carName.includes("kodiaq")) {
        return CAR4WAY_PRICING_PLAN_ID.TOP;
    }

    throw new GeneralError(`No pricing plan for ${car.car_name} by Car4Way Carsharing.`);
}
