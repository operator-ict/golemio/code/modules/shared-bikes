import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { ISharedBikesGeofencingZoneOutput, SharedBikes } from "#sch";
import { ICeskyCarsharingPolygon } from "#sch/datasources/CeskyCarsharingGeofencingZone";
import getUuidByString from "uuid-by-string";
import { getSystemIdByCeskyCarsharingCompanyId } from "#ie/transformations/CeskyCarsharing/CeskyCarsharingSystemIdentifier";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class CeskyCarsharingGeofencingTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = SharedBikes.datasources.CeskyCarsharingGeofencingJsonSchema.name;
    }

    public transform = (data: ICeskyCarsharingPolygon[]): Promise<ISharedBikesGeofencingZoneOutput[]> => {
        const res: ISharedBikesGeofencingZoneOutput[] = [];

        for (const polygon of data) {
            res.push(this.transformElement(polygon));
        }

        return Promise.resolve(res);
    };

    /**
     * Transform zones
     */
    protected transformElement = (element: ICeskyCarsharingPolygon): ISharedBikesGeofencingZoneOutput => {
        return {
            id: getUuidByString(this.getPolygonSum(element)),
            system_id: getSystemIdByCeskyCarsharingCompanyId(element.company_id),
            name: null,
            note: null,
            source: null,
            price: null,
            priority: 0,
            start: null,
            end: null,
            geom: {
                type: "MultiPolygon",
                coordinates: [[element.polygon.map((point) => [Number(point.lon), Number(point.lat)])]],
            },
            ride_allowed: true,
            ride_through_allowed: true,
            maximum_speed_kph: null,
            parking_allowed: true,
        };
    };

    private getPolygonSum = (element: ICeskyCarsharingPolygon): string => {
        let polygonSum: string = String(element.company_id);
        element.polygon.forEach((currentPolygon) => {
            polygonSum += currentPolygon.lon + currentPolygon.lat;
        });
        return polygonSum;
    };
}
