import { ISharedBikesVehicleTypeOutput } from "#sch";
import {
    CESKY_CARSHARING_VEHICLE_TYPE_DIESEL,
    CESKY_CARSHARING_VEHICLE_TYPE_ELECTRIC,
    CESKY_CARSHARING_VEHICLE_TYPE_HYBRID,
    CESKY_CARSHARING_VEHICLE_TYPE_LPG,
    CESKY_CARSHARING_VEHICLE_TYPE_PETROL,
} from "#ie/transformations/CeskyCarsharing/transformConstants";

export function getVehicleTypes(): ISharedBikesVehicleTypeOutput[] {
    return [
        {
            id: CESKY_CARSHARING_VEHICLE_TYPE_ELECTRIC,
            form_factor: "car",
            propulsion_type: "electric",
            max_range_meters: null,
            name: "Carsharing Electric",
        },
        {
            id: CESKY_CARSHARING_VEHICLE_TYPE_PETROL,
            form_factor: "car",
            propulsion_type: "combustion",
            max_range_meters: null,
            name: "Carsharing Benzin",
        },
        {
            id: CESKY_CARSHARING_VEHICLE_TYPE_DIESEL,
            form_factor: "car",
            propulsion_type: "combustion",
            max_range_meters: null,
            name: "Carsharing Diesel",
        },
        {
            id: CESKY_CARSHARING_VEHICLE_TYPE_HYBRID,
            form_factor: "car",
            propulsion_type: "combustion",
            max_range_meters: null,
            name: "Carsharing Hybrid",
        },
        {
            id: CESKY_CARSHARING_VEHICLE_TYPE_LPG,
            form_factor: "car",
            propulsion_type: "combustion",
            max_range_meters: null,
            name: "Carsharing benzin + LPG",
        },
    ];
}
