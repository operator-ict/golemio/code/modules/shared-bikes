import {
    AJO_RENTAL_APP_ID,
    AJO_SYSTEM_ID,
    ANYTIME_RENTAL_APP_ID,
    ANYTIME_SYSTEM_ID,
    AUTONAPUL_RENTAL_APP_ID,
    AUTONAPUL_SYSTEM_ID,
    CAR4WAY_RENTAL_APP_ID,
    CAR4WAY_SYSTEM_ID,
} from "#ie/transformations/CeskyCarsharing/transformConstants";
import { IMobilityOperatorRentalAppMap } from "#sch/datasources/MobilityOperator";

export function getRentalApps(): IMobilityOperatorRentalAppMap {
    return {
        [CAR4WAY_SYSTEM_ID]: {
            id: CAR4WAY_RENTAL_APP_ID,
            android_store_url: "https://play.google.com/store/apps/details?id=cz.car4way.car4way",
            android_discovery_url: null,
            ios_store_url: "https://itunes.apple.com/us/app/car4way/id1043539052?l=cs&ls=1&mt=8",
            ios_discovery_url: null,
            web_url: null,
        },
        [ANYTIME_SYSTEM_ID]: {
            id: ANYTIME_RENTAL_APP_ID,
            android_store_url: "https://go.onelink.me/app/3928b209",
            android_discovery_url: null,
            ios_store_url: "https://go.onelink.me/app/f4680df5",
            ios_discovery_url: null,
            web_url: null,
        },
        [AJO_SYSTEM_ID]: {
            id: AJO_RENTAL_APP_ID,
            android_store_url: null,
            android_discovery_url: null,
            ios_store_url: null,
            ios_discovery_url: null,
            web_url: "https://www.ajo.cz/login",
        },
        [AUTONAPUL_SYSTEM_ID]: {
            id: AUTONAPUL_RENTAL_APP_ID,
            android_store_url: "https://play.google.com/store/apps/details?id=autonapul.carsharing",
            android_discovery_url: null,
            ios_store_url: "https://apps.apple.com/app/id1524778044",
            ios_discovery_url: null,
            web_url: "https://autonapul.zemtu.com/login/?next=/app/#/",
        },
    };
}
