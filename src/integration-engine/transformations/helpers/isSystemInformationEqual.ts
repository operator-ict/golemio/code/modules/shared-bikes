import { ISharedBikesSystemInformationOutput } from "#sch";

export function isSystemInformationEqual(
    objA: ISharedBikesSystemInformationOutput,
    objB: ISharedBikesSystemInformationOutput
): boolean {
    for (const key in objA) {
        if (objA[key] !== objB[key]) {
            return false;
        }
    }
    return true;
}
