import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { ISharedBikesPricingPlanOutput, SharedBikes } from "#sch";
import { IHoppyGoVehicle } from "#sch/datasources";
import { HOPPYGO_PRICING_PLAN_PREFIX, HOPPYGO_SYSTEM_ID } from "#ie/transformations/HoppyGo/transformConstants";

export class HoppyGoPricingPlansTransformation extends BaseTransformation implements ITransformation {
    public name: string = SharedBikes.datasources.HoppyGoVehiclesJsonSchema.name + "PricingPlansTransformation";

    public transform = async (data: {
        cars: IHoppyGoVehicle[];
        transformationDate: Date;
    }): Promise<ISharedBikesPricingPlanOutput[]> => {
        const pricingPlans: ISharedBikesPricingPlanOutput[] = [];

        for (const car of data.cars) {
            const pricingPlan = this.transformElement({ car, transformationDate: data.transformationDate });
            pricingPlans.push(pricingPlan);
        }

        return pricingPlans;
    };

    protected transformElement = (element: { car: IHoppyGoVehicle; transformationDate: Date }): ISharedBikesPricingPlanOutput => {
        return {
            id: HOPPYGO_PRICING_PLAN_PREFIX + element.car.id,
            system_id: HOPPYGO_SYSTEM_ID,
            url: null,
            last_updated: element.transformationDate.toISOString(),
            name: `Ceník HoppyGo ${element.car.id}`,
            currency: "CZK",
            price: element.car.price_per_day,
            is_taxable: false,
            description: `Cena za den ${element.car.price_per_day} Kč`,
            surge_pricing: false,
        };
    };
}
