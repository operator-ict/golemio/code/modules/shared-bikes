import { ISharedBikesVehicleTypeOutput } from "#sch";
import { HOPPYGO_VEHICLE_TYPE_COMBUSTION, HOPPYGO_VEHICLE_TYPE_ELECTRIC } from "#ie/transformations/HoppyGo/transformConstants";

export function getVehicleTypes(): ISharedBikesVehicleTypeOutput[] {
    return [
        {
            id: HOPPYGO_VEHICLE_TYPE_ELECTRIC,
            form_factor: "car",
            propulsion_type: "electric",
            max_range_meters: null,
            name: "HoppyGo Car Electric",
        },
        {
            id: HOPPYGO_VEHICLE_TYPE_COMBUSTION,
            form_factor: "car",
            propulsion_type: "combustion",
            max_range_meters: null,
            name: "HoppyGo Car Combustion",
        },
    ];
}
