import { ISharedBikesRentalAppOutput } from "#sch";
import { HOPPYGO_RENTAL_APP_ID } from "#ie/transformations/HoppyGo/transformConstants";

export function getRentalApps(): ISharedBikesRentalAppOutput {
    return {
        id: HOPPYGO_RENTAL_APP_ID,
        android_store_url: "https://play.google.com/store/apps/details?id=cz.nom.smilecarweb",
        android_discovery_url: null,
        ios_store_url: "https://itunes.apple.com/cz/app/hoppygo/id1184613465?l=cs&mt=8",
        ios_discovery_url: null,
        web_url: null,
    };
}
