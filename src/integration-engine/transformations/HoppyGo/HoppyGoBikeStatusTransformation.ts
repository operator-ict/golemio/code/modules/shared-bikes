import { getWhereGeometry } from "#ie/transformations/helpers/getWhereGeometry";
import {
    HOPPYGO_ID_PREFIX,
    HOPPYGO_PRICING_PLAN_PREFIX,
    HOPPYGO_RENTAL_APP_ID,
    HOPPYGO_SYSTEM_ID,
    HOPPYGO_VEHICLE_TYPE_COMBUSTION,
    HOPPYGO_VEHICLE_TYPE_ELECTRIC,
} from "#ie/transformations/HoppyGo/transformConstants";
import { ISharedBikesBikeStatusOutput, SharedBikes } from "#sch";
import { IHoppyGoVehicle } from "#sch/datasources";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { Point } from "geojson";

export class HoppyGoBikeStatusTransformation extends BaseTransformation implements ITransformation {
    public name: string = SharedBikes.datasources.HoppyGoVehiclesJsonSchema.name + "BikeStatusTransformation";

    public transform = async (data: {
        cars: IHoppyGoVehicle[];
        transformationDate: Date;
    }): Promise<ISharedBikesBikeStatusOutput[]> => {
        const bikeStatus: ISharedBikesBikeStatusOutput[] = [];

        for (const car of data.cars) {
            const transformedCar = this.transformElement({
                car,
                transformationDate: data.transformationDate,
            });
            bikeStatus.push(transformedCar);
        }

        return bikeStatus;
    };

    protected transformElement = (element: { car: IHoppyGoVehicle; transformationDate: Date }): ISharedBikesBikeStatusOutput => {
        return {
            id: HOPPYGO_ID_PREFIX + element.car.id,
            system_id: HOPPYGO_SYSTEM_ID,
            point: this.getCarLocation(element.car.localization),
            helmets: null,
            passengers: null,
            damage_description: null,
            description: `${element.car.manufacturer_name} ${element.car.model_name}`,
            vehicle_registration: null,
            is_reserved: false,
            is_disabled: false,
            vehicle_type_id: this.getVehicleTypeId(element.car),
            last_reported: element.transformationDate.toISOString(),
            current_range_meters: null,
            charge_percent: null,
            rental_app_id: HOPPYGO_RENTAL_APP_ID,
            station_id: null,
            pricing_plan_id: HOPPYGO_PRICING_PLAN_PREFIX + element.car.id,
            make: element.car.manufacturer_name,
            model: element.car.model_name,
            color: null,
        };
    };

    private getCarLocation = (localization: string): Point => {
        const coords = localization.split(",");
        return getWhereGeometry(Number(coords[0]), Number(coords[1]));
    };

    private getVehicleTypeId = (car: IHoppyGoVehicle): string => {
        return car.fuel_description === "electric" ? HOPPYGO_VEHICLE_TYPE_ELECTRIC : HOPPYGO_VEHICLE_TYPE_COMBUSTION;
    };
}
