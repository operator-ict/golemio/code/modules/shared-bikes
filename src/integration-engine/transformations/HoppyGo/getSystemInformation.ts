import { ISharedBikesSystemInformationOutput } from "#sch";
import { HOPPYGO_INTEGRATION_DATE, HOPPYGO_SYSTEM_ID } from "#ie/transformations/HoppyGo/transformConstants";

export function getSystemInformation(rentalAppId: string): ISharedBikesSystemInformationOutput {
    return {
        operator_id: "hoppygo",
        system_id: HOPPYGO_SYSTEM_ID,
        language: "cs",
        logo: "https://scontent.xx.fbcdn.net/v/t1.6435-1/cp0/p80x80/36342579_1573737729401511_169288730132086784_n.png?_nc_cat=110&ccb=1-5&_nc_sid=dbb9e7&_nc_ohc=7vzGRNqrDC8AX9GLL2p&_nc_ht=scontent.xx&edm=ACzcWGEEAAAA&oh=fdf63a63daad762708153fcea1c1d47f&oe=61AFD68B",
        name: "HoppyGo",
        short_name: "HoppyGo",
        operator: null,
        url: null,
        purchase_url: "https://hoppygo.com/cs/",
        start_date: new Date(HOPPYGO_INTEGRATION_DATE),
        phone_number: "+420 220 311 769",
        email: "info@hoppygo.com",
        feed_contact_email: "info@hoppygo.com",
        timezone: "Europe/Prague",
        license_id: null,
        license_url: "https://iptoict.blob.core.windows.net/storage/Legal/GBFSLicence.txt",
        attribution_organization_name: null,
        attribution_url: null,
        terms_of_use_url: "https://www.hoppygo.com/files/cz/HoppyGo_VOP_20221027.pdf",
        rental_app_id: rentalAppId,
    };
}
