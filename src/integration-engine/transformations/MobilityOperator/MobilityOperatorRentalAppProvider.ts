import { DataSource } from "@golemio/core/dist/integration-engine/datasources";
import { IMobilityOperator, IMobilityOperatorRentalAppMap } from "#sch/datasources/MobilityOperator";
import { MobilityOperatorTransformation } from "#ie/transformations/MobilityOperator/MobilityOperatorTransformation";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { ModuleContainerToken } from "#ie/ioc";

@injectable()
export class MobilityOperatorRentalAppProvider {
    constructor(
        @inject(ModuleContainerToken.MobilityOperatorDataSource) private dataSource: DataSource,
        @inject(ModuleContainerToken.MobilityOperatorTransformation) private transformation: MobilityOperatorTransformation
    ) {}

    public async getMobilityOperatorRentalApps(): Promise<IMobilityOperatorRentalAppMap> {
        const dataFromSource: IMobilityOperator[] = await this.dataSource.getAll();
        return this.transformation.transform(dataFromSource);
    }
}
