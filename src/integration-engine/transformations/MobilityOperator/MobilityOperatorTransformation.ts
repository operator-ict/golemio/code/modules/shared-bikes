import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import { ISharedBikesRentalAppOutput, SharedBikes } from "#sch";
import { IMobilityOperator, IMobilityOperatorRentalAppMap } from "#sch/datasources/MobilityOperator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import {
    AJO_RENTAL_APP_ID,
    ANYTIME_RENTAL_APP_ID,
    AUTONAPUL_RENTAL_APP_ID,
    CAR4WAY_RENTAL_APP_ID,
} from "#ie/transformations/CeskyCarsharing/transformConstants";
import { HOPPYGO_RENTAL_APP_ID } from "#ie/transformations/HoppyGo/transformConstants";

@injectable()
export class MobilityOperatorTransformation extends BaseTransformation implements ITransformation {
    public name: string;
    public idPrefix: string = "mobility-operator-app";

    constructor() {
        super();
        this.name = SharedBikes.datasources.MobilityOperatorJsonSchema.name;
    }

    public transform = (data: IMobilityOperator[]): Promise<IMobilityOperatorRentalAppMap> => {
        let result: IMobilityOperatorRentalAppMap = {};

        for (const dataItem of data) {
            if (dataItem.system_id !== null) {
                result[dataItem.system_id] = this.transformElement(dataItem);
            }
        }

        return Promise.resolve(result);
    };

    protected transformElement = (element: IMobilityOperator): ISharedBikesRentalAppOutput => {
        return {
            id: this.getRentalAppIdByMobiltyOperatorId(element.id),
            android_store_url: element.androidAppUrl,
            android_discovery_url: element.androidDiscoveryUrl,
            ios_store_url: element.iosAppUrl,
            ios_discovery_url: element.iosDiscoveryUrl,
            web_url: element.webUrl,
        };
    };

    private getRentalAppIdByMobiltyOperatorId(operatorId: string): string {
        switch (operatorId) {
            case "ajo":
                return AJO_RENTAL_APP_ID;
            case "autonapul":
                return AUTONAPUL_RENTAL_APP_ID;
            case "car4way":
                return CAR4WAY_RENTAL_APP_ID;
            case "anytime":
                return ANYTIME_RENTAL_APP_ID;
            case "hoppygo":
                return HOPPYGO_RENTAL_APP_ID;
            case "rekola":
                return "bb622abc-84a1-11ec-a8a3-0242ac120002";
            default:
                return this.idPrefix + "-" + operatorId;
        }
    }
}
