import { HoppyGoDataSourceFactory } from "#ie/dataSources/HoppyGoDataSourceFactory";
import { HoppyGoStaticDataService } from "#ie/HoppyGoStaticDataService";
import { HoppyGoBikeStatusTransformation } from "#ie/transformations/HoppyGo/HoppyGoBikeStatusTransformation";
import { HoppyGoPricingPlansTransformation } from "#ie/transformations/HoppyGo/HoppyGoPricingPlansTransformation";
import { MobilityOperatorRentalAppProvider } from "#ie/transformations/MobilityOperator/MobilityOperatorRentalAppProvider";
import { IHoppyGoVehicle } from "#sch/datasources";
import { BaseWorker, DataSource } from "@golemio/core/dist/integration-engine";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { ModuleContainerToken, SharedBikesContainer } from "./ioc";
import { BikeStatusModel } from "./models/BikeStatusModel";
import { PricingPlansModel } from "./models/PricingPlansModel";
import { HOPPYGO_SYSTEM_ID } from "#ie/transformations/HoppyGo/transformConstants";
import { OldDataCleanerHelper } from "#ie/helpers/OldDataCleanerHelper";

export class HoppyGoSharedCarsWorker extends BaseWorker {
    private dataSource: DataSource;
    private staticDataService: HoppyGoStaticDataService;

    private pricingPlansTransformation: HoppyGoPricingPlansTransformation;
    private pricingPlansModel: PricingPlansModel;

    private bikeStatusTransformation: HoppyGoBikeStatusTransformation;
    private bikeStatusModel: BikeStatusModel;
    private oldDataCleanerHelper: OldDataCleanerHelper;

    constructor() {
        super();
        this.dataSource = HoppyGoDataSourceFactory.getDataSource();
        this.staticDataService = new HoppyGoStaticDataService(
            SharedBikesContainer.resolve<MobilityOperatorRentalAppProvider>(
                ModuleContainerToken.MobilityOperatorRentalAppProvider
            )
        );
        this.pricingPlansTransformation = new HoppyGoPricingPlansTransformation();
        this.pricingPlansModel = new PricingPlansModel();
        this.bikeStatusTransformation = new HoppyGoBikeStatusTransformation();
        this.bikeStatusModel = new BikeStatusModel();
        this.oldDataCleanerHelper = SharedBikesContainer.resolve<OldDataCleanerHelper>(ModuleContainerToken.OldDataCleanerHelper);
    }

    public refreshHoppyGoSharedCarsData = async (): Promise<void> => {
        const transformationDate = new Date();

        try {
            const cars: IHoppyGoVehicle[] = await this.dataSource.getAll();
            await this.staticDataService.saveSystemBasics();

            const pricingPlans = await this.pricingPlansTransformation.transform({
                cars,
                transformationDate,
            });
            await this.pricingPlansModel.save(pricingPlans);

            const vehicleStatus = await this.bikeStatusTransformation.transform({
                cars,
                transformationDate,
            });
            await this.bikeStatusModel.saveIfInCzechia(vehicleStatus);

            await this.oldDataCleanerHelper.deleteOldTrackableData(transformationDate.getTime(), HOPPYGO_SYSTEM_ID);
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError("Error while refreshing shared car data", this.constructor.name, err);
            }
        }
    };
}
