import { IMobilityOperator } from "#sch/datasources/MobilityOperator";
import { ISchemaDefinition } from "#sch/datasources/Nextbike/NextbikeSharedJsonSchema";

export const MobilityOperatorJsonSchema: ISchemaDefinition<IMobilityOperator[]> = {
    name: "MobilityOperator",
    jsonSchema: {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: {
                    type: "string",
                },
                name: {
                    type: "string",
                },
                isOnDemand: {
                    oneOf: [{ type: "boolean" }, { type: "null", nullable: true }],
                },
                isParkingProvider: {
                    type: "boolean",
                },
                transportModes: {
                    type: "array",
                    items: {
                        type: "string",
                    },
                },
                system_id: {
                    oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                },
                androidAppUrl: {
                    oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                },
                androidDiscoveryUrl: {
                    oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                },
                androidPackageName: {
                    oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                },
                iosAppUrl: {
                    oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                },
                iosDiscoveryUrl: {
                    oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                },
                iosPackageName: {
                    oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                },
                webUrl: {
                    oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                },
                pricingUrl: {
                    oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                },
                termsOfUseUrl: {
                    type: "string",
                },
                paymentMethods: {
                    type: "array",
                    items: {
                        type: "string",
                    },
                },
                email: {
                    oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                },
                orderByPhone: {
                    oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                },
            },
            required: [
                "id",
                "name",
                "isOnDemand",
                "isParkingProvider",
                "transportModes",
                "system_id",
                "androidAppUrl",
                "androidDiscoveryUrl",
                "androidPackageName",
                "iosAppUrl",
                "iosDiscoveryUrl",
                "iosPackageName",
                "webUrl",
                "pricingUrl",
                "termsOfUseUrl",
                "paymentMethods",
                "email",
                "orderByPhone",
            ],
            additionalProperties: true,
        },
    },
};
