import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { INextbikeRentalUris, ISchemaDefinition } from "#sch/datasources/Nextbike/NextbikeSharedJsonSchema";
import { NextbikeRentalUrisJsonSchema } from "#sch/datasources/Nextbike/NextbikeSharedJsonSchema";

export interface INextbikeFreeBikeStatus {
    bike_id: string;
    is_reserved: boolean;
    is_disabled: boolean;
    rental_uris: INextbikeRentalUris;
    lat: number;
    lon: number;
    vehicle_type_id: string | null;
    station_id: string | null;
    pricing_plan_id: string;
}

export interface INextbikeFreeBikeStatusInput {
    last_updated: number;
    ttl: number;
    version: string;
    data: {
        bikes: INextbikeFreeBikeStatus[];
    };
}

const FreeBikeStatusJsonSchema: JSONSchemaType<INextbikeFreeBikeStatus> = {
    type: "object",
    properties: {
        bike_id: {
            type: "string",
        },
        is_reserved: {
            type: "boolean",
        },
        is_disabled: {
            type: "boolean",
        },
        rental_uris: NextbikeRentalUrisJsonSchema,
        lat: {
            type: "number",
        },
        lon: {
            type: "number",
        },
        vehicle_type_id: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        station_id: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        pricing_plan_id: {
            type: "string",
        },
    },
    required: ["bike_id", "is_reserved", "is_disabled", "rental_uris", "lat", "lon", "pricing_plan_id"],
};

export const nextbikeFreeBikeStatusJsonSchema: ISchemaDefinition<INextbikeFreeBikeStatusInput> = {
    name: "SharedBikesNextbikeFreeBikeStatusDataSource",
    jsonSchema: {
        type: "object",
        properties: {
            last_updated: {
                type: "number",
            },
            ttl: {
                type: "number",
            },
            version: {
                type: "string",
            },
            data: {
                type: "object",
                properties: {
                    bikes: {
                        type: "array",
                        items: FreeBikeStatusJsonSchema,
                    },
                },
                required: ["bikes"],
                additionalProperties: false,
            },
        },
        required: ["last_updated", "ttl", "version", "data"],
        additionalProperties: false,
    },
};
