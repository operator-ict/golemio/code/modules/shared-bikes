import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { ISchemaDefinition } from "#sch/datasources/Nextbike/NextbikeSharedJsonSchema";

export interface INextbikeSystemPricing {
    start: number;
    interval: number;
    rate: number;
    end: number;
}

export interface INextbikeSystemPricingPlans {
    plan_id: string;
    name: string;
    currency: string;
    price: number;
    is_taxable: boolean;
    description: string;
    per_min_pricing: INextbikeSystemPricing[];
    per_km_pricing: INextbikeSystemPricing[];
    per_min_reservation_pricing: INextbikeSystemPricing[];
}

export interface INextbikeSystemPricingPlansInput {
    last_updated: number;
    ttl: number;
    version: string;
    data: {
        plans: INextbikeSystemPricingPlans[];
    };
}

const SystemPricingJsonSchema: JSONSchemaType<INextbikeSystemPricing> = {
    type: "object",
    properties: {
        start: {
            type: "number",
        },
        interval: {
            type: "number",
        },
        rate: {
            type: "number",
        },
        end: {
            type: "number",
        },
    },
    required: ["start", "interval", "rate"],
    additionalProperties: false,
};

const SystemPricingPlansJsonSchema: JSONSchemaType<INextbikeSystemPricingPlans> = {
    type: "object",
    properties: {
        plan_id: {
            type: "string",
        },
        name: {
            type: "string",
        },
        currency: {
            type: "string",
        },
        price: {
            type: "number",
        },
        is_taxable: {
            type: "boolean",
        },
        description: {
            type: "string",
        },
        per_min_pricing: {
            type: "array",
            items: SystemPricingJsonSchema,
        },
        per_km_pricing: {
            type: "array",
            items: SystemPricingJsonSchema,
        },
        per_min_reservation_pricing: {
            type: "array",
            items: SystemPricingJsonSchema,
        },
    },
    required: ["plan_id", "name", "currency", "price", "is_taxable", "description"],
    additionalProperties: false,
};

export const nextbikeSystemPricingPlansJsonSchema: ISchemaDefinition<INextbikeSystemPricingPlansInput> = {
    name: "SharedBikesNextbikeSystemPricingPlansDataSource",
    jsonSchema: {
        type: "object",
        properties: {
            last_updated: {
                type: "number",
            },
            ttl: {
                type: "number",
            },
            version: {
                type: "string",
            },
            data: {
                type: "object",
                properties: {
                    plans: {
                        type: "array",
                        items: SystemPricingPlansJsonSchema,
                    },
                },
                required: ["plans"],
                additionalProperties: false,
            },
        },
        required: ["last_updated", "ttl", "version", "data"],
        additionalProperties: false,
    },
};
