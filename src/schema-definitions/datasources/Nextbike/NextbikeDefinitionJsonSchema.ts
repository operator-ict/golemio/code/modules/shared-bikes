import { ISchemaDefinition } from "#sch/datasources/Nextbike/NextbikeSharedJsonSchema";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export enum NEXT_BIKE_SOURCES {
    system_information = "system_information",
    station_information = "station_information",
    station_status = "station_status",
    free_bike_status = "free_bike_status",
    system_pricing_plans = "system_pricing_plans",
}

export type TNextbikeDataTypes = keyof typeof NEXT_BIKE_SOURCES;

export interface INextbikeFeedDefinitionItem {
    name: TNextbikeDataTypes | string;
    url: string;
}

interface INextbikeFeedItem {
    feeds: INextbikeFeedDefinitionItem[];
}

export interface INextbikeDefinitionInput {
    last_updated: number;
    ttl: number;
    version: string;
    data: {
        cs: INextbikeFeedItem;
        en: INextbikeFeedItem;
    };
}

const DefinitionJsonSchema: JSONSchemaType<INextbikeFeedDefinitionItem> = {
    type: "object",
    properties: {
        name: {
            type: "string",
        },
        url: {
            type: "string",
        },
    },
    required: ["name", "url"],
    additionalProperties: false,
};

const FeedJsonSchema: JSONSchemaType<INextbikeFeedItem> = {
    type: "object",
    properties: {
        feeds: {
            type: "array",
            items: DefinitionJsonSchema,
        },
    },
    required: ["feeds"],
    additionalProperties: false,
};

export const nextbikeDefinitionJsonSchema: ISchemaDefinition<INextbikeDefinitionInput> = {
    name: "SharedBikesNextbikeDefinitionDataSource",
    jsonSchema: {
        type: "object",
        properties: {
            last_updated: {
                type: "number",
            },
            ttl: {
                type: "number",
            },
            version: {
                type: "string",
            },
            data: {
                type: "object",
                properties: {
                    cs: FeedJsonSchema,
                    en: FeedJsonSchema,
                },
                required: ["cs", "en"],
                additionalProperties: false,
            },
        },
        required: ["last_updated", "ttl", "version", "data"],
        additionalProperties: false,
    },
};
