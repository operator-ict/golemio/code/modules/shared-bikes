const CeskyCarsharingCarSchema = {
    car_name: {
        type: "string",
    },
    company_email: {
        type: "string",
    },
    company_id: {
        type: "number",
    },
    company_name: {
        type: "string",
    },
    company_phone: {
        type: "string",
    },
    company_web: {
        type: "string",
    },
    fuel: {
        type: "number",
    },
    fuel_type: {
        type: "string",
    },
    latitude: {
        type: "number",
    },
    longitude: {
        type: "number",
    },
    res_url: {
        type: "string",
    },
    rz: {
        type: "string",
    },
    status: {
        type: "number",
    },
};

export const CeskyCarsharingVehiclesJsonSchema = {
    name: "CeskyCarsharingVehiclesDataSource",
    jsonSchema: {
        type: "object",
        properties: {
            message: {
                type: "string",
            },
            status: {
                type: "number",
            },
            cars: {
                type: "array",
                items: {
                    type: "object",
                    properties: CeskyCarsharingCarSchema,
                },
            },
        },
    },
};
