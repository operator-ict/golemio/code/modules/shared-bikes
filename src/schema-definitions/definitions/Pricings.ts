import { DataTypes, ModelAttributes } from "@golemio/core/dist/shared/sequelize";

interface ISharedBikesPricingOutput {
    id: string;
    pricing_plan_id: string;
    pricing_type: string;
    pricing_order: number | null;
    start: number;
    rate: number;
    interval: number;
    end: number | null;
    start_time_of_period: string | null;
    end_time_of_period: string | null;
    [audit: string]: unknown;
}

const outputSequelizeAttributes: ModelAttributes<any, ISharedBikesPricingOutput> = {
    id: {
        type: DataTypes.STRING,
        primaryKey: true,
    },
    pricing_plan_id: DataTypes.STRING,
    pricing_type: DataTypes.TEXT,
    pricing_order: DataTypes.INTEGER,
    start: DataTypes.INTEGER,
    rate: DataTypes.FLOAT,
    interval: DataTypes.INTEGER,
    end: DataTypes.INTEGER,
    start_time_of_period: DataTypes.TEXT,
    end_time_of_period: DataTypes.TEXT,

    // Audit fields
    create_batch_id: DataTypes.BIGINT,
    created_at: DataTypes.DATE,
    created_by: DataTypes.STRING,
    update_batch_id: DataTypes.BIGINT,
    updated_at: DataTypes.DATE,
    updated_by: DataTypes.STRING,
};

const outputJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            pricing_plan_id: {
                type: "string",
                max: 50,
            },
            pricing_type: {
                type: "string",
            },
            pricing_order: {
                type: ["null", "number"],
            },
            start: {
                type: "number",
            },
            rate: {
                type: "number",
            },
            interval: {
                type: "number",
            },
            end: {
                type: ["null", "number"],
            },
            start_time_of_period: {
                type: ["null", "string"],
            },
            end_time_of_period: {
                type: ["null", "string"],
            },
        },
    },
};

export const pricings = {
    name: "SharedBikesPricings",
    outputSequelizeAttributes,
    outputJsonSchema,
    pgTableName: "pricings",
};

export type { ISharedBikesPricingOutput };
