import { DataTypes, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { Point } from "geojson";

interface ISharedBikesBikeStatusOutput {
    id: string;
    system_id: string;
    point: Point;
    helmets: number | null;
    passengers: number | null;
    damage_description: string | null;
    description: string | null;
    vehicle_registration: string | null;
    is_reserved: boolean;
    is_disabled: boolean;
    vehicle_type_id: string;
    last_reported: string;
    current_range_meters: number | null;
    charge_percent: number | null;
    rental_app_id: string | null;
    station_id: string | null;
    pricing_plan_id: string | null;
    make: string | null;
    model: string | null;
    color: string | null;
    [audit: string]: unknown;
}

const outputSequelizeAttributes: ModelAttributes<any, ISharedBikesBikeStatusOutput> = {
    id: {
        type: DataTypes.STRING,
        primaryKey: true,
    },
    system_id: DataTypes.STRING(50),
    point: DataTypes.GEOMETRY,
    helmets: DataTypes.INTEGER,
    passengers: DataTypes.INTEGER,
    damage_description: DataTypes.TEXT,
    description: DataTypes.TEXT,
    vehicle_registration: DataTypes.TEXT,
    is_reserved: DataTypes.BOOLEAN,
    is_disabled: DataTypes.BOOLEAN,
    vehicle_type_id: DataTypes.TEXT,
    last_reported: DataTypes.DATE,
    current_range_meters: DataTypes.FLOAT,
    charge_percent: DataTypes.INTEGER,
    rental_app_id: DataTypes.STRING(50),
    station_id: DataTypes.TEXT,
    pricing_plan_id: DataTypes.STRING(50),
    make: DataTypes.TEXT,
    model: DataTypes.TEXT,
    color: DataTypes.TEXT,

    // Audit fields
    create_batch_id: DataTypes.BIGINT,
    created_at: DataTypes.DATE,
    created_by: DataTypes.STRING,
    update_batch_id: DataTypes.BIGINT,
    updated_at: DataTypes.DATE,
    updated_by: DataTypes.STRING,
};

const outputJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            id: {
                type: "string",
            },
            system_id: {
                type: "string",
            },
            point: {
                type: ["null", "object"],
            },
            helmets: {
                type: ["null", "number"],
            },
            passengers: {
                type: ["null", "number"],
            },
            damage_description: {
                type: ["null", "string"],
            },
            description: {
                type: ["null", "string"],
            },
            vehicle_registration: {
                type: ["null", "string"],
            },
            is_reserved: {
                type: "boolean",
            },
            is_disabled: {
                type: "boolean",
            },
            vehicle_type_id: {
                type: "string",
            },
            last_reported: {
                type: ["null", "string"],
            },
            current_range_meters: {
                type: ["null", "number"],
            },
            charge_percent: {
                type: ["null", "number"],
            },
            rental_app_id: {
                type: ["null", "string"],
            },
            station_id: {
                type: ["null", "string"],
            },
            pricing_plan_id: {
                type: ["null", "string"],
            },
            make: {
                type: ["null", "string"],
            },
            model: {
                type: ["null", "string"],
            },
            color: {
                type: ["null", "string"],
            },
        },
    },
};

export const bikeStatus = {
    name: "SharedBikesBikeStatus",
    outputSequelizeAttributes,
    outputJsonSchema,
    pgTableName: "bike_status",
};

export type { ISharedBikesBikeStatusOutput };
