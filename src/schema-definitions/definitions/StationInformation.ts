import { DataTypes, ModelAttributes, WhereGeometryOptions } from "@golemio/core/dist/shared/sequelize";
import { Point } from "geojson";

interface ISharedBikesStationInformationOutput {
    id: string;
    system_id: string;
    name: string;
    short_name?: string | null;
    point: Point;
    address: string | null;
    post_code: string | null;
    cross_street: string | null;
    region_id: string | null;
    rental_methods: string | null;
    is_virtual_station: boolean | null;
    station_area: WhereGeometryOptions | null;
    capacity: number | null;
    vehicle_capacity: number | null;
    vehicle_type_capacity: string | null;
    is_valet_station: boolean | null;
    rental_app_id: string | null;
    [audit: string]: unknown;
}

const outputSequelizeAttributes: ModelAttributes<any, ISharedBikesStationInformationOutput> = {
    id: {
        type: DataTypes.TEXT,
        primaryKey: true,
    },
    system_id: DataTypes.STRING(50),
    name: DataTypes.TEXT,
    short_name: DataTypes.TEXT,
    point: DataTypes.GEOMETRY,
    address: DataTypes.TEXT,
    post_code: DataTypes.TEXT,
    cross_street: DataTypes.TEXT,
    region_id: DataTypes.TEXT,
    rental_methods: DataTypes.TEXT,
    is_virtual_station: DataTypes.BOOLEAN,
    station_area: DataTypes.GEOMETRY,
    capacity: DataTypes.INTEGER,
    vehicle_capacity: DataTypes.INTEGER,
    vehicle_type_capacity: DataTypes.TEXT,
    is_valet_station: DataTypes.BOOLEAN,
    rental_app_id: DataTypes.STRING(50),

    // Audit fields
    create_batch_id: DataTypes.BIGINT,
    created_at: DataTypes.DATE,
    created_by: DataTypes.STRING,
    update_batch_id: DataTypes.BIGINT,
    updated_at: DataTypes.DATE,
    updated_by: DataTypes.STRING,
};

const outputJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            id: {
                type: "string",
            },
            system_id: {
                type: "string",
            },
            name: {
                type: "string",
            },
            short_name: {
                type: ["null", "string"],
            },
            point: {
                type: "object",
            },
            address: {
                type: ["null", "string"],
            },
            post_code: {
                type: ["null", "string"],
            },
            cross_street: {
                type: ["null", "string"],
            },
            region_id: {
                type: ["null", "string"],
            },
            rental_methods: {
                type: ["null", "string"],
            },
            is_virtual_station: {
                type: ["null", "boolean"],
            },
            station_area: {
                type: ["null", "object"],
            },
            capacity: {
                type: ["null", "number"],
            },
            vehicle_capacity: {
                type: ["null", "number"],
            },
            vehicle_type_capacity: {
                type: ["null", "string"],
            },
            is_valet_station: {
                type: ["null", "boolean"],
            },
            rental_app_id: {
                type: ["null", "string"],
            },
        },
    },
};

export const stationInformation = {
    name: "SharedBikesStationsInformation",
    outputSequelizeAttributes,
    outputJsonSchema,
    pgTableName: "station_information",
};

export type { ISharedBikesStationInformationOutput };
