import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { SharedBikes } from "#sch";
import { ISystemPricingPlanJson } from "#og/definitions";
import { IGBFSModels } from ".";
import { PricingModel } from "./PricingModel";
import { buildPricingPlans } from "#og/helpers";

/**
 * Custom Postgres model for pricing plan
 */
export class PricingPlanModel extends SequelizeModel {
    private pricingModel: PricingModel | undefined;

    constructor() {
        super(
            SharedBikes.definitions.pricingPlans.name + "Model",
            SharedBikes.definitions.pricingPlans.pgTableName,
            SharedBikes.definitions.pricingPlans.outputSequelizeAttributes,
            {
                schema: SharedBikes.pgSchema,
            }
        );
    }

    Associate = (models: IGBFSModels) => {
        this.pricingModel = models.PricingModel;

        this.sequelizeModel.hasMany(models.PricingModel.sequelizeModel, {
            sourceKey: "id",
            foreignKey: "pricing_plan_id",
            as: "pricings",
        });
    };

    async GetAll(systemId: string): Promise<ISystemPricingPlanJson> {
        const plans = await this.sequelizeModel.findAll({
            attributes: {
                include: [
                    [Sequelize.literal("extract(epoch from date_trunc('second', pricing_plans.last_updated))"), "last_updated"],
                    ["id", "plan_id"],
                ],
                exclude: ["id"],
            },
            include: {
                model: this.pricingModel?.sequelizeModel,
                as: "pricings",
            },
            where: {
                system_id: systemId,
            },
        });

        const results = buildPricingPlans(plans);
        return {
            plans: results,
        };
    }

    GetOne(id: any): Promise<any> {
        throw new Error("Method not implemented.");
    }
}
