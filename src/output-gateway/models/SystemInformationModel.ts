import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { Sequelize } from "@golemio/core/dist/shared/sequelize";
import { ISharedBikesSystemInformationOutput, SharedBikes } from "#sch";
import { ISystemInformationJson } from "#og/definitions";
import { IGBFSModels } from "./";
import { RentalAppModel } from "./RentalAppModel";
import { removeEmptyOrNull } from "#og/helpers";

/**
 * Custom Postgres model for system information
 */
export class SystemInformationModel extends SequelizeModel {
    private rentalAppModel: RentalAppModel | undefined;

    constructor() {
        super(
            SharedBikes.definitions.systemInformation.name + "Model",
            SharedBikes.definitions.systemInformation.pgTableName,
            SharedBikes.definitions.systemInformation.outputSequelizeAttributes,
            {
                schema: SharedBikes.pgSchema,
            }
        );
    }

    Associate = (models: IGBFSModels) => {
        this.rentalAppModel = models.RentalAppModel;

        this.sequelizeModel.belongsTo(models.RentalAppModel.sequelizeModel, {
            as: "rental_apps",
            targetKey: "id",
            foreignKey: "rental_app_id",
        });
    };

    async GetAll(options?: any): Promise<ISharedBikesSystemInformationOutput[]> {
        return this.sequelizeModel.findAll();
    }

    async GetOneWithRentalApps(id: string): Promise<ISystemInformationJson | Partial<ISystemInformationJson>> {
        const result = await this.sequelizeModel.findOne({
            attributes: {
                include: [[Sequelize.literal("date(start_date)"), "start_date"]],
                exclude: ["rental_app_id"],
            },
            include: {
                as: "rental_apps",
                model: this.rentalAppModel?.sequelizeModel,
                attributes: [
                    ["android_store_url", "android.store_uri"],
                    ["android_discovery_url", "android.discovery_uri"],
                    ["ios_store_url", "ios.store_uri"],
                    ["ios_discovery_url", "ios.discovery_uri"],
                ],
            },
            where: {
                system_id: id,
            },
            nest: true,
            raw: true,
        });

        return result ? removeEmptyOrNull(result) : result;
    }

    GetOne(id: string): Promise<ISharedBikesSystemInformationOutput> {
        return this.sequelizeModel.findOne({
            where: {
                system_id: id,
            },
            raw: true,
        });
    }
}
