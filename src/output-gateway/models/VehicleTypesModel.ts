import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { SharedBikes } from "#sch";
import { IVehicleType, IVehicleTypesJson } from "#og/definitions";
import { IGBFSModels } from ".";
import { BikeStatusModel } from "./BikeStatusModel";
import { removeEmptyOrNull } from "#og/helpers";

/**
 * Custom Postgres model for vehicle types
 */
export class VehicleTypesModel extends SequelizeModel {
    private bikeStatusModel: BikeStatusModel | undefined;

    constructor() {
        super(
            SharedBikes.definitions.vehicleTypes.name + "Model",
            SharedBikes.definitions.vehicleTypes.pgTableName,
            SharedBikes.definitions.vehicleTypes.outputSequelizeAttributes,
            {
                schema: SharedBikes.pgSchema,
            }
        );
    }

    Associate = (models: IGBFSModels) => {
        this.bikeStatusModel = models.BikeStatusModel;

        this.sequelizeModel.belongsTo(models.BikeStatusModel.sequelizeModel, {
            targetKey: "vehicle_type_id",
            foreignKey: "id",
        });
    };

    async GetAll(systemId: string): Promise<IVehicleTypesJson> {
        const results = await this.sequelizeModel.findAll({
            attributes: [
                [Sequelize.fn("DISTINCT", Sequelize.col("vehicle_types.id")), "vehicle_type_id"],
                "form_factor",
                "propulsion_type",
                "max_range_meters",
                "name",
            ],
            include: [
                {
                    model: this.bikeStatusModel?.sequelizeModel,
                    attributes: [],
                    where: {
                        system_id: systemId,
                    },
                },
            ],
            raw: true,
        });

        return {
            vehicle_types: (results as IVehicleType[]).map((res) => removeEmptyOrNull(res)),
        };
    }

    GetOne(id: any): Promise<any> {
        throw new Error("Method not implemented.");
    }
}
