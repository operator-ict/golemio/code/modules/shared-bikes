export interface ISharedVehiclesQueryParams {
    latlng?: string;
    range?: string;
    limit?: number;
    offset?: number;
    companyName?: string | string[]; // deprecated
    companyNames?: string | string[];
    updatedSince?: string;
}
