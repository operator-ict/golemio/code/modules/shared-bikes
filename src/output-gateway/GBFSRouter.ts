/**
 *
 * Router /WEB LAYER/: maps routes to specific controller functions, passes request parameters and handles responses.
 * Handles web logic (http request, response). Sets response headers, handles error responses.
 */

import { useCacheMiddleware } from "@golemio/core/dist/output-gateway/redis";
import { BaseRouter } from "@golemio/core/dist/output-gateway/routes/BaseRouter";
import { checkErrors, paginationLimitMiddleware, pagination } from "@golemio/core/dist/output-gateway/Validation";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { param } from "@golemio/core/dist/shared/express-validator";
import { config } from "@golemio/core/dist/output-gateway";
import { models } from "./models";
import { IGBFSJson, IGBFSVersionsJson, IGBFSWrapper } from "./definitions";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { BikeStatusModel } from "./models/BikeStatusModel";
import { SystemInformationModel } from "./models/SystemInformationModel";
import { VehicleTypesModel } from "./models/VehicleTypesModel";
import { StationInformationModel } from "./models/StationInformationModel";
import { StationStatusModel } from "./models/StationStatusModel";
import { PricingPlanModel } from "./models/PricingPlanModel";
import { GeofencingZonesModel } from "./models/GeofencingZonesModel";

// 2 minutes, based on cron-tasks configuration
const TTL = 120;
// GBFS specification version
const VERSION = "3.0";

export class GBFSRouter extends BaseRouter {
    // Assign router to the express.Router() instance
    public router: Router = Router();
    private bikeStatusModel: BikeStatusModel;
    private systemInformationModel: SystemInformationModel;
    private vehicleTypesModel: VehicleTypesModel;
    private stationInformationModel: StationInformationModel;
    private stationStatusModel: StationStatusModel;
    private pricingPlanModel: PricingPlanModel;
    private geofencingZonesModel: GeofencingZonesModel;
    private readonly apiUrlPrefix: string;

    public constructor() {
        super();
        this.bikeStatusModel = models.BikeStatusModel;
        this.systemInformationModel = models.SystemInformationModel;
        this.vehicleTypesModel = models.VehicleTypesModel;
        this.stationInformationModel = models.StationInformationModel;
        this.stationStatusModel = models.StationStatusModel;
        this.pricingPlanModel = models.PricingPlanModel;
        this.geofencingZonesModel = models.GeofencingZonesModel;
        this.apiUrlPrefix = config.api_url_prefix;
        this.initRoutes();
    }

    private async buildGBFSOutput<T>(data: T): Promise<IGBFSWrapper<T>> {
        return {
            last_updated: (await this.bikeStatusModel.GetLatestLastReportedAsUnix()).last_reported,
            ttl: TTL,
            version: VERSION,
            data,
        };
    }

    private GetSharedMobilityProviders = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const systems = await this.systemInformationModel.GetAll();

            res.status(200).send({
                shared_mobility_providers: {
                    providers_url_list: systems.map((system) => {
                        return `${this.apiUrlPrefix}${req.baseUrl}/${system.system_id}/gbfs`;
                    }),
                },
            });
        } catch (err) {
            next(err);
        }
    };

    private GetGBFSJson = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const systemId: string = req.params.system_id;
            const system = await this.systemInformationModel.GetOne(systemId);
            if (!system) {
                throw new GeneralError("not_found", "GBFSRouter", undefined, 404);
            }

            const data: IGBFSJson = {
                [system.language]: {
                    feeds: [
                        {
                            name: "gbfs_versions",
                            url: `${this.apiUrlPrefix}${req.baseUrl}/${systemId}/gbfs_versions`,
                        },
                        {
                            name: "system_information",
                            url: `${this.apiUrlPrefix}${req.baseUrl}/${systemId}/system_information`,
                        },
                        {
                            name: "free_bike_status",
                            url: `${this.apiUrlPrefix}${req.baseUrl}/${systemId}/free_bike_status`,
                        },
                        {
                            name: "vehicle_types",
                            url: `${this.apiUrlPrefix}${req.baseUrl}/${systemId}/vehicle_types`,
                        },
                        {
                            name: "station_information",
                            url: `${this.apiUrlPrefix}${req.baseUrl}/${systemId}/station_information`,
                        },
                        {
                            name: "station_status",
                            url: `${this.apiUrlPrefix}${req.baseUrl}/${systemId}/station_status`,
                        },
                        {
                            name: "system_pricing_plans",
                            url: `${this.apiUrlPrefix}${req.baseUrl}/${systemId}/system_pricing_plans`,
                        },
                        {
                            name: "geofencing_zones",
                            url: `${this.apiUrlPrefix}${req.baseUrl}/${systemId}/geofencing_zones`,
                        },
                    ],
                },
            };

            res.status(200).send(await this.buildGBFSOutput(data));
        } catch (err) {
            next(err);
        }
    };

    private GetGBFSVersionsJson = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const systemId: string = req.params.system_id;
            const system = await this.systemInformationModel.GetOne(systemId);
            if (!system) {
                throw new GeneralError("not_found", "GBFSRouter", undefined, 404);
            }

            const data: IGBFSVersionsJson = {
                versions: [
                    {
                        version: "3.0",
                        url: "https://gitlab.com/operator-ict/golemio/code/modules/shared-bikes/-/blob/master/docs/GBFS_extension.md",
                    },
                ],
            };

            res.status(200).send(await this.buildGBFSOutput(data));
        } catch (err) {
            next(err);
        }
    };

    private GetSystemInformationJson = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const systemId: string = req.params.system_id;
            const system = await this.systemInformationModel.GetOneWithRentalApps(systemId);
            if (!system) {
                throw new GeneralError("not_found", "GBFSRouter", undefined, 404);
            }

            res.status(200).send(await this.buildGBFSOutput(system));
        } catch (err) {
            next(err);
        }
    };

    private GetFreeBikeStatusJson = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const systemId: string = req.params.system_id;
            const system = await this.systemInformationModel.GetOne(systemId);
            if (!system) {
                throw new GeneralError("not_found", "GBFSRouter", undefined, 404);
            }
            const data = await this.bikeStatusModel.GetAllGTFS(systemId);

            res.status(200).send(await this.buildGBFSOutput(data));
        } catch (err) {
            next(err);
        }
    };

    private GetVehicleTypesJson = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const systemId: string = req.params.system_id;
            const system = await this.systemInformationModel.GetOne(systemId);
            if (!system) {
                throw new GeneralError("not_found", "GBFSRouter", undefined, 404);
            }
            const data = await this.vehicleTypesModel.GetAll(systemId);

            res.status(200).send(await this.buildGBFSOutput(data));
        } catch (err) {
            next(err);
        }
    };

    private GetStationInformationJson = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const systemId: string = req.params.system_id;
            const system = await this.systemInformationModel.GetOne(systemId);
            if (!system) {
                throw new GeneralError("not_found", "GBFSRouter", undefined, 404);
            }
            const data = await this.stationInformationModel.GetAll(systemId);

            res.status(200).send(await this.buildGBFSOutput(data));
        } catch (err) {
            next(err);
        }
    };

    private GetStationStatusJson = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const systemId: string = req.params.system_id;
            const system = await this.systemInformationModel.GetOne(systemId);
            if (!system) {
                throw new GeneralError("not_found", "GBFSRouter", undefined, 404);
            }
            const data = await this.stationStatusModel.GetAll(systemId);

            res.status(200).send(await this.buildGBFSOutput(data));
        } catch (err) {
            next(err);
        }
    };

    private GetSystemPricingPlansJson = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const systemId: string = req.params.system_id;
            const system = await this.systemInformationModel.GetOne(systemId);
            if (!system) {
                throw new GeneralError("not_found", "GBFSRouter", undefined, 404);
            }
            const data = await this.pricingPlanModel.GetAll(systemId);

            res.status(200).send(await this.buildGBFSOutput(data));
        } catch (err) {
            next(err);
        }
    };

    private GetGeofencingZonesJson = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const systemId: string = req.params.system_id;
            const system = await this.systemInformationModel.GetOne(systemId);
            if (!system) {
                throw new GeneralError("not_found", "GBFSRouter", undefined, 404);
            }
            const data = await this.geofencingZonesModel.GetAll(systemId);

            res.status(200).send(await this.buildGBFSOutput(data));
        } catch (err) {
            next(err);
        }
    };

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     * @param {number|string} expire TTL for the caching middleware
     */
    private initRoutes = (expire?: number | string): void => {
        this.router.get(
            "/systems_list",
            pagination,
            checkErrors,
            paginationLimitMiddleware("GBFSRouter"),
            useCacheMiddleware(expire),
            this.GetSharedMobilityProviders
        );
        this.router.get(
            "/:system_id/gbfs",
            [param("system_id").exists()],
            pagination,
            checkErrors,
            paginationLimitMiddleware("GBFSRouter"),
            useCacheMiddleware(expire),
            this.GetGBFSJson
        );
        this.router.get(
            "/:system_id/gbfs_versions",
            [param("system_id").exists()],
            pagination,
            checkErrors,
            paginationLimitMiddleware("GBFSRouter"),
            useCacheMiddleware(expire),
            this.GetGBFSVersionsJson
        );
        this.router.get(
            "/:system_id/system_information",
            [param("system_id").exists()],
            pagination,
            checkErrors,
            paginationLimitMiddleware("GBFSRouter"),
            useCacheMiddleware(expire),
            this.GetSystemInformationJson
        );
        this.router.get(
            "/:system_id/free_bike_status",
            [param("system_id").exists()],
            pagination,
            checkErrors,
            paginationLimitMiddleware("GBFSRouter"),
            useCacheMiddleware(expire),
            this.GetFreeBikeStatusJson
        );
        this.router.get(
            "/:system_id/vehicle_types",
            [param("system_id").exists()],
            pagination,
            checkErrors,
            paginationLimitMiddleware("GBFSRouter"),
            useCacheMiddleware(expire),
            this.GetVehicleTypesJson
        );
        this.router.get(
            "/:system_id/station_information",
            [param("system_id").exists()],
            pagination,
            checkErrors,
            paginationLimitMiddleware("GBFSRouter"),
            useCacheMiddleware(expire),
            this.GetStationInformationJson
        );
        this.router.get(
            "/:system_id/station_status",
            [param("system_id").exists()],
            pagination,
            checkErrors,
            paginationLimitMiddleware("GBFSRouter"),
            useCacheMiddleware(expire),
            this.GetStationStatusJson
        );
        this.router.get(
            "/:system_id/system_pricing_plans",
            [param("system_id").exists()],
            pagination,
            checkErrors,
            paginationLimitMiddleware("GBFSRouter"),
            useCacheMiddleware(expire),
            this.GetSystemPricingPlansJson
        );
        this.router.get(
            "/:system_id/geofencing_zones",
            [param("system_id").exists()],
            pagination,
            checkErrors,
            paginationLimitMiddleware("GBFSRouter"),
            useCacheMiddleware(expire),
            this.GetGeofencingZonesJson
        );
    };
}

const gbfsRouter: Router = new GBFSRouter().router;

export { gbfsRouter };
