export const removeEmptyOrNull = <T extends object>(obj: T): Partial<T> => {
    return Object.entries(obj)
        .filter(([_, v]) => v != null)
        .reduce((acc, [k, v]) => ({ ...acc, [k]: v === Object(v) && !Array.isArray(v) ? removeEmptyOrNull(v) : v }), {});
};
