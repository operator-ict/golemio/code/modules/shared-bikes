import {
    ISharedBikesFeatureProperties,
    ISharedCarsFeatureProperties,
    ISharedVehiclesFeature,
} from "#og/interfaces/ISharedVehiclesFeature";
import { IBikeStatus } from "#og/models/BikeStatusModel";
import {
    buildGeojsonFeatureCollection,
    buildGeojsonFeatureType,
    IGeoJSONFeatureCollection,
    TGeoCoordinates,
} from "@golemio/core/dist/output-gateway";

abstract class FeatureCollectionBuilder<T extends Record<string, any>> {
    public buildFeatureCollection(locations: IBikeStatus[]): IGeoJSONFeatureCollection {
        const normData: Array<ISharedVehiclesFeature<T>> = [];
        locations.forEach((location: IBikeStatus) => {
            normData.push(this.buildFeatureItem(location));
        });
        return buildGeojsonFeatureCollection(normData);
    }

    public buildFeatureItem(location: IBikeStatus): ISharedVehiclesFeature<T> {
        const obj = this.mapItems(location);
        return <ISharedVehiclesFeature<T>>buildGeojsonFeatureType("point", obj);
    }

    public abstract mapItems(location: IBikeStatus): T & Record<"point", TGeoCoordinates>;
}

class BikesFeatureCollectionBuilder extends FeatureCollectionBuilder<ISharedBikesFeatureProperties> {
    public mapItems(location: IBikeStatus) {
        return {
            id: location.id,
            in_rack: !!location.station_id,
            label: "",
            location_note: location.description,
            name: location.name,
            res_url: location.company_web,
            company: {
                name: location.company_name,
                web: location.company_web,
            },
            type: getTypeForSharedBikes(location.type),
            updated_at: location.last_reported,
            point: location.point,
        };
    }
}

class CarsFeatureCollectionBuilder extends FeatureCollectionBuilder<ISharedCarsFeatureProperties> {
    public mapItems(location: IBikeStatus) {
        return {
            id: location.id,
            name: location.description,
            res_url: location.company_web,
            company: {
                name: location.company_name,
                web: location.company_web,
            },
            availability: getAvailabilityForSharedCars(location.id),
            fuel: getFuelForSharedCars(location.type_id),
            updated_at: location.last_reported,
            point: location.point,
        };
    }
}

const getTypeForSharedBikes = (id: string): { description: string; id: number } => {
    switch (id) {
        case "bicycle":
            return { description: "bike", id: 1 };
        case "e-bike":
            return { description: "ebike", id: 2 };
        case "scooter":
            return { description: "scooter", id: 3 };
        case "padlujemecz":
            return { description: "paddleboard", id: 4 };
        case "shared_moped":
            return { description: "shared moped", id: 5 };
        default:
            return { description: "bike", id: 1 };
    }
};

const getAvailabilityForSharedCars = (id: string): { description: string; id: number } => {
    if (id.includes("hoppygo")) {
        return { description: "dle domluvy s provozovatelem", id: 2 };
    } else if (id.includes("cesky-carsharing")) {
        return { description: "ihned", id: 1 };
    } else {
        return { description: "neznámý", id: 0 };
    }
};

const getFuelForSharedCars = (type: string): { description: string; id: number } => {
    switch (type) {
        case "carsharing_diesel":
            return { description: "nafta", id: 2 };
        case "carsharing_benzin":
            return { description: "benzín", id: 1 };
        case "carsharing_benzin_LPG":
            return { description: "benzín + LPG", id: 3 };
        case "hoppygo_car_combustion":
            return { description: "jiný", id: 7 };
        case "carsharing_electric":
        case "hoppygo_car_electric":
            return { description: "elektřina", id: 4 };
        case "carsharing_hybrid":
            return { description: "hybrid", id: 5 };
        default:
            return { description: "neznámý", id: 0 };
    }
};

const bikesFeatureCollectionBuilder = new BikesFeatureCollectionBuilder();
const carsFeatureCollectionBuilder = new CarsFeatureCollectionBuilder();

export { bikesFeatureCollectionBuilder, carsFeatureCollectionBuilder };
