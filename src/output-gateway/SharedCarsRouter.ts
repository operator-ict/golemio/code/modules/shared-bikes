/**
 *
 * Router /WEB LAYER/: maps routes to specific controller functions, passes request parameters and handles responses.
 * Handles web logic (http request, response). Sets response headers, handles error responses.
 */

import { parseCoordinates } from "@golemio/core/dist/output-gateway/Geo";
import { useCacheMiddleware } from "@golemio/core/dist/output-gateway/redis";
import { BaseRouter } from "@golemio/core/dist/output-gateway/routes/BaseRouter";
import { checkErrors, paginationLimitMiddleware, pagination } from "@golemio/core/dist/output-gateway/Validation";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { query, param } from "@golemio/core/dist/shared/express-validator";
import { BikeStatusModel } from "./models/BikeStatusModel";
import { models } from "./models";
import { carsFeatureCollectionBuilder } from "./helpers/buildGeoFeature";
import { ISharedVehiclesQueryParams } from "./interfaces/ISharedVehiclesQueryParams";

export class SharedCarsRouter extends BaseRouter {
    // Assign router to the express.Router() instance
    public router: Router = Router();
    private bikeStatusModel: BikeStatusModel;

    public constructor() {
        super();
        this.bikeStatusModel = models.BikeStatusModel;
        this.initRoutes();
    }

    private GetAll = async (
        req: Request<unknown, unknown, unknown, ISharedVehiclesQueryParams>,
        res: Response,
        next: NextFunction
    ) => {
        let companyNames = req.query.companyNames;

        if (companyNames) {
            companyNames = this.ConvertToArray(companyNames);
        } else {
            companyNames = undefined;
        }

        try {
            let coords: { lat: number | undefined; lng: number | undefined; range: number | undefined } = {
                lat: undefined,
                lng: undefined,
                range: undefined,
            };
            if (req.query.latlng) {
                coords = await parseCoordinates(req.query.latlng, req.query.range || "0");
            }
            const data = await this.bikeStatusModel.GetAll({
                lat: coords.lat,
                lng: coords.lng,
                range: coords.range,
                limit: req.query.limit,
                offset: req.query.offset,
                updatedSince: req.query.updatedSince,
                companyNames: companyNames,
                vehicleTypes: ["car"],
            });

            res.status(200).send(carsFeatureCollectionBuilder.buildFeatureCollection(data));
        } catch (err) {
            next(err);
        }
    };

    private GetOne = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.bikeStatusModel.GetOne(req.params.vehicleId);
            if (!data) {
                throw new GeneralError("not_found", "SharedCarsRouter", undefined, 404);
            }

            res.status(200).send(carsFeatureCollectionBuilder.buildFeatureItem(data));
        } catch (err) {
            next(err);
        }
    };

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     * @param {number|string} expire TTL for the caching middleware
     */
    private initRoutes = (expire?: number | string): void => {
        this.router.get(
            "/",
            [
                query("latlng").optional().isString(),
                query("range").optional().isNumeric().toInt(),
                query("limit").optional().isNumeric().toInt(),
                query("offset").optional().isNumeric().toInt(),
                query("companyName").optional().isString(),
                query("updatedSince").optional().isISO8601(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("SharedCarsRouter"),
            useCacheMiddleware(expire),
            this.GetAll
        );

        this.router.get(
            "/:vehicleId",
            [param("vehicleId").exists()],
            pagination,
            checkErrors,
            paginationLimitMiddleware("SharedCarsRouter"),
            useCacheMiddleware(expire),
            this.GetOne
        );
    };
}

const sharedCarsRouter: Router = new SharedCarsRouter().router;

export { sharedCarsRouter };
