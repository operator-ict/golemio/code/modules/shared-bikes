export interface IStationStatusJson {
    stations: Array<IStationStatus | Partial<IStationStatus>>;
}

export interface IStationStatus {
    station_id: number;
    num_bikes_available: number;
    num_bikes_disabled: number | null;
    num_docks_available: number | null;
    is_installed: boolean | null;
    is_renting: boolean | null;
    is_returning: boolean | null;
    last_reported: number;
}
