import { WhereGeometryOptions } from "@golemio/core/dist/shared/sequelize";
import { IRentalApp } from "./";

export interface IStationInformationsJson {
    stations: Array<IStationInformation | Partial<IStationInformation>>;
}

export interface IStationInformation {
    station_id: string;
    name: string;
    short_name?: string | null;
    lat: number;
    lon: number;
    address: string | null;
    post_code: string | null;
    cross_street: string | null;
    region_id: string | null;
    rental_methods: string | null;
    is_virtual_station: boolean | null;
    station_area: WhereGeometryOptions | null;
    capacity: number | null;
    vehicle_capacity: number | null;
    vehicle_type_capacity: string | null;
    is_valet_station: boolean | null;
    rental_uris: IRentalApp[];
}
