import { IRentalApp, IVehicleType } from "./";

export interface IFreeBikeStatusJson {
    bikes: Array<IFreeBikeStatus | Partial<IFreeBikeStatus>>;
}

export interface IFreeBikeStatus {
    bike_id: string;
    system_id: string;
    lat: number;
    lon: number;
    helmets: number | null;
    passengers: number | null;
    damage_description: string | null;
    description: string | null;
    vehicle_registration: string | null;
    is_reserved: boolean;
    is_disabled: boolean;
    rental_uris: IRentalApp[];
    vehicle_type_id: string;
    vehicle_type: IVehicleType;
    last_reported: number;
    current_range_meters: number | null;
    charge_percent: number | null;
    station_id: string;
    pricing_plan_id?: string | null;
    make: string | null;
    model: string | null;
    color: string | null;
}
