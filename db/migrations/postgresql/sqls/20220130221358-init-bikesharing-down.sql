DROP TABLE IF EXISTS bike_status;

DROP TABLE IF EXISTS station_status_vehicle_type;

DROP TABLE IF EXISTS vehicle_types;

DROP TABLE IF EXISTS station_status;

DROP TABLE IF EXISTS station_information;

DROP TABLE IF EXISTS rental_apps_system_info;

DROP TABLE IF EXISTS rental_apps;

DROP TABLE IF EXISTS pricings;

DROP TABLE IF EXISTS pricing_plans;

DROP TABLE IF EXISTS geofencing_rules;

DROP TABLE IF EXISTS geofencing_zones;

DROP TABLE IF EXISTS system_information;
