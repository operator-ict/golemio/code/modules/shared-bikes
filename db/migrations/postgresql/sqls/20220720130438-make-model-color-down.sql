ALTER TABLE bikesharing.bike_status DROP COLUMN make;
ALTER TABLE bikesharing.bike_status DROP COLUMN model;
ALTER TABLE bikesharing.bike_status DROP COLUMN color;
