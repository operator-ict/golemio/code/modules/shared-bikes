ALTER TABLE bikesharing.pricings DROP CONSTRAINT pricings_pkey;
ALTER TABLE bikesharing.pricings DROP COLUMN id;
ALTER TABLE bikesharing.pricings ADD CONSTRAINT pricings_pkey PRIMARY KEY (pricing_plan_id, pricing_type, start);
