ALTER TABLE bikesharing.pricings DROP CONSTRAINT pricings_pkey;
ALTER TABLE bikesharing.bike_status ADD COLUMN pricing_plan_id character varying(50);
ALTER TABLE bikesharing.bike_status ADD CONSTRAINT bike_status_pricing_plan_id_fkey FOREIGN KEY (pricing_plan_id) REFERENCES bikesharing.pricing_plans(id);

