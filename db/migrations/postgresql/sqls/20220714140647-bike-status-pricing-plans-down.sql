ALTER TABLE bikesharing.bike_status DROP CONSTRAINT bike_status_pricing_plan_id_fkey;
ALTER TABLE bikesharing.bike_status DROP COLUMN pricing_plan_id;
ALTER TABLE bikesharing.pricings ADD CONSTRAINT pricings_pkey PRIMARY KEY (pricing_plan_id, pricing_type);
