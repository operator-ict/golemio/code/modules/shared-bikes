ALTER TABLE bikesharing.pricings DROP CONSTRAINT pricings_pkey;
ALTER TABLE bikesharing.pricings ADD COLUMN id VARCHAR(50) NOT NULL DEFAULT substr(md5(random()::text), 0, 25);
UPDATE bikesharing.pricings SET id = md5(concat(pricing_plan_id, pricing_type, start, rate, start_time_of_period));
ALTER TABLE bikesharing.pricings ADD CONSTRAINT pricings_pkey PRIMARY KEY (id);
