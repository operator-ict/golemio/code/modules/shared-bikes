DROP TABLE geofencing_rules;
DROP TABLE geofencing_zones;

CREATE TABLE IF NOT EXISTS geofencing_zones (
    "id" text,
    "system_id" varchar(50) NOT NULL,
    "name" text,
    "note" text,
    "source" text,
    "price" integer,
    "priority" integer NOT NULL,
    "start" timestamp,
    "end" timestamp,
    "geom" geometry NOT NULL,
    "ride_allowed" boolean NOT NULL,
    "ride_through_allowed" boolean NOT NULL,
    "parking_allowed" boolean NOT NULL,
    "maximum_speed_kph" integer,

    -- Audit fields
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    CONSTRAINT geofencing_zones_pkey PRIMARY KEY (id),
    CONSTRAINT geofencing_zones_system_id_fkey FOREIGN KEY (system_id) REFERENCES system_information(system_id)
);

CREATE TABLE IF NOT EXISTS pricing_plan_payment (
    "pricing_plan_id" varchar(50) NOT NULL,
    "payment_method" varchar(40) NOT NULL,

    -- Audit fields
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    CONSTRAINT pricings_plan_id_fkey FOREIGN KEY (pricing_plan_id) REFERENCES pricing_plans(id)
);

ALTER TABLE system_information ADD COLUMN "terms_of_use_url" text;

ALTER TABLE station_information ADD COLUMN "capacity" integer;

ALTER TABLE station_status_vehicle_type RENAME COLUMN "num_docks_available" TO "vehicle_docks_available";
ALTER TABLE station_status_vehicle_type ADD COLUMN "count" integer;

-- Static data for Rekola
INSERT INTO pricing_plan_payment ("pricing_plan_id", "payment_method", "created_at", "updated_at")
    VALUES
    ('4b72f960-84cf-11ec-a8a3-0242ac120002', 'CARD_ONLINE', NOW(), NOW()),
    ('4b72f960-84cf-11ec-a8a3-0242ac120002', 'CARD_OFFLINE', NOW(), NOW()),
    ('4b72f960-84cf-11ec-a8a3-0242ac120002', 'CASH', NOW(), NOW()),
    ('4b72f960-84cf-11ec-a8a3-0242ac120002', 'COINS_ONLY', NOW(), NOW()),
    ('4b72f960-84cf-11ec-a8a3-0242ac120002', 'MOBILE_APP', NOW(), NOW()),
    ('4b72f960-84cf-11ec-a8a3-0242ac120002', 'LITACKA', NOW(), NOW()),
    ('4b72f960-84cf-11ec-a8a3-0242ac120002', 'SMS_PAYMENT', NOW(), NOW()),
    ('4b72f960-84cf-11ec-a8a3-0242ac120002', 'APPLE_PAY', NOW(), NOW()),
    ('4b72f960-84cf-11ec-a8a3-0242ac120002', 'GOOGLE_PAY', NOW(), NOW()),
    ('4b72f960-84cf-11ec-a8a3-0242ac120002', 'UNKNOWN', NOW(), NOW());

UPDATE pricings SET ("pricing_type", "rate", "interval") = ('per_min_pricing', 42, 30)
    WHERE pricing_plan_id = '4b72f960-84cf-11ec-a8a3-0242ac120002';

UPDATE system_information SET ("license_url", "terms_of_use_url") = ('https://iptoict.blob.core.windows.net/storage/Legal/GBFSLicence.txt', 'https://www.rekola.cz/podminky-cz')
    WHERE system_id = 'd727bb19-9755-40b2-9615-01fbb6180b8b';
