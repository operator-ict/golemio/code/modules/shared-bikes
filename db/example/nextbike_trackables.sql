INSERT INTO rental_apps (id,android_store_url,android_discovery_url,ios_store_url,ios_discovery_url,web_url,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by)
VALUES
    ('9f09907a-464e-5c5d-8a6c-76a2948cecd4',NULL,'https://app.nextbike.net/station?id=27582806',NULL,'https://app.nextbike.net/station?id=27582806','https://nxtb.it/p/27582806',NULL,'2022-06-24 10:42:01.965+02',NULL,NULL,'2023-03-20 12:51:01.325+01',NULL),
	('mobility-operator-app-nextbike_tg','https://app.nextbike.net/','https://app.nextbike.net/','https://apps.apple.com/app/id504288371','nextbike://','https://www.nextbikeczech.com',NULL,'2023-02-23 11:03:03.531',NULL,NULL,'2023-03-20 12:58:06.698',NULL);

INSERT INTO system_information (operator_id,system_id,"language",logo,"name",short_name,"operator",url,purchase_url,start_date,phone_number,email,feed_contact_email,timezone,license_id,license_url,attribution_organization_name,attribution_url,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by,terms_of_use_url,rental_app_id)
VALUES
    ('nextbike_tg','8586cf7d-7fc9-5c3e-8805-e83481f13e3c','cs','https://upload.wikimedia.org/wikipedia/commons/f/f3/Nextbike_Logo.svg','Nextbike Praha',NULL,'nextbike Czech Republic, Libusina 526/101, 779 00 Olomouc','https://www.nextbikeczech.com/',NULL,'2022-06-20 02:00:00.000','+420 5816 52047','servis@nextbikeczech.com',NULL,'Europe/Berlin','CC0-1.0',NULL,NULL,NULL,NULL,'2022-06-24 10:42:08.263',NULL,NULL,'2023-03-20 12:56:06.759',NULL,'https://www.nextbikeczech.com/vseobecne-obchodni-podminky/','mobility-operator-app-nextbike_tg');

INSERT INTO pricing_plans (id,system_id,url,last_updated,"name",currency,price,is_taxable,description,surge_pricing,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by)
VALUES
	('nextbike-nextbike_tg-Rate0337','8586cf7d-7fc9-5c3e-8805-e83481f13e3c','https://www.nextbikeczech.com/kompletni-cenik/','2023-03-20 12:59:39.000','CZK 30/30 min, CZK 300/24h','CZK',0.0,false,'CZK 30/30 min, CZK 300/24h',false,NULL,'2022-07-26 16:26:24.478',NULL,NULL,'2023-03-20 13:00:13.514',NULL);

INSERT INTO station_information (id,"name",short_name,point,address,post_code,cross_street,region_id,rental_methods,is_virtual_station,station_area,vehicle_capacity,vehicle_type_capacity,is_valet_station,rental_app_id,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by,capacity,system_id)
VALUES
    ('nextbike-27582806','P7 - OC Stromovka','46065','SRID=4326;POINT (14.430091 50.101629)',NULL,NULL,NULL,'661',NULL,false,NULL,NULL,NULL,NULL,'9f09907a-464e-5c5d-8a6c-76a2948cecd4',NULL,'2023-01-05 02:18:20.687',NULL,NULL,'2023-03-20 12:43:06.071',NULL,10,'8586cf7d-7fc9-5c3e-8805-e83481f13e3c'),
    ('nextbike-27582843','P5-Nemocnice Motol výstup z metra','46070','SRID=4326;POINT (14.34161 50.07532)',NULL,NULL,NULL,'661',NULL,false,NULL,NULL,NULL,NULL,'mobility-operator-app-nextbike_tg',NULL,'2023-01-05 02:18:20.687',NULL,NULL,'2023-03-20 12:43:06.071',NULL,10,'8586cf7d-7fc9-5c3e-8805-e83481f13e3c');

WITH init_values AS (
    SELECT CAST(current_date - INTERVAL '1 day' + INTERVAL '10 hour' AS TIMESTAMPTZ) AT TIME ZONE 'Europe/Prague' AS start_datetime
)
INSERT INTO station_status (station_id,num_bikes_available,num_bikes_disabled,num_docks_available,is_installed,is_renting,is_returning,last_reported,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by)
VALUES
    ('nextbike-27582806',1,NULL,9,true,true,true,(SELECT start_datetime FROM init_values),NULL,'2022-08-04 07:26:30.224',NULL,NULL,'2023-03-20 12:45:08.985',NULL),
    ('nextbike-27582843',0,NULL,10,true,true,true,(SELECT start_datetime FROM init_values),NULL,'2022-08-04 07:26:30.224',NULL,NULL,'2023-03-20 12:45:08.985',NULL);

INSERT INTO vehicle_types (id,form_factor,propulsion_type,max_range_meters,"name",create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by)
VALUES
    ('nextbike-bike','bicycle','human',NULL,'bicycle',NULL,'2022-04-21 12:20:09.472',NULL,NULL,'2023-03-20 12:47:17.754',NULL);

INSERT INTO station_status_vehicle_type (station_id,vehicle_type_id,num_bikes_available,num_bikes_disabled,vehicle_docks_available,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by,count)
VALUES
    ('nextbike-27582806','nextbike-bike',1,NULL,9,NULL,'2022-08-04 07:26:41.834',NULL,NULL,'2023-03-20 12:48:11.493',NULL,1),
    ('nextbike-27582843','nextbike-bike',0,NULL,10,NULL,'2022-08-04 07:26:41.834',NULL,NULL,'2023-03-20 12:48:11.493',NULL,0);

WITH init_values AS (
    SELECT CAST(current_date - INTERVAL '1 day' + INTERVAL '10 hour' AS TIMESTAMPTZ) AT TIME ZONE 'Europe/Prague' AS start_datetime
)
INSERT INTO bike_status (id,system_id,point,helmets,passengers,damage_description,description,vehicle_registration,is_reserved,is_disabled,vehicle_type_id,last_reported,current_range_meters,charge_percent,rental_app_id,station_id,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by,pricing_plan_id,make,model,color)
VALUES
    ('nextbike-pdyLwe2j8gWmQ9BcdX5epxnqEvlA5NRZ','8586cf7d-7fc9-5c3e-8805-e83481f13e3c','SRID=4326;POINT (14.430091 50.101629)',NULL,NULL,NULL,NULL,NULL,false,false,'nextbike-bike',(SELECT start_datetime FROM init_values),NULL,NULL,'9f09907a-464e-5c5d-8a6c-76a2948cecd4','nextbike-27582806',NULL,'2023-03-18 18:55:06.358',NULL,NULL,NOW(),NULL,'nextbike-nextbike_tg-Rate0337',NULL,NULL,NULL),
    ('nextbike-4Vz7XLdKBnw31M4tjzJGbRnPRamMEW89','8586cf7d-7fc9-5c3e-8805-e83481f13e3c','SRID=4326;POINT (14.430091 50.101629)',NULL,NULL,NULL,NULL,NULL,false,false,'nextbike-bike',(SELECT start_datetime FROM init_values),NULL,NULL,'9f09907a-464e-5c5d-8a6c-76a2948cecd4','nextbike-27582806',NULL,'2023-03-17 11:23:07.297',NULL,NULL,NOW(),NULL,'nextbike-nextbike_tg-Rate0337',NULL,NULL,NULL),
    ('nextbike-kY9eaq3JPDZ8LO5iPjqrpR6oKQr27A4z','8586cf7d-7fc9-5c3e-8805-e83481f13e3c','SRID=4326;POINT (14.418211 50.083791)',NULL,NULL,NULL,NULL,NULL,false,false,'nextbike-bike',(SELECT start_datetime FROM init_values),NULL,NULL,NULL,NULL,NULL,'2023-03-17 12:57:06.286',NULL,NULL,NOW(),NULL,'nextbike-nextbike_tg-Rate0337',NULL,NULL,NULL);
