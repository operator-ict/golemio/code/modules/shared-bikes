TRUNCATE
    "geofencing_zones",
    "bike_status",
    "station_status_vehicle_type",
    "vehicle_types",
    "station_status",
    "station_information";

UPDATE system_information
SET rental_app_id='bb622abc-84a1-11ec-a8a3-0242ac120002'
WHERE system_id = 'd727bb19-9755-40b2-9615-01fbb6180b8b';

DELETE FROM pricings WHERE pricing_plan_id LIKE 'nextbike-%';
DELETE FROM pricing_plans WHERE system_id IN (
   '8586cf7d-7fc9-5c3e-8805-e83481f13e3c',
   'ef63ac2e-4c71-4c28-a496-f0dc010d630b',
   'ajo'
);

DELETE FROM system_information WHERE rental_app_id IN (
   '9f09907a-464e-5c5d-8a6c-76a2948cecd4',
   'mobility-operator-app-nextbike_tg',
   'mobility-operator-app-ajo',
   'mobility-operator-app-hoppygo',
   'ajo_rental_apps',
   'hoppygo_rental_apps'
);

DELETE FROM "rental_apps" WHERE id IN (
   '012eb469-6d60-4ad4-9249-ccbe7a19f102',
   '3b1be791-dea5-46be-bc91-debf49b4e97a',
   'mobility-operator-app-rekola',
   '9f09907a-464e-5c5d-8a6c-76a2948cecd4',
   'mobility-operator-app-nextbike_tg',
   'mobility-operator-app-ajo',
   'mobility-operator-app-hoppygo',
   'ajo_rental_apps',
   'hoppygo_rental_apps'
);
