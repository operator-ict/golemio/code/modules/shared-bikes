INSERT INTO rental_apps (id,android_store_url,android_discovery_url,ios_store_url,ios_discovery_url,web_url,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by)
VALUES
	('mobility-operator-app-ajo','https://www.ajo.cz/login',NULL,'https://www.ajo.cz/login',NULL,'https://www.ajo.cz/',NULL,'2023-02-23 11:04:01.072',NULL,NULL,'2023-03-21 10:48:01.642',NULL),
	('mobility-operator-app-hoppygo','https://play.google.com/store/apps/details?id=cz.nom.smilecarweb',NULL,'https://itunes.apple.com/cz/app/hoppygo/id1184613465?l=cs&mt=8',NULL,'https://hoppygo.com/cs/',NULL,'2023-02-23 11:04:02.333',NULL,NULL,'2023-03-21 10:48:01.803',NULL),
	('ajo_rental_apps',NULL,NULL,NULL,NULL,'https://www.ajo.cz/login',NULL,'2022-10-27 10:58:01.651',NULL,NULL,'2023-02-21 13:40:06.126',NULL),
	('hoppygo_rental_apps','https://play.google.com/store/apps/details?id=cz.nom.smilecarweb',NULL,'https://itunes.apple.com/cz/app/hoppygo/id1184613465?l=cs&mt=8',NULL,NULL,NULL,'2022-08-25 10:22:00.635',NULL,NULL,'2023-02-21 13:40:07.616',NULL);

INSERT INTO system_information (operator_id,system_id,"language",logo,name,short_name,"operator",url,purchase_url,start_date,phone_number,email,feed_contact_email,timezone,license_id,license_url,attribution_organization_name,attribution_url,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by,terms_of_use_url,rental_app_id)
VALUES
	('ajo','ajo','cs','','AJO','AJO',NULL,NULL,'https://www.ajo.cz/','2022-10-01 14:00:00.000','+420 530 514 214','info@ajo.cz','info@ajo.cz','Europe/Prague',NULL,'https://iptoict.blob.core.windows.net/storage/Legal/GBFSLicence.txt',NULL,NULL,NULL,'2022-10-27 10:58:02.140',NULL,NULL,'2023-03-21 10:46:03.715',NULL,'https://www.ajo.cz/data/op/ajo-op-v3_0.pdf','mobility-operator-app-ajo'),
	('hoppygo','ef63ac2e-4c71-4c28-a496-f0dc010d630b','cs','https://scontent.xx.fbcdn.net/v/t1.6435-1/cp0/p80x80/36342579_1573737729401511_169288730132086784_n.png?_nc_cat=110&ccb=1-5&_nc_sid=dbb9e7&_nc_ohc=7vzGRNqrDC8AX9GLL2p&_nc_ht=scontent.xx&edm=ACzcWGEEAAAA&oh=fdf63a63daad762708153fcea1c1d47f&oe=61AFD68B','HoppyGo','HoppyGo',NULL,NULL,'https://hoppygo.com/cs/','2021-11-01 01:00:00.000','+420 220 311 769','info@hoppygo.com','info@hoppygo.com','Europe/Prague',NULL,'https://iptoict.blob.core.windows.net/storage/Legal/GBFSLicence.txt',NULL,NULL,NULL,'2022-09-15 09:56:43.180',NULL,NULL,'2023-03-21 10:46:03.614',NULL,'https://www.hoppygo.com/files/cz/HoppyGo_VOP_20221027.pdf','mobility-operator-app-hoppygo');

INSERT INTO pricing_plans (id,system_id,url,last_updated,"name",currency,price,is_taxable,description,surge_pricing,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by)
VALUES
	('hoppygo_individual_rides_13910','ef63ac2e-4c71-4c28-a496-f0dc010d630b',NULL,'2023-03-21 10:58:00.645','Ceník HoppyGo 13910','CZK',649.0,false,'Cena za den 649 Kč',false,NULL,'2023-03-07 13:24:02.811',NULL,NULL,'2023-03-21 10:58:02.635',NULL),
	('hoppygo_individual_rides_9943','ef63ac2e-4c71-4c28-a496-f0dc010d630b',NULL,'2023-03-21 10:58:00.645','Ceník HoppyGo 9943','CZK',1409.0,false,'Cena za den 1409 Kč',false,NULL,'2022-08-25 10:22:01.590',NULL,NULL,'2023-03-21 10:58:02.635',NULL),
	('ajo_tarif_top','ajo','https://www.ajo.cz/cenik','2022-10-18 16:56:00.000','Ceník AJO tarif TOP','CZK',119.0,false,'Tarif TOP',false,NULL,'2022-10-27 10:58:02.249',NULL,NULL,'2023-03-21 10:58:04.053',NULL);

INSERT INTO vehicle_types (id,form_factor,propulsion_type,max_range_meters,"name",create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by)
VALUES
	('hoppygo_car_electric','car','electric',NULL,'HoppyGo Car Electric',NULL,'2022-08-25 10:22:01.073',NULL,NULL,'2023-03-21 10:48:01.826',NULL),
	('hoppygo_car_combustion','car','combustion',NULL,'HoppyGo Car Combustion',NULL,'2022-08-25 10:22:01.073',NULL,NULL,'2023-03-21 10:48:01.826',NULL),
	('carsharing_electric','car','electric',NULL,'Carsharing Electric',NULL,'2022-10-27 10:58:02.164',NULL,NULL,'2023-03-21 10:48:02.107',NULL),
	('carsharing_benzin','car','combustion',NULL,'Carsharing Benzin',NULL,'2022-10-27 10:58:02.164',NULL,NULL,'2023-03-21 10:48:02.107',NULL),
	('carsharing_diesel','car','combustion',NULL,'Carsharing Diesel',NULL,'2022-10-27 10:58:02.164',NULL,NULL,'2023-03-21 10:48:02.107',NULL),
	('carsharing_hybrid','car','combustion',NULL,'Carsharing Hybrid',NULL,'2022-10-27 10:58:02.164',NULL,NULL,'2023-03-21 10:48:02.107',NULL),
	('carsharing_benzin_LPG','car','combustion',NULL,'Carsharing benzin + LPG',NULL,'2022-10-27 10:58:02.164',NULL,NULL,'2023-03-21 10:48:02.107',NULL);

WITH init_values AS (
    SELECT CAST(current_date - INTERVAL '1 day' + INTERVAL '10 hour' AS TIMESTAMPTZ) AT TIME ZONE 'Europe/Prague' AS start_datetime
)
INSERT INTO bike_status (id,system_id,point,helmets,passengers,damage_description,description,vehicle_registration,is_reserved,is_disabled,vehicle_type_id,last_reported,current_range_meters,charge_percent,rental_app_id,station_id,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by,pricing_plan_id,make,model,color)
VALUES
	('hoppygo-13910','ef63ac2e-4c71-4c28-a496-f0dc010d630b','SRID=4326;POINT (14.423982 50.074558)',NULL,NULL,NULL,'Peugeot 207',NULL,false,false,'hoppygo_car_combustion',(SELECT start_datetime FROM init_values),NULL,NULL,'hoppygo_rental_apps',NULL,NULL,'2023-03-07 13:24:15.923',NULL,NULL,'2023-03-21 10:52:13.997',NULL,'hoppygo_individual_rides_13910','Peugeot','207',NULL),
	('hoppygo-9943','ef63ac2e-4c71-4c28-a496-f0dc010d630b','SRID=4326;POINT (18.29234 49.839294)',NULL,NULL,NULL,'Ostatní Other',NULL,false,false,'hoppygo_car_electric',(SELECT start_datetime FROM init_values),NULL,NULL,'hoppygo_rental_apps',NULL,NULL,'2022-08-25 10:22:10.339',NULL,NULL,'2023-03-21 10:52:13.997',NULL,'hoppygo_individual_rides_9943','Ostatní','Other',NULL),
	('cesky-carsharing-1BF8202','ajo','SRID=4326;POINT (14.4099 50.0731)',NULL,NULL,NULL,'Fabia III kombi  ','1BF8202',false,false,'carsharing_benzin_LPG',(SELECT start_datetime FROM init_values),NULL,NULL,'ajo_rental_apps',NULL,NULL,'2022-11-08 15:00:20.813',NULL,NULL,'2023-03-21 10:54:04.878',NULL,'ajo_tarif_top','Škoda','Fabia III kombi',NULL),
	('cesky-carsharing-1BF8203','ajo','SRID=4326;POINT (14.4423 50.0906)',NULL,NULL,NULL,'Fabia III kombi  ','1BF8203',false,false,'carsharing_benzin_LPG',(SELECT start_datetime FROM init_values),NULL,NULL,'ajo_rental_apps',NULL,NULL,'2022-11-08 15:00:20.813',NULL,NULL,'2023-03-21 10:54:04.878',NULL,'ajo_tarif_top','Škoda','Fabia III kombi',NULL),
	('cesky-carsharing-1BF8207','ajo','SRID=4326;POINT (14.4711 50.1324)',NULL,NULL,NULL,'Fabia III kombi  ','1BF8207',false,false,'carsharing_benzin_LPG',(SELECT start_datetime FROM init_values),NULL,NULL,'ajo_rental_apps',NULL,NULL,'2022-11-08 15:00:20.813',NULL,NULL,'2023-03-21 10:54:04.878',NULL,'ajo_tarif_top','Škoda','Fabia III kombi',NULL),
	('cesky-carsharing-1BF8210','ajo','SRID=4326;POINT (14.4579 50.0793)',NULL,NULL,NULL,'Fabia III kombi  ','1BF8210',false,false,'carsharing_benzin_LPG',(SELECT start_datetime FROM init_values),NULL,NULL,'ajo_rental_apps',NULL,NULL,'2022-11-08 15:00:20.862',NULL,NULL,'2023-03-21 10:54:04.878',NULL,'ajo_tarif_top','Škoda','Fabia III kombi',NULL);
