```mermaid
classDiagram
rental_apps --> system_information: id - rental_app_id
pricing_plans --> system_information: system_information_id - system_id
pricings --> pricing_plans: pricing_plan_id - id
pricing_plan_payment --> pricing_plans: pricing_plan_id - id
geofencing_zones --> system_information: system_id - system_id
bike_status --> system_information: system_id - system_id
vehicle_types --> bike_status: id - vehicle_type_id
rental_apps --> bike_status: id - rental_app_id
station_information --> bike_status:id - station_id
station_information --> system_information: system_id - system_id
pricing_plans --> bike_status: id - pricing_plan_id
rental_apps --> station_information: id - rental_app_id
station_information --> station_status:id - station_id
station_status_vehicle_type --> vehicle_types: vehicle_type_id - id
station_status_vehicle_type --> station_information: station_id - id

class system_information {
  system_id string
  operator_id string
  language string
  logo url
  name string
  short_name string
  operator string
  url url
  purchase_url url
  start_date timestamp
  phone_number string
  email string
  feed_contact_email string
  timezone string
  license_id string
  license_url url
  terms_of_use_url string
  attribution_organization_name string
  attribution_url url
  rental_app_id int
}

class rental_apps {
  id string
  android_store_uri url
  android_discovery_uri url
  ios_store_uri url
  ios_discovery_uri url
  web_uri url
}

class pricing_plans {
  system_information_id string
  id string
  url url
  last_updated timestamp
  name string
  currency string
  price float
  is_taxable bool
  description string
  surge_pricing boolean
}

class pricings {
  id string
  pricing_plan_id string
  pricing_type string
  pricing_order int
  start int
  rate float
  interval int
  end int
  start_time_of_period string
  end_time_of_period string
}

class pricing_plan_payment {
  pricing_plan_id string
  payment_method string
}

class geofencing_zones {
  id string
  system_id string
  name string
  note string
  source string
  price int
  priority int
  start timestamp
  end timestamp
  geom geom
  ride_allowed bool
  ridethrough_allowed bool
  parking_allowed bool
  maximum_speed_kmh int
}

class bike_status {
  bike_id string
  system_id string
  point geom
  helmets int
  passengers int
  damage_description string
  description string
  vehicle_registration string
  is_reserved bool
  is_disabled bool
  vehicle_type_id int
  last_reported timestamp
  current_range_meters float
  charge_percent int
  rental_app_id int
  station_id string
  pricing_plan_id id
  make
  model
  color
}

class vehicle_types  {
  id string
  form_factor string
  propulsion_type string
  max_range_meters float
  name string
}

class station_information {
  id string
  system_id string
  name string
  short_name string
  point geom
  capacity int
  address string
  post_code string
  cross_street string
  region_id string
  rental_methods string
  is_virtual_station bool
  station_area geom
  capacity int
  vehicle_capacity int
  vehicle_type_capacity object
  is_valet_station bool
  rental_app_id int
}

class station_status  {
  station_id string
  num_bikes_available int
  num_bikes_disabled int
  num_docks_available int
  is_installed bool
  is_renting bool
  is_returning bool
  last_reported timestamp
}

class station_status_vehicle_type {
  station_id string
  count int
  vehicle_type_id string
  num_bikes_available int
  vehicle_docks_available int
  num_docks_disabled int
}
```
