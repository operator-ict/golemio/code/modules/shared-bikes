import { ISharedBikesSystemInformationOutput } from "#sch";

export const systemIdFixture = "8586cf7d-7fc9-5c3e-8805-e83481f13e3c";

export const NextbikeSystemInformationFixture: ISharedBikesSystemInformationOutput = {
    operator_id: "nextbike_tg",
    system_id: systemIdFixture,
    language: "cs",
    logo: "https://upload.wikimedia.org/wikipedia/commons/f/f3/Nextbike_Logo.svg",
    name: "Nextbike Praha",
    short_name: null,
    operator: "nextbike Czech Republic, Libusina 526/101, 779 00 Olomouc",
    url: "https://www.nextbikeczech.com/",
    purchase_url: null,
    start_date: new Date("2022-06-20 00:00:00.000000+00:00"),
    phone_number: "+420 5816 52047",
    email: "servis@nextbikeczech.com",
    feed_contact_email: null,
    timezone: "Europe/Berlin",
    license_id: "CC0-1.0",
    license_url: null,
    attribution_organization_name: null,
    attribution_url: null,
    terms_of_use_url: "https://www.nextbikeczech.com/vseobecne-obchodni-podminky/",
    rental_app_id: "mobility-operator-app-nextbike_tg",
};
