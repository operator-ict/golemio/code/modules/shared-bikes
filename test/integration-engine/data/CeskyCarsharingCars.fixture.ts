export const CeskyCarsharingCarsFixture = [
    {
        id: "cesky-carsharing-1BF8203",
        system_id: "ajo",
        point: {
            type: "Point",
            coordinates: [14.4441, 50.0908],
        },
        helmets: null,
        passengers: null,
        damage_description: null,
        description: "Fabia III kombi  ",
        vehicle_registration: "1BF8203",
        is_reserved: false,
        is_disabled: false,
        vehicle_type_id: "carsharing_benzin_LPG",
        last_reported: "2022-10-18T11:24:00.000Z",
        current_range_meters: null,
        charge_percent: null,
        rental_app_id: "ajo_rental_apps",
        station_id: null,
        pricing_plan_id: "ajo_tarif_top",
        make: "Škoda",
        model: "Fabia III kombi",
        color: null,
    },
    {
        id: "cesky-carsharing-1BR5670",
        system_id: "autonapul",
        point: {
            type: "Point",
            coordinates: [14.39218, 50.10577],
        },
        helmets: null,
        passengers: null,
        damage_description: null,
        description: "Škoda Citigo",
        vehicle_registration: "1BR5670",
        is_reserved: false,
        is_disabled: false,
        vehicle_type_id: "carsharing_benzin",
        last_reported: "2022-10-18T11:24:00.000Z",
        current_range_meters: null,
        charge_percent: null,
        rental_app_id: "autonapul_rental_apps",
        station_id: null,
        pricing_plan_id: "autonapul_economy",
        make: "Škoda",
        model: "Citigo",
        color: null,
    },
    {
        id: "cesky-carsharing-1BR1408",
        system_id: "autonapul",
        point: {
            type: "Point",
            coordinates: [14.47144, 50.0685],
        },
        helmets: null,
        passengers: null,
        damage_description: null,
        description: "Ford Transit",
        vehicle_registration: "1BR1408",
        is_reserved: false,
        is_disabled: false,
        vehicle_type_id: "carsharing_diesel",
        last_reported: "2022-10-18T11:24:00.000Z",
        current_range_meters: null,
        charge_percent: null,
        rental_app_id: "autonapul_rental_apps",
        station_id: null,
        pricing_plan_id: "autonapul_grand",
        make: "Ford",
        model: "Transit",
        color: null,
    },
    {
        id: "cesky-carsharing-EL350AJ",
        system_id: "autonapul",
        point: {
            type: "Point",
            coordinates: [16.575947, 49.232048],
        },
        helmets: null,
        passengers: null,
        damage_description: null,
        description: "Škoda CitigoE",
        vehicle_registration: "EL350AJ",
        is_reserved: false,
        is_disabled: false,
        vehicle_type_id: "carsharing_electric",
        last_reported: "2022-10-18T11:24:00.000Z",
        current_range_meters: null,
        charge_percent: null,
        rental_app_id: "autonapul_rental_apps",
        station_id: null,
        pricing_plan_id: "autonapul_electro",
        make: "Škoda",
        model: "CitigoE",
        color: null,
    },
    {
        id: "cesky-carsharing-2BS3449",
        system_id: "autonapul",
        point: {
            type: "Point",
            coordinates: [14.476422, 50.10384],
        },
        helmets: null,
        passengers: null,
        damage_description: null,
        description: "jiné Sandero",
        vehicle_registration: "2BS3449",
        is_reserved: false,
        is_disabled: false,
        vehicle_type_id: "carsharing_benzin_LPG",
        last_reported: "2022-10-18T11:24:00.000Z",
        current_range_meters: null,
        charge_percent: null,
        rental_app_id: "autonapul_rental_apps",
        station_id: null,
        pricing_plan_id: "autonapul_economy",
        make: "Dacia",
        model: "Sandero",
        color: null,
    },
    {
        id: "cesky-carsharing-5ST0420",
        system_id: "car4way",
        point: {
            type: "Point",
            coordinates: [14.465196, 50.105687],
        },
        helmets: null,
        passengers: null,
        damage_description: null,
        description: "Škoda Scala",
        vehicle_registration: "5ST0420",
        is_reserved: false,
        is_disabled: false,
        vehicle_type_id: "carsharing_benzin",
        last_reported: "2022-10-18T11:24:00.000Z",
        current_range_meters: null,
        charge_percent: null,
        rental_app_id: "car4way_rental_apps",
        station_id: null,
        pricing_plan_id: "car4way_optimum",
        make: "Škoda",
        model: "Scala",
        color: null,
    },
    {
        id: "cesky-carsharing-5SK0489",
        system_id: "car4way",
        point: {
            type: "Point",
            coordinates: [14.432001, 50.128679],
        },
        helmets: null,
        passengers: null,
        damage_description: null,
        description: "VW Caddy",
        vehicle_registration: "5SK0489",
        is_reserved: false,
        is_disabled: false,
        vehicle_type_id: "carsharing_diesel",
        last_reported: "2022-10-18T11:24:00.000Z",
        current_range_meters: null,
        charge_percent: null,
        rental_app_id: "car4way_rental_apps",
        station_id: null,
        pricing_plan_id: "car4way_master",
        make: "VW",
        model: "Caddy",
        color: null,
    },
    {
        id: "cesky-carsharing-8AB2335",
        system_id: "anytime",
        point: {
            type: "Point",
            coordinates: [14.431817054748535, 50.065193176269531],
        },
        helmets: null,
        passengers: null,
        damage_description: null,
        description: "Toyota Toyota Yaris",
        vehicle_registration: "8AB2335",
        is_reserved: false,
        is_disabled: false,
        vehicle_type_id: "carsharing_hybrid",
        last_reported: "2022-10-18T11:24:00.000Z",
        current_range_meters: null,
        charge_percent: null,
        rental_app_id: "anytime_rental_apps",
        station_id: null,
        pricing_plan_id: "anytime_yaris",
        make: "Toyota",
        model: "Toyota Yaris",
        color: null,
    },
    {
        id: "cesky-carsharing-8AT6113",
        system_id: "anytime",
        point: {
            type: "Point",
            coordinates: [14.514374732971191, 50.024200439453125],
        },
        helmets: null,
        passengers: null,
        damage_description: null,
        description: "Toyota Toyota C-HR Impress",
        vehicle_registration: "8AT6113",
        is_reserved: false,
        is_disabled: false,
        vehicle_type_id: "carsharing_hybrid",
        last_reported: "2022-10-18T11:24:00.000Z",
        current_range_meters: null,
        charge_percent: null,
        rental_app_id: "anytime_rental_apps",
        station_id: null,
        pricing_plan_id: "anytime_chr",
        make: "Toyota",
        model: "Toyota C-HR Impress",
        color: null,
    },
    {
        id: "cesky-carsharing-2BE0547",
        system_id: "ajo",
        point: {
            type: "Point",
            coordinates: [16.6134, 49.2016],
        },
        helmets: null,
        passengers: null,
        damage_description: null,
        description: "SUV Sportage  ",
        vehicle_registration: "2BE0547",
        is_reserved: false,
        is_disabled: false,
        vehicle_type_id: "carsharing_benzin_LPG",
        last_reported: "2022-10-18T11:24:00.000Z",
        current_range_meters: null,
        charge_percent: null,
        rental_app_id: "ajo_rental_apps",
        station_id: null,
        pricing_plan_id: "ajo_tarif_top_suv",
        make: "Kia",
        model: "Sportage SUV",
        color: null,
    },
];
