import { ISharedBikesStationStatusVehicleTypeOutput } from "#sch";

export const rekolaStationStatusVehicleTypeOutputFixture: ISharedBikesStationStatusVehicleTypeOutput[] = [
    {
        station_id: "rekola-4",
        vehicle_type_id: "rekola-bike",
        count: 1,
        num_bikes_available: 1,
        num_bikes_disabled: null,
        vehicle_docks_available: null,
    },
    {
        station_id: "rekola-5",
        vehicle_type_id: "rekola-bike",
        count: 2,
        num_bikes_available: 2,
        num_bikes_disabled: null,
        vehicle_docks_available: null,
    },
    {
        station_id: "rekola-6",
        vehicle_type_id: "rekola-bike",
        count: 1,
        num_bikes_available: 1,
        num_bikes_disabled: null,
        vehicle_docks_available: null,
    },
    {
        station_id: "rekola-200014",
        vehicle_type_id: "rekola-bike",
        count: 1,
        num_bikes_available: 1,
        num_bikes_disabled: null,
        vehicle_docks_available: null,
    },
    {
        station_id: "rekola-200029",
        vehicle_type_id: "rekola-bike",
        count: 1,
        num_bikes_available: 1,
        num_bikes_disabled: null,
        vehicle_docks_available: null,
    },
    {
        station_id: "rekola-32",
        vehicle_type_id: "rekola-bike",
        count: 1,
        num_bikes_available: 1,
        num_bikes_disabled: null,
        vehicle_docks_available: null,
    },
];
