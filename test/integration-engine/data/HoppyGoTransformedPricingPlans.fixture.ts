import { ISharedBikesPricingPlanOutput } from "#sch";
import { HOPPYGO_PRICING_PLAN_PREFIX, HOPPYGO_SYSTEM_ID } from "#ie/transformations/HoppyGo/transformConstants";

export const hoppyGoPricingPlanTransformationFixture = (transformationDate: Date): ISharedBikesPricingPlanOutput[] => [
    {
        id: HOPPYGO_PRICING_PLAN_PREFIX + "7437",
        system_id: HOPPYGO_SYSTEM_ID,
        url: null,
        last_updated: transformationDate.toISOString(),
        name: "Ceník HoppyGo 7437",
        currency: "CZK",
        price: 100,
        is_taxable: false,
        description: `Cena za den 100 Kč`,
        surge_pricing: false,
    },
    {
        id: HOPPYGO_PRICING_PLAN_PREFIX + "12472",
        system_id: HOPPYGO_SYSTEM_ID,
        url: null,
        last_updated: transformationDate.toISOString(),
        name: "Ceník HoppyGo 12472",
        currency: "CZK",
        price: 650,
        is_taxable: false,
        description: `Cena za den 650 Kč`,
        surge_pricing: false,
    },
];
