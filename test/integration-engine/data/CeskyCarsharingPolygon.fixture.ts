import { ISharedBikesGeofencingZoneOutput } from "#sch";

export const CeskyCarsharingPolygonFixture: ISharedBikesGeofencingZoneOutput[] = [
    {
        id: "3084de00-55cd-53c9-ad74-d02348419e2a",
        system_id: "car4way",
        name: null,
        note: null,
        source: null,
        price: null,
        priority: 0,
        start: null,
        end: null,
        geom: {
            type: "MultiPolygon",
            coordinates: [
                [
                    [
                        [16.6450779344401, 49.2312769852182],
                        [16.642902214588, 49.234428856644],
                        [16.6392114949835, 49.2383097849608],
                        [16.6412285161627, 49.2399909588816],
                        [16.6460779500617, 49.2370068358105],
                        [16.649017651142, 49.2333920227371],
                    ],
                ],
            ],
        },
        ride_allowed: true,
        ride_through_allowed: true,
        maximum_speed_kph: null,
        parking_allowed: true,
    },
    {
        id: "dbc39cbf-d21e-545c-98fb-58d8a50cd718",
        system_id: "anytime",
        name: null,
        note: null,
        source: null,
        price: null,
        priority: 0,
        start: null,
        end: null,
        geom: {
            type: "MultiPolygon",
            coordinates: [
                [
                    [
                        [14.5888527, 50.111728],
                        [14.5935528, 50.1054842],
                        [14.601685, 50.1046318],
                        [14.6043451, 50.1001677],
                        [14.6110836, 50.1016048],
                        [14.6254173, 50.1080457],
                        [14.6277347, 50.1112383],
                        [14.6305664, 50.1161037],
                        [14.6297946, 50.1191639],
                        [14.5888527, 50.111728],
                    ],
                ],
            ],
        },
        ride_allowed: true,
        ride_through_allowed: true,
        maximum_speed_kph: null,
        parking_allowed: true,
    },
];
