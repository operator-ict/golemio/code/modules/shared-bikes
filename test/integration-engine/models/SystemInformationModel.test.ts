import { SystemInformationModel } from "#ie/models/SystemInformationModel";
import { MobilityOperatorRentalAppProvider } from "#ie/transformations/MobilityOperator/MobilityOperatorRentalAppProvider";
import { MobilityOperatorTransformation } from "#ie/transformations/MobilityOperator/MobilityOperatorTransformation";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";
import { FakeDataSource } from "../transformations/MobilityOperator/FakeDataSource";
import { HoppyGoStaticDataService } from "#ie/HoppyGoStaticDataService";
import { HOPPYGO_SYSTEM_ID } from "#ie/transformations/HoppyGo/transformConstants";
import { NextbikeSystemInformationFixture } from "../data/NextbikeSystemInformation.fixture";

describe("SystemInformationModel", () => {
    let systemInformationModel: SystemInformationModel;
    let sandbox: SinonSandbox;
    let staticDataService: HoppyGoStaticDataService;

    beforeEach(async () => {
        sandbox = sinon.createSandbox();
        await PostgresConnector.connect();
        systemInformationModel = new SystemInformationModel();
        const mobilityOperatorRentalAppProvider = new MobilityOperatorRentalAppProvider(
            new FakeDataSource() as any as DataSource,
            new MobilityOperatorTransformation()
        );
        staticDataService = new HoppyGoStaticDataService(mobilityOperatorRentalAppProvider);
        await staticDataService.saveSystemBasics();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("getOne() gets correct row", async () => {
        const row = await systemInformationModel.GetOne(HOPPYGO_SYSTEM_ID);
        expect(row.system_id).to.eq(HOPPYGO_SYSTEM_ID);
        // compare some data...
        expect(row.name).to.eq("HoppyGo");
        expect(row.email).to.eq("info@hoppygo.com");
    });

    it("deleteBySystemId() deletes given row", async () => {
        const systemId = "systemToDelete";

        let systemInformationToDelete = structuredClone(NextbikeSystemInformationFixture);
        systemInformationToDelete.system_id = systemId;
        systemInformationToDelete.operator_id = systemId;

        await systemInformationModel.save([systemInformationToDelete]);
        await systemInformationModel.deleteBySystemId(systemId);
        const deletedRow = await systemInformationModel.GetOne(systemId);
        expect(deletedRow).to.be.null;
        const notDeletedRow = await systemInformationModel.GetOne(HOPPYGO_SYSTEM_ID);
        expect(notDeletedRow).not.to.be.null;
    });
});
