import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { BikeStatusModel } from "#ie/models/BikeStatusModel";
import { ISharedBikesBikeStatusOutput } from "#sch";
import { REKOLA_SYSTEM_ID } from "#ie/transformations/Rekola";

chai.use(chaiAsPromised);

describe("BikeStatusModel", () => {
    let model: BikeStatusModel;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        PostgresConnector.connect();
        model = new BikeStatusModel();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("deleteDataBefore should delete", async () => {
        const input: Partial<ISharedBikesBikeStatusOutput> = {
            id: "rekola-bikestatus-test",
            system_id: "d727bb19-9755-40b2-9615-01fbb6180b8b",
            point: {
                type: "Point",
                coordinates: [14.4418911111, 50.1071377778],
            },
            description: "",
            is_reserved: false,
            is_disabled: false,
            vehicle_type_id: "rekola-bike",
            rental_app_id: null,
            last_reported: "2020-02-07T02:58:32.410Z",
        };

        await model.save([input]);
        const deletedRows = await model.deleteDataBefore(1581044312411, REKOLA_SYSTEM_ID);
        expect(deletedRows).to.eq(1);
    });
});
