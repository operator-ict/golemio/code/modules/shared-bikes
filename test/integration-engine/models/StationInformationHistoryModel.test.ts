import { BikeStatusModel } from "#ie/models/BikeStatusModel";
import { StationInformationHistoryModel } from "#ie/models/StationInformationHistoryModel";
import { StationStatusModel } from "#ie/models/StationStatusModel";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";
import { rekolaBikeStatusOutputFixture } from "../data/rekolaBikeStatusOutput.fixture";
import { rekolaStationInformationOutputFixtture } from "../data/rekolaStationInformationOutput.fixture";
import { rekolaStationStatusOutputFixture } from "../data/rekolaStationStatusOutput.fixture";
import { REKOLA_SYSTEM_ID } from "#ie/transformations/Rekola";

chai.use(chaiAsPromised);

describe("StationInformationHistoryModel", () => {
    let stationInformationHistoryModel: StationInformationHistoryModel;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        PostgresConnector.connect();
        stationInformationHistoryModel = new StationInformationHistoryModel();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("deleteNHoursOldData should delete", async () => {
        await stationInformationHistoryModel.save(rekolaStationInformationOutputFixtture);

        const stationStatusModel = new StationStatusModel();
        await stationStatusModel.save(rekolaStationStatusOutputFixture);

        const bikeStatusModel = new BikeStatusModel();
        await bikeStatusModel.save(rekolaBikeStatusOutputFixture);

        const deletedRows = await stationInformationHistoryModel.deleteDataBefore(1656235740001, REKOLA_SYSTEM_ID);
        expect(deletedRows).to.eq(6);
    });
});
