import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { OldDataCleanerHelper } from "#ie/helpers/OldDataCleanerHelper";
import { ModuleContainerToken, SharedBikesContainer } from "#ie/ioc";

describe("OldDataCleanerHelper", () => {
    let helper: OldDataCleanerHelper;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
                transaction: sandbox.stub(),
            })
        );

        helper = SharedBikesContainer.resolve(ModuleContainerToken.OldDataCleanerHelper);

        sandbox.stub(helper["stationInformationHistoryModel"], "deleteDataBefore");
        sandbox.stub(helper["bikeStatusModel"], "deleteDataBefore");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("deleteOldTrackableData should call the correct methods", async () => {
        await helper.deleteOldTrackableData(123456, "rekola");
        sandbox.assert.calledOnceWithExactly(
            helper["stationInformationHistoryModel"].deleteDataBefore as SinonSpy,
            123456,
            "rekola"
        );
        sandbox.assert.calledOnceWithExactly(helper["bikeStatusModel"].deleteDataBefore as SinonSpy, 123456, "rekola");
    });
});
