import { NextbikeSharedBikesWorker } from "#ie/NextbikeSharedBikesWorker";
import { NextbikeDefinitionTransformation } from "#ie/transformations/Nextbike";
import { config, DataSource } from "@golemio/core/dist/integration-engine";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { ISharedBikesSystemInformationOutput } from "#sch";

describe("NextbikeSharedBikesWorker", () => {
    let worker: NextbikeSharedBikesWorker;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
                transaction: sandbox.stub(),
                query: sandbox.stub().returns([[{}]]),
            })
        );

        sinon.stub(config, "datasources").value({
            NextbikeSharedBikes: {
                headers: {},
                baseUrl: "https://gbfs.nextbike.net/maps/gbfs/v2/",
                sourceIds: ["nextbike_tg"],
            },
        });

        worker = new NextbikeSharedBikesWorker();

        sandbox.stub(worker["dataSourceFactory"], "getDataSourceDefinition").returns({
            getAll: async () =>
                Promise.resolve({
                    last_updated: 1655289551,
                    ttl: 60,
                    data: {
                        cs: {
                            feeds: [
                                {
                                    name: "system_information",
                                    url: "https://gbfs.nextbike.net/maps/gbfs/v2/nextbike_tg/cs/system_information.json",
                                },
                            ],
                        },
                    },
                    version: "2.2",
                }),
        } as unknown as DataSource);

        sandbox.stub(worker["dataSourceFactory"], "getDataSourceData").returns({
            getAll: async () => {},
        } as unknown as DataSource);

        sandbox.stub(worker["transformationFactory"], "getDefinitionTransformation").returns({
            transform: () => [
                {
                    name: "system_information",
                    url: "https://gbfs.nextbike.net/maps/gbfs/v2/nextbike_tg/cs/system_information.json",
                },
            ],
        } as unknown as NextbikeDefinitionTransformation);

        sandbox.stub(worker["transformationFactory"], "getDataTransformation").returns({
            transform: () => ({
                systemInfo: {
                    system_id: "nextbike_tg",
                } as ISharedBikesSystemInformationOutput,
            }),
        } as any);

        sandbox.stub(worker["systemInformationModel"], "save");
        sandbox.stub(worker["pricingPlansModel"], "save");
        sandbox.stub(worker["pricingModel"], "save");
        sandbox.stub(worker["rentalAppsModel"], "save");
        sandbox.stub(worker["vehicleTypesModel"], "save");
        sandbox.stub(worker["bikeStatusModel"], "save");
        sandbox.stub(worker["stationStatusModel"], "save");
        sandbox.stub(worker["stationStatusVehicleTypeModel"], "save");
        sandbox.stub(worker["stationInformationModel"], "save");
        sandbox.stub(worker["oldDataCleanerHelper"], "deleteOldTrackableData");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("refreshNextbikeData should call the correct methods", async () => {
        await worker.refreshNextbikeData();
        sandbox.assert.calledOnce(worker["dataSourceFactory"].getDataSourceDefinition as SinonSpy);
        sandbox.assert.calledOnce(worker["transformationFactory"].getDefinitionTransformation as SinonSpy);

        sandbox.assert.calledOnce(worker["dataSourceFactory"].getDataSourceData as SinonSpy);
        sandbox.assert.calledOnce(worker["transformationFactory"].getDataTransformation as SinonSpy);

        sandbox.assert.calledOnce(worker["systemInformationModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["pricingPlansModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["pricingModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["rentalAppsModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["vehicleTypesModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["bikeStatusModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["stationStatusModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["stationStatusVehicleTypeModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["stationInformationModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["oldDataCleanerHelper"].deleteOldTrackableData as SinonSpy);
    });
});
