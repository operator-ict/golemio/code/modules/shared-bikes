import { HoppyGoStaticDataService } from "#ie/HoppyGoStaticDataService";
import { RentalAppsModel } from "#ie/models/RentalAppsModel";
import { SystemInformationModel } from "#ie/models/SystemInformationModel";
import { VehicleTypesModel } from "#ie/models/VehicleTypesModel";

import { getRentalApps } from "#ie/transformations/HoppyGo/getRentalApps";
import { getSystemInformation } from "#ie/transformations/HoppyGo/getSystemInformation";
import { getVehicleTypes } from "#ie/transformations/HoppyGo/getVehicleTypes";
import {
    HOPPYGO_RENTAL_APP_ID,
    HOPPYGO_SYSTEM_ID,
    HOPPYGO_VEHICLE_TYPE_COMBUSTION,
    HOPPYGO_VEHICLE_TYPE_ELECTRIC,
} from "#ie/transformations/HoppyGo/transformConstants";
import { MobilityOperatorRentalAppProvider } from "#ie/transformations/MobilityOperator/MobilityOperatorRentalAppProvider";
import { MobilityOperatorTransformation } from "#ie/transformations/MobilityOperator/MobilityOperatorTransformation";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";
import { FakeDataSource } from "./transformations/MobilityOperator/FakeDataSource";

describe("HoppyGoStaticDataService", async () => {
    let sandbox: SinonSandbox;
    const systemInformationModel = new SystemInformationModel();
    const mobilityOperatorMapProvider = new MobilityOperatorRentalAppProvider(
        new FakeDataSource() as any as DataSource,
        new MobilityOperatorTransformation()
    );
    const hoppyGoStaticDataService = new HoppyGoStaticDataService(mobilityOperatorMapProvider);
    const vehicleTypesModel = new VehicleTypesModel();
    const rentalAppsModel = new RentalAppsModel();

    beforeEach(async () => {
        sandbox = sinon.createSandbox();
        await PostgresConnector.connect();
        await systemInformationModel.deleteBySystemId(HOPPYGO_SYSTEM_ID);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("saves HoppyGo static data correctly", async () => {
        const emptySystemInformation = await systemInformationModel.GetOne(HOPPYGO_SYSTEM_ID);
        expect(emptySystemInformation).to.be.null;

        await hoppyGoStaticDataService.saveSystemBasics();

        const systemInformation = await systemInformationModel.GetOne(HOPPYGO_SYSTEM_ID);
        expect(systemInformation).to.deep.equal(getSystemInformation(HOPPYGO_RENTAL_APP_ID));

        const vehicleTypes = getVehicleTypes();
        const electricVehicleType = await vehicleTypesModel.findOne({ id: HOPPYGO_VEHICLE_TYPE_ELECTRIC });
        const combustionVehicleType = await vehicleTypesModel.findOne({ id: HOPPYGO_VEHICLE_TYPE_COMBUSTION });
        expect(electricVehicleType).to.deep.equal(vehicleTypes[0]);
        expect(combustionVehicleType).to.deep.equal(vehicleTypes[1]);

        const rentalApps = getRentalApps();
        const actualRentalApps = await rentalAppsModel.findOne({ id: rentalApps.id });
        expect(actualRentalApps).to.deep.equal(rentalApps);
    });
});
