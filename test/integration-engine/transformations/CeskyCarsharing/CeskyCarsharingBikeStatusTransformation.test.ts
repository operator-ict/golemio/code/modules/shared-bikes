import { CeskyCarsharingBikeStatusTransformation } from "#ie/transformations/CeskyCarsharing/CeskyCarsharingBikeStatusTransformation";
import fs from "fs";
import { expect } from "chai";
import { CeskyCarsharingCarsFixture } from "../../data/CeskyCarsharingCars.fixture";
import { ICeskyCarsharingVehicles } from "#sch/datasources";

describe("CeskyCarsharingBikeStatusTransformation", async () => {
    const transformation = new CeskyCarsharingBikeStatusTransformation();

    it("transforms data correctly", async () => {
        const testSourceData: ICeskyCarsharingVehicles = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/cesky-carsharing-cars-datasource.json").toString("utf8")
        );
        const actualResult = await transformation.transform({
            cars: testSourceData.cars,
            transformationDate: new Date("2022-10-18T11:24:00.000Z"),
        });
        expect(actualResult).to.deep.equal(CeskyCarsharingCarsFixture);
    });
});
