import fs from "fs";
import { expect } from "chai";
import { ICeskyCarsharingGeofencingJson } from "#sch/datasources";
import { CeskyCarsharingGeofencingTransformation } from "#ie/transformations";
import { CeskyCarsharingPolygonFixture } from "../../data/CeskyCarsharingPolygon.fixture";

describe("CeskyCarsharingGeofencingTransformation", async () => {
    const transformation = new CeskyCarsharingGeofencingTransformation();

    it("transforms data correctly", async () => {
        const testSourceData: ICeskyCarsharingGeofencingJson = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/cesky-carsharing-polygon-datasource.json").toString("utf8")
        );
        const actualResult = await transformation.transform(testSourceData.polygons);
        expect(actualResult).to.deep.equal(CeskyCarsharingPolygonFixture);
    });
});
