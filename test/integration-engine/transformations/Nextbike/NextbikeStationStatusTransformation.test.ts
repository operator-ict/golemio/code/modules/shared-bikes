import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { NextbikeStationStatusTransformation } from "#ie/transformations";
import { INextbikeStationStatusInput } from "#sch/datasources";
import { nextbikeTransformedDataFixture } from "../../data/nextbikeTransformedData.fixture";

chai.use(chaiAsPromised);

describe("NextbikeStationStatusTransformation", () => {
    let transformation: NextbikeStationStatusTransformation;
    let testSourceData: INextbikeStationStatusInput;

    beforeEach(() => {
        transformation = new NextbikeStationStatusTransformation("TestId");
        testSourceData = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/nextbike-station-status-datasource.json").toString("utf8")
        );
    });

    it("should have name", () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("SharedBikesNextbikeStationStatusDataSourceTestIdTransformation");
    });

    it("should properly transform data", async () => {
        const data = await transformation.transform(testSourceData);
        expect(data.stationStatus).to.deep.equal(nextbikeTransformedDataFixture.stationStatus);
        expect(data.stationStatusVehicleType).to.deep.equal(nextbikeTransformedDataFixture.stationStatusVehicleType);
    });
});
