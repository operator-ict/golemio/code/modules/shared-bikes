import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { NextbikeSystemPricingPlansTransformation } from "#ie/transformations";
import { INextbikeSystemPricingPlansInput } from "#sch/datasources";
import { nextbikeTransformedDataFixture } from "../../data/nextbikeTransformedData.fixture";

chai.use(chaiAsPromised);

describe("NextbikeSystemPricingPlansTransformation", () => {
    let transformation: NextbikeSystemPricingPlansTransformation;
    let testSourceData: INextbikeSystemPricingPlansInput;

    beforeEach(() => {
        transformation = new NextbikeSystemPricingPlansTransformation("TestId");
        testSourceData = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/nextbike-system-pricing-plans-datasource.json").toString("utf8")
        );
    });

    it("should have name", () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("SharedBikesNextbikeSystemPricingPlansDataSourceTestIdTransformation");
    });

    it("should properly transform data", async () => {
        const data = await transformation.transform(testSourceData);
        expect(data).to.deep.equal({
            pricingPlan: nextbikeTransformedDataFixture.pricingPlans,
            pricing: nextbikeTransformedDataFixture.pricings,
        });
    });
});
