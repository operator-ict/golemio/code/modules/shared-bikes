import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { NextbikeSystemInformationTransformation } from "#ie/transformations";
import { INextbikeSystemInformationInput } from "#sch/datasources";
import { nextbikeTransformedDataFixture } from "../../data/nextbikeTransformedData.fixture";
import { MobilityOperatorRentalAppProvider } from "#ie/transformations/MobilityOperator/MobilityOperatorRentalAppProvider";
import { FakeDataSource } from "../MobilityOperator/FakeDataSource";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources";
import { MobilityOperatorTransformation } from "#ie/transformations/MobilityOperator/MobilityOperatorTransformation";

chai.use(chaiAsPromised);

describe("NextbikeSystemInformationTransformation", () => {
    let transformation: NextbikeSystemInformationTransformation;
    let testSourceData: INextbikeSystemInformationInput;

    beforeEach(() => {
        const mobilityOperatorMapProvider = new MobilityOperatorRentalAppProvider(
            new FakeDataSource() as any as DataSource,
            new MobilityOperatorTransformation()
        );

        transformation = new NextbikeSystemInformationTransformation("TestId", mobilityOperatorMapProvider);
        testSourceData = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/nextbike-system-information-datasource.json").toString("utf8")
        );
    });

    it("should have name", () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("SharedBikesNextbikeSystemInfoDataSourceTestIdTransformation");
    });

    it("should properly transform data", async () => {
        const data = await transformation.transform(testSourceData);
        expect(data.systemInfo).to.deep.equal(nextbikeTransformedDataFixture.systemInformation[0]);
        expect(nextbikeTransformedDataFixture.rentalApps).to.include.deep.members([data.rentalApps]);
    });
});
