import { expect } from "chai";
import { HoppyGoBikeStatusTransformation } from "#ie/transformations/HoppyGo/HoppyGoBikeStatusTransformation";
import { hoppyGoDatasourceFixture } from "../../data/HoppyGoDatasource.fixture";
import { hoppyGoBikeStatusTransformationFixture } from "../../data/HoppyGoTransformedBikeStatus.fixture";

describe("HoppyGoBikeStatusTransformation", async () => {
    const bikeStatusTransformation = new HoppyGoBikeStatusTransformation();
    const transformationDate = new Date();

    it("should have name", () => {
        expect(bikeStatusTransformation.name).not.to.be.undefined;
        expect(bikeStatusTransformation.name).is.equal("HoppyGoVehiclesDataSourceBikeStatusTransformation");
    });

    it("should correctly transform data", async () => {
        const actualData = await bikeStatusTransformation.transform({
            cars: hoppyGoDatasourceFixture,
            transformationDate,
        });

        expect(actualData).to.deep.equal(hoppyGoBikeStatusTransformationFixture(transformationDate));
    });
});
