import fs from "fs";
import { expect } from "chai";
import { RekolaStationStatusTransformation } from "#ie/transformations/Rekola";
import { rekolaStationStatusOutputFixture } from "../../data/rekolaStationStatusOutput.fixture";

describe("RekolaStationStatusTransformation", async () => {
    it("transforms input data correctly", async () => {
        const testSourceData = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/rekola-trackables-datasource-short.json").toString("utf8")
        );
        let transformationDate = new Date("2022-06-26T09:29:00.000Z");
        const stationStatusTransformation = new RekolaStationStatusTransformation();
        const transformedData = await stationStatusTransformation.transform({
            racks: testSourceData.racks,
            transformationDate,
        });

        expect(transformedData).to.deep.equal(rekolaStationStatusOutputFixture);
    });
});
