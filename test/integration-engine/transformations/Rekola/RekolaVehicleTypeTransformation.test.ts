import fs from "fs";
import { expect } from "chai";
import { RekolaVehicleTypeTransformation } from "#ie/transformations/Rekola";
import { rekolaVehicleTypeOutputFixture } from "../../data/rekolaVehicleTypeOutput.fixture";

describe("RekolaVehicleTypeTransformation", async () => {
    it("transforms input data correctly", async () => {
        const testSourceData = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/rekola-trackables-datasource-short.json").toString("utf8")
        );
        const vehicleTypeTransformation = new RekolaVehicleTypeTransformation();
        const transformedData = await vehicleTypeTransformation.transform(testSourceData);

        expect(transformedData).to.deep.equal(rekolaVehicleTypeOutputFixture);
    });
});
