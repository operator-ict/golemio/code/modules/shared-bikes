import fs from "fs";
import { expect } from "chai";
import { RekolaBikeStatusTransformation } from "#ie/transformations/Rekola";
import { rekolaBikeStatusOutputFixture } from "../../data/rekolaBikeStatusOutput.fixture";

describe("RekolaBikeStatusTransformation", async () => {
    it("transforms input data correctly", async () => {
        const testSourceData = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/rekola-trackables-datasource-short.json").toString("utf8")
        );

        const transformationDate = new Date("2022-06-26T09:29:00.000Z");
        const transformation = new RekolaBikeStatusTransformation();
        const transformedData = await transformation.transform({
            trackables: testSourceData,
            transformationDate,
        });

        expect(transformedData).to.deep.equal(rekolaBikeStatusOutputFixture);
    });
});
