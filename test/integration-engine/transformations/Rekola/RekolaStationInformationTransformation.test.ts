import fs from "fs";
import { expect } from "chai";
import { RekolaStationInformationTransformation } from "#ie/transformations/Rekola";
import { rekolaStationInformationOutputFixtture } from "../../data/rekolaStationInformationOutput.fixture";

describe("RekolaStationInformationTransformation", async () => {
    it("transforms input data correctly", async () => {
        const testSourceData = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/rekola-trackables-datasource-short.json").toString("utf8")
        );
        const stationInformationTransformation = new RekolaStationInformationTransformation();

        const transformedData = await stationInformationTransformation.transform(testSourceData.racks);

        expect(transformedData).to.deep.equal(rekolaStationInformationOutputFixtture);
    });
});
