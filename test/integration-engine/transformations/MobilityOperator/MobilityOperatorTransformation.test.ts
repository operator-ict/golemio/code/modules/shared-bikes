import { MobilityOperatorTransformation } from "#ie/transformations/MobilityOperator/MobilityOperatorTransformation";
import { IMobilityOperator } from "#sch/datasources/MobilityOperator";
import fs from "fs";
import { expect } from "chai";
import { MobilityOperatorMapFixture } from "../../data/MobilityOperatorMap.fixture";

describe("MobilityOperatorTransformation", () => {
    const transformation = new MobilityOperatorTransformation();

    it("transforms data correctly", async () => {
        const testData: IMobilityOperator[] = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/MobilityOperator.json").toString("utf8")
        );

        const actualResult = await transformation.transform(testData);
        expect(actualResult).to.deep.equal(MobilityOperatorMapFixture);
    });
});
