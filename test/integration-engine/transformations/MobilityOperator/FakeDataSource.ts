import fs from "fs";

export class FakeDataSource {
    public async getAll(): Promise<any> {
        return JSON.parse(fs.readFileSync(__dirname + "/../../data/MobilityOperator.json").toString("utf8"));
    }
}
