import sinon, { SinonSandbox, SinonSpy } from "sinon";
import fs from "fs";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { RekolaSharedBikesWorker } from "#ie/RekolaSharedBikesWorker";

describe("RekolaSharedBikesWorker", () => {
    let worker: RekolaSharedBikesWorker;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
                transaction: sandbox.stub(),
                query: sandbox.stub().returns([[{}]]),
            })
        );

        worker = new RekolaSharedBikesWorker();

        const trackableData = JSON.parse(
            fs.readFileSync(__dirname + "/data/rekola-trackables-datasource-short.json").toString("utf8")
        );

        sandbox.stub(worker["zonesDatasource"], "getAll");
        sandbox.stub(worker["trackablesDatasource"], "getAll").returns(trackableData);

        sandbox.stub(worker["geofencingTransformation"], "transform").callsFake(() => Promise.resolve([] as any));

        sandbox.stub(worker["stationInformationTransformation"], "transform").callsFake(() => Promise.resolve([] as any));
        sandbox.stub(worker["stationStatusTransformation"], "transform").callsFake(() => Promise.resolve([] as any));
        sandbox.stub(worker["bikeStatusTransformation"], "transform").callsFake(() => Promise.resolve([] as any));
        sandbox.stub(worker["vehicleTypeTransformation"], "transform").callsFake(() => Promise.resolve([] as any));
        sandbox.stub(worker["stationStatusVehicleTypeTransformation"], "transform").callsFake(() => Promise.resolve([] as any));
        sandbox.stub(worker["rekolaStaticDataService"], "saveRekolaStaticData").callsFake(() => Promise.resolve([] as any));

        sandbox.stub(worker["geofencingZonesModel"], "replace");

        sandbox.stub(worker["vehicleTypesModel"], "save");
        sandbox.stub(worker["stationInformationModel"], "save");
        sandbox.stub(worker["stationStatusModel"], "save");
        sandbox.stub(worker["stationStatusVehicleTypeModel"], "save");
        sandbox.stub(worker["bikeStatusModel"], "save");
        sandbox.stub(worker["oldDataCleanerHelper"], "deleteOldTrackableData");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("refreshRekolaGeofencingData should call the correct methods", async () => {
        await worker.refreshRekolaGeofencingData();
        sandbox.assert.calledOnce(worker["zonesDatasource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["geofencingTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["geofencingZonesModel"].replace as SinonSpy);
    });

    it("refreshRekolaTrackableData should call the correct methods", async () => {
        await worker.refreshRekolaTrackableData();

        sandbox.assert.calledOnce(worker["rekolaStaticDataService"].saveRekolaStaticData as SinonSpy);
        sandbox.assert.calledOnce(worker["trackablesDatasource"].getAll as SinonSpy);

        sandbox.assert.calledOnce(worker["stationInformationTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["stationStatusTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["bikeStatusTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["vehicleTypeTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["stationStatusVehicleTypeTransformation"].transform as SinonSpy);

        sandbox.assert.calledOnce(worker["vehicleTypesModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["stationInformationModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["stationStatusModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["stationStatusVehicleTypeModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["bikeStatusModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["oldDataCleanerHelper"].deleteOldTrackableData as SinonSpy);
    });
});
