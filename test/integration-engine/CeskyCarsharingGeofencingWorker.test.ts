import sinon, { SinonSandbox, SinonSpy } from "sinon";
import fs from "fs";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { CeskyCarsharingGeofencingWorker } from "#ie/CeskyCarsharingGeofencingWorker";

describe("CeskyCarsharingGeofencingWorker", () => {
    let worker: CeskyCarsharingGeofencingWorker;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
                transaction: sandbox.stub(),
                query: sandbox.stub().returns([[{}]]),
            })
        );

        worker = new CeskyCarsharingGeofencingWorker();

        const trackableData = JSON.parse(
            fs.readFileSync(__dirname + "/data/cesky-carsharing-polygon-datasource.json").toString("utf8")
        );

        sandbox.stub(worker["dataSource"], "getAll").returns(trackableData);
        sandbox
            .stub(worker["ceskyCarsharingStaticDataService"], "saveSystemsStaticData")
            .callsFake(() => Promise.resolve([] as any));
        sandbox.stub(worker["geofencingTransformation"], "transform").callsFake(() => Promise.resolve([] as any));
        sandbox.stub(worker["geofencingZonesModel"], "save").callsFake(() => Promise.resolve([] as any));
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("refreshCeskyCarsharingGeofencingData should call the correct methods", async () => {
        await worker.refreshCeskyCarsharingGeofencingData();
        sandbox.assert.calledOnce(worker["ceskyCarsharingStaticDataService"].saveSystemsStaticData as SinonSpy);
        sandbox.assert.calledOnce(worker["dataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["geofencingTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["geofencingZonesModel"].save as SinonSpy);
    });
});
