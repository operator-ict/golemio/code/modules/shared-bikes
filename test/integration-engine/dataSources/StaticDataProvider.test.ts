import { StaticDataProvider } from "#ie/dataSources/StaticDataProvider";
import { ISharedBikesRentalAppOutput } from "#sch";
import { expect } from "chai";
import { ModuleContainerToken, SharedBikesContainer } from "#ie/ioc";

describe("StaticDataProvider", () => {
    const staticDataProvider: StaticDataProvider = SharedBikesContainer.resolve<StaticDataProvider>(
        ModuleContainerToken.StaticDataProvider
    );

    it("returns valid data", () => {
        const actualResult = staticDataProvider.getRentalApps("Rekola");
        const expectedResult: ISharedBikesRentalAppOutput = {
            id: "bb622abc-84a1-11ec-a8a3-0242ac120002",
            android_store_url: "https://play.google.com/store/apps/details?id=cz.rekola.app&hl=cs",
            android_discovery_url: "https://play.google.com/store/apps/details?id=cz.rekola.app&hl=cs",
            ios_store_url: "https://itunes.apple.com/cz/app/rekola/id888759232?mt=8",
            ios_discovery_url: "https://itunes.apple.com/cz/app/rekola/id888759232?mt=8",
            web_url: "https://www.rekola.cz",
        };

        expect(actualResult).to.deep.eq(expectedResult);
    });

    it("throws error when file does not exist for given provider", () => {
        expect(() => {
            staticDataProvider.getRentalApps("PragueTractorCompany");
        }).to.throw();
    });
});
