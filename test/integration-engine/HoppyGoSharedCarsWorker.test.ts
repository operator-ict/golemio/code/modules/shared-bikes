import { HoppyGoSharedCarsWorker } from "#ie/HoppyGoSharedCarsWorker";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import fs from "fs";
import sinon, { SinonSandbox, SinonSpy } from "sinon";

describe("HoppyGoSharedCarsWorker", () => {
    let worker: HoppyGoSharedCarsWorker;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
                transaction: sandbox.stub(),
                query: sandbox.stub().returns([[{}]]),
            })
        );

        worker = new HoppyGoSharedCarsWorker();

        const trackableData = JSON.parse(fs.readFileSync(__dirname + "/data/hoppygo-datasource.json").toString("utf8"));

        sandbox.stub(worker["dataSource"], "getAll").returns(trackableData);

        sandbox.stub(worker["staticDataService"], "saveSystemBasics").callsFake(() => Promise.resolve([] as any));
        sandbox.stub(worker["pricingPlansTransformation"], "transform").callsFake(() => Promise.resolve([] as any));
        sandbox.stub(worker["bikeStatusTransformation"], "transform").callsFake(() => Promise.resolve([] as any));

        sandbox.stub(worker["pricingPlansModel"], "save");
        sandbox.stub(worker["bikeStatusModel"], "save");
        sandbox.stub(worker["oldDataCleanerHelper"], "deleteOldTrackableData");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("refreshHoppyGoSharedCarsData should call the correct methods", async () => {
        await worker.refreshHoppyGoSharedCarsData();
        sandbox.assert.calledOnce(worker["dataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["staticDataService"].saveSystemBasics as SinonSpy);
        sandbox.assert.calledOnce(worker["pricingPlansTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["pricingPlansModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["bikeStatusTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["bikeStatusModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["oldDataCleanerHelper"].deleteOldTrackableData as SinonSpy);
    });
});
