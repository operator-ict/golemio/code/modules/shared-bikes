import sinon, { SinonSandbox } from "sinon";
import request from "supertest";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import { sharedCarsRouter } from "#og";

chai.use(chaiAsPromised);

describe("SharedCars Router", () => {
    // Create clean express instance
    const app = express();
    // Basic configuration: create a sinon sandbox for testing
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox && sandbox.restore();
    });

    before(() => {
        // Mount the tested router to the express instance
        app.use("/sharedcars", sharedCarsRouter);
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should respond with json to GET /sharedcars", (done) => {
        request(app).get("/sharedcars").set("Accept", "application/json").expect("Content-Type", /json/).expect(200, done);
    });

    it("GET /sharedcars should only return cars", (done) => {
        request(app)
            .get("/sharedcars")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body).to.be.an.instanceOf(Object);
                expect(res.body.features).to.be.an.instanceOf(Array);
                expect(res.body.type).to.be.equal("FeatureCollection");
                expect(res.body.features.length).to.be.greaterThan(0);
                for (const entry of res.body.features) {
                    expect(entry.properties.fuel.id).to.be.not.eql(0);
                }
                done();
            });
    });

    it("GET /sharedcars?companyNames=AJO should only return AJO cars", (done) => {
        request(app)
            .get("/sharedcars?companyNames=AJO")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body).to.be.an.instanceOf(Object);
                expect(res.body.features).to.be.an.instanceOf(Array);
                expect(res.body.type).to.be.equal("FeatureCollection");
                expect(res.body.features.length).to.be.greaterThan(0);
                for (const entry of res.body.features) {
                    expect(entry.properties.company.name).to.be.equal("AJO");
                }
                done();
            });
    });

    it("GET /sharedcars?companyNames[]=AJO&companyNames[]=HoppyGo should only return AJO and HoppyGo cars", (done) => {
        request(app)
            .get("/sharedcars?companyNames[]=AJO&companyNames[]=HoppyGo")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body).to.be.an.instanceOf(Object);
                expect(res.body.features).to.be.an.instanceOf(Array);
                expect(res.body.type).to.be.equal("FeatureCollection");
                expect(res.body.features.length).to.be.greaterThan(0);
                for (const entry of res.body.features) {
                    expect(entry.properties.company.name).to.be.oneOf(["AJO", "HoppyGo"]);
                }
                done();
            });
    });

    it(
        "GET /sharedcars?latlng=50.07583730334994,14.416242211769685&range=750&limit=5 " +
            "should return vehicles within the range",
        (done) => {
            request(app)
                .get("/sharedcars?latlng=50.07583730334994,14.416242211769685&range=750&limit=5")
                .end((err: any, res: any) => {
                    expect(res.statusCode).to.be.equal(200);
                    expect(res.body).to.be.an.instanceOf(Object);
                    expect(res.body.features).to.be.an.instanceOf(Array);
                    expect(res.body.type).to.be.equal("FeatureCollection");
                    expect(res.body.features).to.have.length(2);
                    done();
                });
        }
    );

    it("GET /sharedcars?updatedSince=CURRENT_DATE should not return anything", (done) => {
        request(app)
            .get("/sharedcars?updatedSince=" + new Date().toISOString())
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body).to.be.an.instanceOf(Object);
                expect(res.body.features).to.be.an.instanceOf(Array);
                expect(res.body.type).to.be.equal("FeatureCollection");
                expect(res.body.features).to.have.length(0);
                done();
            });
    });

    it("GET /sharedcars/hoppygo-9943 should return one vehicle", (done) => {
        request(app)
            .get("/sharedcars/hoppygo-9943")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body).to.be.an.instanceOf(Object);
                expect(res.body.geometry.coordinates).to.be.an.instanceOf(Array);
                expect(res.body.type).to.be.equal("Feature");
                expect(res.body.properties.id).to.be.equal("hoppygo-9943");
                done();
            });
    });

    it("GET /sharedcars/idk should response with 404", (done) => {
        request(app)
            .get("/sharedcars/idk")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(404);
                done();
            });
    });
});
