import sinon, { SinonSandbox } from "sinon";
import request from "supertest";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import chaiJsonSchema from "chai-json-schema";
import fs from "fs";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import { gbfsRouter } from "#og/GBFSRouter";

chai.use(chaiAsPromised);
chai.use(chaiJsonSchema);

describe("GBFS Router", () => {
    // Create clean express instance
    const app = express();
    // Basic configuration: create a sinon sandbox for testing
    let sandbox: SinonSandbox;
    const testSystemId = "d727bb19-9755-40b2-9615-01fbb6180b8b";
    const jsonSchemas: { [key: string]: any } = {
        gbfs: undefined,
        gbfs_versions: undefined,
        system_information: undefined,
        free_bike_status: undefined,
        vehicle_types: undefined,
        station_information: undefined,
        station_status: undefined,
        system_pricing_plans: undefined,
        geofencing_zones: undefined,
    };

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        for (const key of Object.keys(jsonSchemas)) {
            jsonSchemas[key] = JSON.parse(fs.readFileSync(`${__dirname}/gbfsJsonSchemas/${key}.schema.json`).toString("utf-8"));
        }
    });

    afterEach(() => {
        sandbox && sandbox.restore();
    });

    before(() => {
        // Mount the tested router to the express instance
        app.use("/sharedbikes/gbfs", gbfsRouter);
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should respond with json to GET /sharedbikes/gbfs/systems_list", (done) => {
        request(app)
            .get("/sharedbikes/gbfs/systems_list")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.body).to.be.an.instanceOf(Object);
                expect(res.body).to.haveOwnProperty("shared_mobility_providers");
                expect(res.body.shared_mobility_providers).to.haveOwnProperty("providers_url_list");
                expect(res.body.shared_mobility_providers.providers_url_list).to.be.an.instanceOf(Array);
                done();
            });
    });

    describe("gbfs", () => {
        it("should respond with json to GET /sharedbikes/gbfs/:system_id/gbfs", (done) => {
            request(app)
                .get(`/sharedbikes/gbfs/${testSystemId}/gbfs`)
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.be.an.instanceOf(Object);
                    expect(res.body).to.be.jsonSchema(jsonSchemas.gbfs);
                    done();
                });
        });

        it("should respond with 404 to GET /sharedbikes/gbfs/bad_id/gbfs", (done) => {
            request(app)
                .get(`/sharedbikes/gbfs/bad_id/gbfs`)
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(404, done);
        });
    });

    describe("gbfs_versions", () => {
        it("should respond with json to GET /sharedbikes/gbfs/:system_id/gbfs_versions", (done) => {
            request(app)
                .get(`/sharedbikes/gbfs/${testSystemId}/gbfs_versions`)
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.be.an.instanceOf(Object);
                    expect(res.body).to.be.jsonSchema(jsonSchemas.gbfs_versions);
                    done();
                });
        });

        it("should respond with 404 to GET /sharedbikes/gbfs/bad_id/gbfs_versions", (done) => {
            request(app)
                .get(`/sharedbikes/gbfs/bad_id/gbfs_versions`)
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(404, done);
        });
    });

    describe("system_information", () => {
        it("should respond with json to GET /sharedbikes/gbfs/:system_id/system_information", (done) => {
            request(app)
                .get(`/sharedbikes/gbfs/${testSystemId}/system_information`)
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.be.an.instanceOf(Object);
                    expect(res.body).to.be.jsonSchema(jsonSchemas.system_information);
                    done();
                });
        });

        it("should respond with 404 to GET /sharedbikes/gbfs/bad_id/system_information", (done) => {
            request(app)
                .get(`/sharedbikes/gbfs/bad_id/system_information`)
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(404, done);
        });
    });

    describe("free_bike_status", () => {
        it("should respond with json to GET /sharedbikes/gbfs/:system_id/free_bike_status", (done) => {
            request(app)
                .get(`/sharedbikes/gbfs/${testSystemId}/free_bike_status`)
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.be.an.instanceOf(Object);
                    expect(res.body).to.be.jsonSchema(jsonSchemas.free_bike_status);
                    done();
                });
        });

        it("should respond with 404 to GET /sharedbikes/gbfs/bad_id/free_bike_status", (done) => {
            request(app)
                .get(`/sharedbikes/gbfs/bad_id/free_bike_status`)
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(404, done);
        });
    });

    describe("vehicle_types", () => {
        it("should respond with json to GET /sharedbikes/gbfs/:system_id/vehicle_types", (done) => {
            request(app)
                .get(`/sharedbikes/gbfs/${testSystemId}/vehicle_types`)
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.be.an.instanceOf(Object);
                    expect(res.body).to.be.jsonSchema(jsonSchemas.vehicle_types);
                    done();
                });
        });

        it("should respond with 404 to GET /sharedbikes/gbfs/bad_id/vehicle_types", (done) => {
            request(app)
                .get(`/sharedbikes/gbfs/bad_id/vehicle_types`)
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(404, done);
        });
    });

    describe("station_information", () => {
        it("should respond with json to GET /sharedbikes/gbfs/:system_id/station_information", (done) => {
            request(app)
                .get(`/sharedbikes/gbfs/${testSystemId}/station_information`)
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.be.an.instanceOf(Object);
                    expect(res.body).to.be.jsonSchema(jsonSchemas.station_information);
                    done();
                });
        });

        it("should respond with 404 to GET /sharedbikes/gbfs/bad_id/station_information", (done) => {
            request(app)
                .get(`/sharedbikes/gbfs/bad_id/station_information`)
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(404, done);
        });
    });

    describe("station_status", () => {
        it("should respond with json to GET /sharedbikes/gbfs/:system_id/station_status", (done) => {
            request(app)
                .get(`/sharedbikes/gbfs/${testSystemId}/station_status`)
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.be.an.instanceOf(Object);
                    expect(res.body).to.be.jsonSchema(jsonSchemas.station_status);
                    done();
                });
        });

        it("should respond with 404 to GET /sharedbikes/gbfs/bad_id/station_status", (done) => {
            request(app)
                .get(`/sharedbikes/gbfs/bad_id/station_status`)
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(404, done);
        });
    });

    describe("system_pricing_plans", () => {
        it("should respond with json to GET /sharedbikes/gbfs/:system_id/system_pricing_plans", (done) => {
            request(app)
                .get(`/sharedbikes/gbfs/${testSystemId}/system_pricing_plans`)
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.be.an.instanceOf(Object);
                    expect(res.body).to.be.jsonSchema(jsonSchemas.system_pricing_plans);
                    done();
                });
        });

        it("should respond with 404 to GET /sharedbikes/gbfs/bad_id/system_pricing_plans", (done) => {
            request(app)
                .get(`/sharedbikes/gbfs/bad_id/system_pricing_plans`)
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(404, done);
        });
    });

    describe("geofencing_zones", () => {
        it("should respond with json to GET /sharedbikes/gbfs/:system_id/geofencing_zones", (done) => {
            request(app)
                .get(`/sharedbikes/gbfs/${testSystemId}/geofencing_zones`)
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.be.an.instanceOf(Object);
                    expect(res.body).to.be.jsonSchema(jsonSchemas.geofencing_zones);
                    done();
                });
        });

        it("should respond with 404 to GET /sharedbikes/gbfs/bad_id/geofencing_zones", (done) => {
            request(app)
                .get(`/sharedbikes/gbfs/bad_id/geofencing_zones`)
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(404, done);
        });
    });
});
