import chai, { expect } from "chai";
import { removeEmptyOrNull } from "#og/helpers";

describe("removeEmptyOrNull", () => {
    it("should remove null properties", async () => {
        expect(
            removeEmptyOrNull({
                system_id: "d727bb19-9755-40b2-9615-01fbb6180b8b",
                helmets: null,
                passengers: null,
                damage_description: null,
                description: "",
                vehicle_registration: null,
                is_reserved: false,
                is_disabled: false,
                vehicle_type_id: "rekola-bike",
                last_reported: 1647853200,
                current_range_meters: null,
                charge_percent: null,
                station_id: "rekola-4",
                bike_id: "rekola-1926",
                lat: 50.1088538325,
                lon: 14.4610581741,
                pricing_plan_id: "4b72f960-84cf-11ec-a8a3-0242ac120002",
                rental_uris: {
                    android: null,
                    ios: null,
                    web: "https://www.rekola.cz",
                },
                vehicle_type: {
                    id: "rekola-bike",
                    form_factor: "bicycle",
                    propulsion_type: "human",
                    max_range_meters: null,
                    name: "bicycle",
                },
            })
        ).to.be.deep.equal({
            system_id: "d727bb19-9755-40b2-9615-01fbb6180b8b",
            description: "",
            is_reserved: false,
            is_disabled: false,
            vehicle_type_id: "rekola-bike",
            last_reported: 1647853200,
            station_id: "rekola-4",
            bike_id: "rekola-1926",
            lat: 50.1088538325,
            lon: 14.4610581741,
            pricing_plan_id: "4b72f960-84cf-11ec-a8a3-0242ac120002",
            rental_uris: {
                web: "https://www.rekola.cz",
            },
            vehicle_type: {
                id: "rekola-bike",
                form_factor: "bicycle",
                propulsion_type: "human",
                name: "bicycle",
            },
        });
    });

    it("should not remove null properties inside array", async () => {
        expect(removeEmptyOrNull({ arrar: [{ notnull: "value", null: null }] })).to.be.deep.equal({
            arrar: [{ notnull: "value", null: null }],
        });
    });
});
