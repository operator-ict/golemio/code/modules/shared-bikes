# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [2.2.3] - 2023-07-04

### Changed

-   Enable additional properties for MobilityOperator.json ([p0131#141](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/141))

## [2.2.2] - 2023-06-19

### Removed

-   Remove BeRider integration ([p0131#136](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/136))

## [2.2.1] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [2.2.0] - 2023-06-05

### Fixed

-   Configurable GBFS url link ([p0131#132](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/132))

## [2.1.11] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [2.1.10] - 2023-05-29

### Changed

-   Update openapi docs

## [2.1.9] - 2023-04-17

### Changed

- Deleting old data immediately after provider worker run, instead of periodic by time

## [2.1.8] - 2023-04-05

### Changed

- Errors from Český carsharing transformation logged as "debug" instead of "error"

## [2.1.7] - 2023-04-03

### Changed
- DI and json static files Český carsharing
- updated price plans for Český carsharing

### Fixed
- Fixed rental app ids of mobilityOperator source

## [2.1.6] - 2023-03-27

### Added

-   Add shared cars router as a replacement of deprecated shared-cars module ([#14](https://gitlab.com/operator-ict/golemio/code/modules/shared-bikes/-/issues/14))

## [2.1.5] - 2023-03-22

### Fixed

-   Add filter by vehicle types to legacy endpoint, fix query filter companyNames ([#20](https://gitlab.com/operator-ict/golemio/code/modules/shared-bikes/-/issues/20))

### Added

-   Add shared cars router as a replacement of deprecated shared-cars module ([#14](https://gitlab.com/operator-ict/golemio/code/modules/shared-bikes/-/issues/14))

## [2.1.4] - 2023-03-20

### Changed

-   data outside of czechia are not saved.

## [2.1.3] - 2023-03-08

### Fixed

- Include staticData in package

## [2.1.2] - 2023-03-06

### Changed

-   open api docs minor changes

## [2.1.1] - 2023-02-27

### Added
-   Added MobilityOperator data source; used as source for rental_apps
-   Partly implemented DI via tsyringe for Rekola and BeRider

### Changed
-   Moved BeRider constants from worker to contants file
-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))


## [2.1.0] - 2023-01-23

### Changed

-   Migrate to npm

## [2.0.16] - 2023-01-10

### Fixed

-   `SharedBikesNextbikeFreeBikeStatusDataSource` failing due to a change of `vehicle_type_id`'s field type

## [2.0.15] - 2023-01-04

### Changed

-   updated reference of terms of use for HoppyGo, NextBike and BeRider

## [2.0.14] - 2022-12-09

### Fixed

-   `SharedBikesNextbikeFreeBikeStatusDataSource` failing due to a new property `vehicle_type_id`

## [2.0.13] - 2022-12-07

### Changed

-   Add id column to pricings to allow multiple pricings of a type being stored in database
-   Update Cesky carsharing pricelists
-   Update Nextbike pricing plans description

## [2.0.12] - 2022-11-14

### Changed

-   Update pricing plans of Český carsharing / Autonapůl
-   Skip cars in Český carsharing feed that cannot be processed

## [2.0.11] - 2022-11-03

### Added

-   New integration: Český carsharing

## [2.0.10] - 2022-10-11

-   Update static data

## [2.0.9] - 2022-09-21

### Changed

-   The Lodash library was replaced with js functions ([core#42](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/42))
-   Removed JOIN on pricing_plans in BikeStatusModel.GetAllGTFS() as not needed.

### Added

-   Add HoppyGo integration

### Removed

-   Unused dependencies ([general#5](https://gitlab.com/operator-ict/golemio/code/modules/general/-/issues/5))

## [2.0.8] - 2022-08-01

### Added

-   Fix / add bike_status.pricing_plan_id
-   Added make, model and color to BikeStatus

## [2.0.7] - 2022-07-18

### Added

-   New Nextbike integration

### Changed

-   Fix strict validation for Berider (Helmets, isClean)
-   Rekola transformation refactoring

## [2.0.6] - 2022-06-07

### Changed

-   change operator_id from integer to string
-   Typescript version update from 4.4.4 to 4.6.4

## [2.0.5] - 2022-05-19

### Added

-   Add BeRider integration

### Changed

-   refactor: code structure and schema fixes
-   fix OG/IE SystemInformationModel

## [2.0.4] - 2022-05-18

### Fixed

-   Minor fixes in json validation schema to support strict option

## [2.0.3] - 2022-05-02

-   Add transformation of Next Bike api data to GBFS

### Changed

-   fix: remove unused rental_apps_system_info
-   fix: add pricings table primary key
-   refactor: separate StationInformationHistoryModel from generic one
-   fix: change rekola system information language to cs
-   fix: fix schemas implementation

### Added

### Changed

-   update rental app discovery url

## [2.0.2] - 2022-04-13

### Changed

-   Removed `.json` extension from GBFS endpoint urls

## [2.0.1] - 2022-04-07

### Added

-   Implement GBFS output api ([p0131#23](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/23))
-   Add OpenAPI v3.0.3 ([#4](https://gitlab.com/operator-ict/golemio/code/modules/shared-bikes/-/issues/4))
-   Add implementation documentation ([#6](https://gitlab.com/operator-ict/golemio/code/modules/shared-bikes/-/issues/6))

### Fixed

-   Add time zone to timestamps

## [2.0.0] - 2022-03-01

### Added

-   Implement GBFS format for Rekola trackables and geofencing zones ([p0131#22](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/22))

## Changed

-   Rewrite models from Mongo to Postgres
